--face_view_cfg.lua
--source: 1_face_view_捏脸预制管理表.xlsx
--luacheck: ignore 631

--导出配置内容
local face_view = {
    {
        icon='#A1694F',
        id=101,
        res_name='#B28F7D',
        type=1
    },
    {
        icon='#CF825F',
        id=102,
        res_name='#E6B59C',
        type=1
    },
    {
        icon='#FFA175',
        id=103,
        res_name='#F2BD96',
        type=1
    },
    {
        icon='#FFC98C',
        id=104,
        res_name='#FFF4D6',
        type=1
    },
    {
        icon='#FFC3A6',
        id=105,
        res_name='#FFFFFFF',
        type=1
    },
    {
        icon='UI_Custom_Menu_Face_Img',
        id=106,
        res_name='Max_Head_0_Pre',
        type=2
    },
    {
        icon='UI_Custom_Menu_Face_Img',
        id=107,
        res_name='Max_Head_1_Pre',
        type=2
    },
    {
        icon='UI_Custom_Menu_Face_Img',
        id=108,
        res_name='Max_Head_2_Pre',
        type=2
    },
    {
        icon='UI_Custom_Menu_Face_Img',
        id=109,
        res_name='Lucy_Head_0_Pre',
        type=2
    },
    {
        icon='UI_Custom_Menu_Face_Img',
        id=110,
        res_name='Lucy_Head_1_Pre',
        type=2
    },
    {
        icon='UI_Custom_Menu_Face_Img',
        id=111,
        res_name='Lucy_Head_2_Pre',
        type=2
    }
}


return face_view
