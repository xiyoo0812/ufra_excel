--itemstory_cfg.lua
--source: 2_ItemStory_道具故事表.xlsx
--luacheck: ignore 631

--导出配置内容
local itemstory = {
    {
        id=1,
        item_id=101083,
        lc_key='LC_label_ui_Homeland_ItemStory_1'
    },
    {
        id=2,
        item_id=101104,
        lc_key='LC_label_ui_Homeland_ItemStory_1'
    },
    {
        id=3,
        item_id=101111,
        lc_key='LC_label_ui_Homeland_ItemStory_2'
    },
    {
        id=4,
        item_id=101119,
        lc_key='LC_label_ui_Homeland_ItemStory_2'
    },
    {
        id=5,
        item_id=101102,
        lc_key='LC_label_ui_Homeland_ItemStory_3'
    },
    {
        id=6,
        item_id=101105,
        lc_key='LC_label_ui_Homeland_ItemStory_3'
    },
    {
        id=7,
        item_id=101109,
        lc_key='LC_label_ui_Homeland_ItemStory_4'
    },
    {
        id=8,
        item_id=101081,
        lc_key='LC_label_ui_Homeland_ItemStory_4'
    },
    {
        id=9,
        item_id=101082,
        lc_key='LC_label_ui_Homeland_ItemStory_5'
    },
    {
        id=10,
        item_id=101112,
        lc_key='LC_label_ui_Homeland_ItemStory_5'
    }
}


return itemstory
