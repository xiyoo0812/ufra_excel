--dungeon_01_cfg.lua
--source: 7_dungeon_01地图信息表.xlsx
--luacheck: ignore 631

--导出配置内容
local dungeon_01 = {
    {
        dir={
            0,
            0,
            0
        },
        dynamic=true,
        id=1001,
        name='铜矿_01_1230测试',
        pos={
            -557,
            53,
            -753
        },
        proto_id=20019,
        refresh_limit=0,
        refresh_time=259200,
        scale={
            100,
            100,
            100
        },
        status=true,
        type=4,
        visit_gather=true
    }
}


return dungeon_01
