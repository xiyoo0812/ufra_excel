--sandrock_cfg.lua
--source: 7_sandrock_sandrock地图信息表.xlsx
--luacheck: ignore 631

--导出配置内容
local sandrock = {
    {
        dynamic=true,
        id=5,
        name='砂石',
        pos={
            -21570,
            4677,
            -6430
        },
        proto_id=20001,
        refresh_time=3600,
        type=4
    },
    {
        dynamic=true,
        id=6,
        name='杉树',
        pos={
            -21570,
            4601,
            -6698
        },
        proto_id=20002,
        refresh_time=3600,
        type=4
    },
    {
        dynamic=true,
        id=7,
        name='小草',
        pos={
            -22491,
            4544,
            -6698
        },
        proto_id=20003,
        refresh_time=3600,
        type=4
    },
    {
        dynamic=true,
        id=8,
        name='宝箱',
        pos={
            -23520,
            4475,
            -6698
        },
        proto_id=20007,
        refresh_time=3600,
        type=4
    },
    {
        dynamic=true,
        id=9,
        name='仙人掌',
        pos={
            -23520,
            4642,
            -5793
        },
        proto_id=20008,
        refresh_time=3600,
        type=4
    }
}


return sandrock
