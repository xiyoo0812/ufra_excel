--chat_template_cfg.lua
--source: 15-chat_template_聊天模版表.xlsx
--luacheck: ignore 631

--导出配置内容
local chat_template = {
    {
        content='LC_chat_content_100000',
        id=100000
    },
    {
        content='LC_chat_content_100050',
        id=100050
    },
    {
        content='LC_chat_content_100051',
        id=100051
    },
    {
        content='LC_chat_content_100052',
        id=100052
    },
    {
        content='LC_chat_content_100100',
        id=100100
    },
    {
        content='LC_chat_content_100150',
        id=100150
    }
}


return chat_template
