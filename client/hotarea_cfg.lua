--hotarea_cfg.lua
--source: 7_hotarea_热区传送表.xlsx
--luacheck: ignore 631

--导出配置内容
local hotarea = {
    {
        id=1,
        map_id=1003,
        pos={
            131997,
            14030,
            89149
        },
        radius=800,
        tar_map_id=1005,
        tar_pos={
            890,
            1,
            300
        },
        type=1
    },
    {
        id=2,
        map_id=1005,
        pos={
            41,
            203,
            -26
        },
        radius=800,
        tar_map_id=1003,
        tar_pos={
            131024,
            13127,
            84370
        },
        type=1
    }
}


return hotarea
