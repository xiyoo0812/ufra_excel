--game_push_cfg.lua
--source: 17-推送通知配置表.xlsx
--luacheck: ignore 631

--导出配置内容
local game_push = {
    {
        id=1,
        level=100,
        template='LC_label_ui_Game_pushbell_1'
    },
    {
        id=2,
        level=99,
        template='LC_label_ui_Game_pushbell_2'
    }
}


return game_push
