--activity_sign_in_cfg.lua
--source: 12_activity_sign_in_7日签到.xlsx
--luacheck: ignore 631

--导出配置内容
local activity_sign_in = {
    {
        activity_id=1000,
        child_id=1,
        day=1,
        id=1,
        rewards={
            [103026]=5,
            [103027]=5,
            [202007]=1,
            [203007]=1
        }
    },
    {
        activity_id=1000,
        child_id=2,
        day=2,
        id=2,
        rewards={
            [20010]=1,
            [201007]=1,
            [204007]=1
        }
    },
    {
        activity_id=1000,
        child_id=3,
        day=3,
        id=3,
        rewards={
            [101102]=1,
            [103027]=5
        }
    }
}


return activity_sign_in
