--activity_timed_reward_cfg.lua
--source: 12_activity_timed_reward_定时投放奖励配置.xlsx
--luacheck: ignore 631

--导出配置内容
local activity_timed_reward = {
    {
        activity_id=1010,
        child_id=1,
        id=1,
        rewards={
            [103026]=5,
            [103027]=2
        },
        show_time='09:00-14:00',
        times={
            [0]='09:00:00',
            [1]='14:00:00'
        }
    },
    {
        activity_id=1010,
        child_id=2,
        id=2,
        rewards={
            [103026]=5,
            [103027]=2
        },
        show_time='18:00-23:00',
        times={
            [0]='18:00:00',
            [1]='23:00:00'
        }
    }
}


return activity_timed_reward
