--npc_interact_main_cfg.lua
--source: 3_npc_interact_main_NPC一级交互表.xlsx
--luacheck: ignore 631

--导出配置内容
local npc_interact_main = {
    {
        enum_key='JUMP_SYSTEM',
        id=1
    },
    {
        enum_key='OPEN_BRANCH',
        id=2
    },
    {
        enum_key='ONCE_DIALOG',
        id=3
    },
    {
        enum_key='ONCE_FITMENT',
        id=4
    }
}


return npc_interact_main
