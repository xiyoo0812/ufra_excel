--tutorial_video_group_cfg.lua
--source: 0_tutorial_video_group_视频新手引导.xlsx
--luacheck: ignore 631

--导出配置内容
local tutorial_video_group = {
    {
        id=1,
        name='LC_label_ui_tutorial_type',
        res='UI_Guide_Guide_0_Icon'
    },
    {
        id=2,
        name='LC_label_ui_guide_video_parent_title_1',
        res='UI_Guide_Basic_0_Icon'
    },
    {
        id=3,
        name='LC_label_ui_guide_video_parent_title_2',
        res='UI_Guide_Basic_0_Icon'
    },
    {
        id=6,
        name='LC_label_ui_guide_video_parent_title_3',
        res='UI_Guide_Box_0_Icon'
    },
    {
        id=5,
        name='LC_label_ui_guide_video_parent_title_4',
        res='UI_Guide_Construct_0_Icon'
    },
    {
        id=4,
        name='LC_label_ui_guide_video_parent_title_5',
        res='UI_Guide_Basic_0_Icon'
    },
    {
        id=7,
        name='LC_label_ui_guide_video_parent_title_6',
        res='UI_Guide_Basic_0_Icon'
    }
}


return tutorial_video_group
