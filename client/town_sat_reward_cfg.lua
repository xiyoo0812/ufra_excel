--town_sat_reward_cfg.lua
--source: 6_town_sat_城镇满意度.xlsx
--luacheck: ignore 631

--导出配置内容
local town_sat_reward = {
    {
        arg1=100,
        id=101,
        name='LC_label_ui_Satisfaction_reward2',
        rewards={
            [130002]=10
        },
        type=1
    },
    {
        arg1=60,
        arg2=12,
        id=102,
        name='LC_label_ui_Satisfaction_reward1',
        rewards={
            [103027]=10
        },
        type=2
    }
}


return town_sat_reward
