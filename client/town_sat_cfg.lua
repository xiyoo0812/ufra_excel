--town_sat_cfg.lua
--source: 6_town_sat_城镇满意度.xlsx
--luacheck: ignore 631

--导出配置内容
local town_sat = {
    {
        effect_icons='UI_Satisfaction_img_Architecture_1,UI_Satisfaction_img_Buff_2',
        effect_ids={
            30000002,
            30000004
        },
        effect_keys='LC_label_ui_Satisfaction_effectNeg2,LC_label_ui_Satisfaction_effectNeg1',
        end_value=20,
        id=1,
        sat_lv=1,
        start_value=0
    },
    {
        end_value=70,
        id=2,
        sat_lv=2,
        start_value=20
    },
    {
        effect_icons='UI_Satisfaction_img_Architecture_2,UI_Satisfaction_img_Buff_3',
        effect_ids={
            30000001,
            30000003
        },
        effect_keys='LC_label_ui_Satisfaction_effectPos2,LC_label_ui_Satisfaction_effectPos1',
        end_value=100,
        id=3,
        sat_lv=3,
        start_value=70
    }
}


return town_sat
