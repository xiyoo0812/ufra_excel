--player_interact_main_cfg.lua
--source: 2_player_interact_main_角色社交动作表.xlsx
--luacheck: ignore 631

--导出配置内容
local player_interact_main = {
    {
        actionName='Cheer',
        icon='UI_Emoticon_img_Hello_2',
        id=1,
        name='LC_label_ui_Social_Actions_Say_Hello'
    },
    {
        actionName='Congrats',
        icon='UI_Emoticon_img_Congratulation_2',
        id=2,
        name='LC_label_ui_Social_Actions_Congratulations'
    },
    {
        actionName='Bow',
        icon='UI_Emoticon_img_Dow_2',
        id=3,
        name='LC_label_ui_Social_Actions_Bow'
    },
    {
        actionName='Applause',
        icon='UI_Emoticon_img_Handclap_2',
        id=4,
        name='LC_label_ui_Social_Actions_Handclap'
    },
    {
        actionName='Awkward',
        icon='UI_Emoticon_img_Awkward_2',
        id=5,
        name='LC_label_ui_Social_Actions_Awkward'
    },
    {
        actionName='Helpless',
        icon='UI_Emoticon_img_Shruggie_2',
        id=6,
        name='LC_label_ui_Social_Actions_Let_go'
    },
    {
        actionName='Shake',
        icon='UI_Emoticon_img_Shakehead_2',
        id=7,
        name='LC_label_ui_Social_Actions_Shake'
    },
    {
        actionName='Salute',
        icon='UI_Emoticon_img_Salute_2',
        id=8,
        name='LC_label_ui_Social_Actions_Salute'
    },
    {
        actionName='Photo1',
        icon='UI_Emoticon_img_Ye_2',
        id=9,
        name='LC_label_ui_Social_Actions_Photo1'
    },
    {
        actionName='Photo2',
        icon='UI_Emoticon_img_Chok_2',
        id=10,
        name='LC_label_ui_Social_Actions_Photo2'
    },
    {
        actionName='Photo3',
        icon='UI_Emoticon_img_Akimbo_2',
        id=11,
        name='LC_label_ui_Social_Actions_Photo3'
    }
}


return player_interact_main
