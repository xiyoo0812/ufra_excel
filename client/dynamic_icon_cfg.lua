--dynamic_icon_cfg.lua
--source: 7_dynamic_icon_地图非实体类动态图标配置.xlsx
--luacheck: ignore 631

--导出配置内容
local dynamic_icon = {
    {
        icon='UI_Map_Task2_Img',
        id='task_main',
        type='Task'
    },
    {
        icon='UI_Map_Task4_Img',
        id='task_branch',
        type='Task'
    },
    {
        icon='UI_Map_Task6_Img',
        id='task_order',
        type='Task'
    },
    {
        icon='UI_Map_Task_Img',
        id='task_main_accept',
        type='Task'
    },
    {
        icon='UI_Map_Task3_Img',
        id='task_branch_accept',
        type='Task'
    },
    {
        icon='UI_Map_Task5_Img',
        id='task_order_accept',
        type='Task'
    },
    {
        icon='I_Map_img_MissionArea_00',
        id='task_main_area',
        type='TaskArea'
    },
    {
        icon='I_Map_img_MissionArea_01',
        id='task_branch_area',
        type='TaskArea'
    },
    {
        icon='I_Map_icon_Player_00',
        id='player',
        type='Player'
    },
    {
        icon='I_Main_img_MapView_0',
        id='view',
        type='View'
    }
}


return dynamic_icon
