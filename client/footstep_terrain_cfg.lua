--footstep_terrain_cfg.lua
--source: 99_音效.xlsx
--luacheck: ignore 631

--导出配置内容
local footstep_terrain = {
    {
        id=0,
        switch_name='grass'
    },
    {
        id=1,
        switch_name='rock'
    },
    {
        id=2,
        switch_name='grass'
    },
    {
        id=3,
        switch_name='mud'
    },
    {
        id=4,
        switch_name='grass'
    },
    {
        id=5,
        switch_name='grass'
    },
    {
        id=6,
        switch_name='mud'
    },
    {
        id=7,
        switch_name='mud'
    },
    {
        id=8,
        switch_name='rock'
    },
    {
        id=9,
        switch_name='mud'
    },
    {
        id=10,
        switch_name='grass'
    },
    {
        id=11,
        switch_name='grass'
    },
    {
        id=12,
        switch_name='mud'
    },
    {
        id=13,
        switch_name='mud'
    },
    {
        id=14,
        switch_name='mud'
    },
    {
        id=15,
        switch_name='sand'
    }
}


return footstep_terrain
