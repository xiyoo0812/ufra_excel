--map_icon_lod_cfg.lua
--source: 7_map_icon_lod_地图视距调整参数.xlsx
--luacheck: ignore 631

--导出配置内容
local map_icon_lod = {
    {
        id=0,
        max_scale=1,
        min_scale=0
    },
    {
        id=1,
        max_scale=1,
        min_scale=0.78
    },
    {
        id=2,
        max_scale=0.75,
        min_scale=0.4
    },
    {
        id=3,
        max_scale=0.4,
        min_scale=0
    }
}


return map_icon_lod
