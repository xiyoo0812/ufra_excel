--world_icon_cfg.lua
--source: 7_world_icon_地图非实体类图标静态配置.xlsx
--luacheck: ignore 631

--导出配置内容
local world_icon = {
    {
        id='home',
        name='Home',
        pos={
            35334,
            22071,
            -23681
        },
        type='AreaText'
    }
}


return world_icon
