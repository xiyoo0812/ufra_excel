--recruit_qup_cfg.lua
--source: 3_recruit_pool_招募表.xlsx
--luacheck: ignore 631

--导出配置内容
local recruit_qup = {
    {
        count=1,
        id=1,
        up=-10
    },
    {
        count=2,
        id=2,
        up=-10
    },
    {
        count=3,
        id=3,
        up=-10
    },
    {
        count=4,
        id=4,
        up=-10
    },
    {
        count=5,
        id=5,
        up=-10
    },
    {
        count=6,
        id=6,
        up=-10
    },
    {
        count=7,
        id=7,
        up=-10
    },
    {
        count=8,
        id=8,
        up=-10
    },
    {
        count=9,
        id=9,
        up=-10
    },
    {
        count=10,
        id=10,
        up=-10
    },
    {
        count=11,
        id=11,
        up=-10
    },
    {
        count=12,
        id=12,
        up=-10
    },
    {
        count=13,
        id=13,
        up=3
    },
    {
        count=14,
        id=14,
        up=7
    },
    {
        count=15,
        id=15,
        up=12
    },
    {
        count=16,
        id=16,
        up=18
    },
    {
        count=17,
        id=17,
        up=25
    },
    {
        count=18,
        id=18,
        up=33
    },
    {
        count=19,
        id=19,
        up=42
    },
    {
        count=20,
        id=20,
        up=52
    },
    {
        count=21,
        id=21,
        up=63
    },
    {
        count=22,
        id=22,
        up=75
    },
    {
        count=23,
        id=23,
        up=88
    },
    {
        count=24,
        id=24,
        up=100
    },
    {
        count=25,
        id=25,
        up=100
    },
    {
        count=26,
        id=26,
        up=100
    },
    {
        count=27,
        id=27,
        up=100
    },
    {
        count=28,
        id=28,
        up=100
    },
    {
        count=29,
        id=29,
        up=100
    },
    {
        count=30,
        id=30,
        up=100
    },
    {
        count=31,
        id=31,
        up=100
    },
    {
        count=32,
        id=32,
        up=100
    },
    {
        count=33,
        id=33,
        up=100
    },
    {
        count=34,
        id=34,
        up=100
    },
    {
        count=35,
        id=35,
        up=100
    },
    {
        count=36,
        id=36,
        up=100
    },
    {
        count=37,
        id=37,
        up=100
    },
    {
        count=38,
        id=38,
        up=100
    },
    {
        count=39,
        id=39,
        up=100
    },
    {
        count=40,
        id=40,
        up=100
    },
    {
        count=41,
        id=41,
        up=100
    },
    {
        count=42,
        id=42,
        up=100
    },
    {
        count=43,
        id=43,
        up=100
    },
    {
        count=44,
        id=44,
        up=100
    },
    {
        count=45,
        id=45,
        up=100
    },
    {
        count=46,
        id=46,
        up=100
    },
    {
        count=47,
        id=47,
        up=100
    },
    {
        count=48,
        id=48,
        up=100
    },
    {
        count=49,
        id=49,
        up=100
    },
    {
        count=50,
        id=50,
        up=100
    },
    {
        count=51,
        id=51,
        up=100
    },
    {
        count=52,
        id=52,
        up=100
    },
    {
        count=53,
        id=53,
        up=100
    },
    {
        count=54,
        id=54,
        up=100
    },
    {
        count=55,
        id=55,
        up=100
    },
    {
        count=56,
        id=56,
        up=100
    },
    {
        count=57,
        id=57,
        up=100
    },
    {
        count=58,
        id=58,
        up=100
    },
    {
        count=59,
        id=59,
        up=100
    },
    {
        count=60,
        id=60,
        up=100
    },
    {
        count=61,
        id=61,
        up=100
    },
    {
        count=62,
        id=62,
        up=100
    },
    {
        count=63,
        id=63,
        up=100
    },
    {
        count=64,
        id=64,
        up=100
    },
    {
        count=65,
        id=65,
        up=100
    },
    {
        count=66,
        id=66,
        up=100
    }
}


return recruit_qup
