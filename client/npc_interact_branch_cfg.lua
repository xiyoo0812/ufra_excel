--npc_interact_branch_cfg.lua
--source: 3_npc_interact_branch_NPC二级交互表.xlsx
--luacheck: ignore 631

--导出配置内容
local npc_interact_branch = {
    {
        id=1,
        type='DailyDialog',
        unlock_like=0,
        unlock_lv=1
    },
    {
        id=2,
        type='JumpSystem',
        unlock_like=0,
        unlock_lv=1
    },
    {
        id=3,
        type='SendGift',
        unlock_like=1,
        unlock_lv=0
    },
    {
        id=4,
        type='Task',
        unlock_like=0,
        unlock_lv=1
    },
    {
        id=5,
        type='Fitment',
        unlock_like=0,
        unlock_lv=1
    }
}


return npc_interact_branch
