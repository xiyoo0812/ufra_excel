--npc_affinity_level_cfg.lua
--source: 3_npc_affinity_level_NPC好感度等级.xlsx
--luacheck: ignore 631

--导出配置内容
local npc_affinity_level = {
    {
        affinity=50,
        id=1,
        level=1,
        name='LC_attribute_att_30'
    },
    {
        affinity=100,
        id=2,
        level=2,
        name='LC_attribute_att_31'
    },
    {
        affinity=500,
        id=3,
        level=3,
        name='LC_attribute_att_32'
    },
    {
        affinity=1000,
        id=4,
        level=4,
        name='LC_attribute_att_33'
    },
    {
        affinity=2000,
        id=5,
        level=5,
        name='LC_attribute_att_34'
    },
    {
        affinity=5000,
        id=6,
        level=6,
        name='LC_attribute_att_35'
    },
    {
        affinity=8000,
        id=7,
        level=7,
        name='LC_attribute_att_36'
    },
    {
        affinity=8000,
        id=8,
        level=8,
        name='LC_attribute_att_37'
    }
}


return npc_affinity_level
