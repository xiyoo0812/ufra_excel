--condition_cfg.lua
--source: 5_condition_任务条件定义表.xlsx
--luacheck: ignore 631

--导出配置内容
local condition = {
    {
        enum_key='LEVEL',
        id=1,
        name='等级'
    },
    {
        enum_key='BUILDING',
        id=2,
        name='建筑'
    },
    {
        enum_key='TOWN',
        id=3,
        name='城镇等级'
    },
    {
        enum_key='TASK',
        id=4,
        name='任务'
    },
    {
        enum_key='NPC',
        id=5,
        name='NPC'
    },
    {
        enum_key='CHAIN',
        id=6,
        name='任务链'
    },
    {
        enum_key='ASSIGN',
        id=7,
        name='派驻'
    },
    {
        enum_key='ASSIGN_GROUP',
        id=8,
        name='派驻npc到某个建筑组'
    },
    {
        enum_key='NPC_LIKING',
        id=9,
        name='npc好感等级'
    },
    {
        enum_key='FINISH_TARGET',
        id=10,
        name='完成目标'
    },
    {
        enum_key='NPC_COUNT',
        id=11,
        name='npc数量'
    },
    {
        enum_key='RESIDENT_NUM',
        id=12,
        name='居民数量'
    },
    {
        enum_key='GROUP_COUNT',
        id=13,
        name='建筑组数量'
    }
}


return condition
