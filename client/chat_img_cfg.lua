--chat_img_cfg.lua
--source: 15-chat_img_聊天图片表.xlsx
--luacheck: ignore 631

--导出配置内容
local chat_img = {
    {
        icon_name='UI_Emoticon_img_Anger_1',
        icon_str='/emoji0',
        id=1000
    },
    {
        icon_name='UI_Emoticon_img_Decent_1',
        icon_str='/emoji1',
        id=1001
    },
    {
        icon_name='UI_Emoticon_img_Grievance_1',
        icon_str='/emoji2',
        id=1002
    },
    {
        icon_name='UI_Emoticon_img_Serious_1',
        icon_str='/emoji3',
        id=1003
    },
    {
        icon_name='UI_Emoticon_img_Smile_1',
        icon_str='/emoji4',
        id=1004
    },
    {
        icon_name='UI_Emoticon_img_Awkward_3',
        icon_str='/emoji5',
        id=1005
    },
    {
        icon_name='UI_Emoticon_img_Complacent_1',
        icon_str='/emoji6',
        id=1006
    },
    {
        icon_name='UI_Emoticon_img_Dull_1',
        icon_str='/emoji7',
        id=1007
    },
    {
        icon_name='UI_Emoticon_img_Shy_1',
        icon_str='/emoji8',
        id=1008
    },
    {
        icon_name='UI_Emoticon_img_Sleep_1',
        icon_str='/emoji9',
        id=1009
    }
}


return chat_img
