--copy_followers_cfg.lua
--source: 9_copy_副本表.xlsx
--luacheck: ignore 631

--导出配置内容
local copy_followers = {
    {
        copy_id=1,
        dir={
            0,
            8419,
            0
        },
        id=1,
        name='洛根-跟随',
        pos={
            -2695,
            1893,
            533
        },
        proto_id=30001
    },
    {
        copy_id=1,
        dir={
            0,
            8944,
            0
        },
        id=2,
        name='艾弗里-跟随',
        pos={
            -2546,
            1893,
            -143
        },
        proto_id=30002
    },
    {
        copy_id=2,
        dir={
            0,
            -6211,
            0
        },
        id=3,
        name='洛根-跟随',
        pos={
            8202,
            4093,
            8321
        },
        proto_id=30001
    },
    {
        copy_id=2,
        dir={
            0,
            -6211,
            0
        },
        id=4,
        name='艾弗里-跟随',
        pos={
            8533,
            4093,
            8659
        },
        proto_id=30002
    }
}


return copy_followers
