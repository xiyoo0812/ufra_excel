--push_notif_cfg.lua
--source: 17-推送通知配置表.xlsx
--luacheck: ignore 631

--导出配置内容
local push_notif = {
    {
        content='LC_label_ui_Push_Notif_100001',
        id=100001,
        title='LC_label_ui_Push_notif_reward_title',
        type=0
    },
    {
        content='LC_label_ui_Push_Notif_100001',
        id=100002,
        title='LC_label_ui_Push_notif_reward_title',
        type=0
    },
    {
        content='LC_label_ui_Push_Notif_100100',
        id=100100,
        title='LC_label_ui_Push_notif_reward_title',
        type=1
    },
    {
        content='LC_label_ui_Push_Notif_100200',
        id=100200,
        title='LC_label_ui_Push_notif_activity_title',
        type=1
    },
    {
        content='LC_label_ui_Push_notif_activity_feedbacktext',
        id=100300,
        title='LC_label_ui_Push_notif_activity_feedbacktitle',
        type=1
    },
    {
        content='LC_label_ui_Push_notif_activity_feedbacktext',
        id=100301,
        title='LC_label_ui_Push_notif_activity_feedbacktitle',
        type=1
    },
    {
        content='LC_label_ui_Push_notif_activity_feedbacktext',
        id=100302,
        title='LC_label_ui_Push_notif_activity_feedbacktitle',
        type=1
    }
}


return push_notif
