--client_reward_cfg.lua
--source: 14_client_reward_客户端奖励表.xlsx
--luacheck: ignore 631

--导出配置内容
local client_reward = {
    {
        desc='社区分享奖励',
        enable=1,
        id=1000,
        limit_count=1,
        name='社区分享奖励',
        rewards={
            [100001]=1000,
            [101073]=1
        }
    }
}


return client_reward
