--activity_item_exchange_cfg.lua
--source: 12_activity_item_exchange_物品兑换表.xlsx
--luacheck: ignore 631

--导出配置内容
local activity_item_exchange = {
    {
        activity_id=1020,
        child_id=1,
        costs={
            [100001]=50
        },
        id=1,
        max_count=1,
        obtains={
            [100002]=60
        }
    },
    {
        activity_id=1020,
        child_id=2,
        costs={
            [100001]=200
        },
        id=2,
        max_count=1,
        obtains={
            [100002]=60
        }
    }
}


return activity_item_exchange
