--bait_item_cfg.lua
--source: 7_fishing_鱼饵表.xlsx
--luacheck: ignore 631

--导出配置内容
local bait_item = {
    {
        drop_id=2013,
        id=109001,
        itemId=109001,
        name='1级鱼饵'
    },
    {
        drop_id=2014,
        id=109002,
        itemId=109002,
        name='2级鱼饵'
    },
    {
        drop_id=2015,
        id=109003,
        itemId=109003,
        name='3级鱼饵'
    }
}


return bait_item
