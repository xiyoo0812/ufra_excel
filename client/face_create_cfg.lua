--face_create_cfg.lua
--source: 1_face_create_捏脸表.xlsx
--luacheck: ignore 631

--导出配置内容
local face_create = {
    {
        config_female={
            101,
            102,
            103,
            104,
            105
        },
        config_male={
            101,
            102,
            103,
            104,
            105
        },
        id=1001,
        title='LC_label_ui_Custom_Skin_Colour'
    },
    {
        config_female={
            109
        },
        config_male={
            106
        },
        id=1002,
        title='LC_label_ui_Custom_Face'
    }
}


return face_create
