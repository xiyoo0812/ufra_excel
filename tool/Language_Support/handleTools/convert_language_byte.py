import sys
import os
import xlrd
import xlwt
from xlutils.filter import process, XLRDReader, XLWTWriter
import argparse
import win32com.client as win32
import datetime

from os.path import join
curpath = os.path.dirname(os.path.abspath(sys.argv[0]))
root_path = join(sys.path[0], "..", "..")
src_folder = join(root_path, "excel", "language")

# print(curpath)
# print(root_path)
# print(src_folder)

# 翻译表格，对应翻译空间、翻译任务
sheetNames = ["button", "equip", "item", "type", "attribute", "label", "resource", "tips", "dialogue", "mail", "recruit", "npc", "building", "structure", "order", "username", "monster", "skill"]
titleNames = ["Section", "Surfix", "ID", "Key", "Remark", "Length Limit", "en", "cn", "kr", "jp", "fr", "de", "ru", "sp", "po", "it", "nl", "tr", "id", "pls", "thai", "zh", "fa", "ro", "ar", "can"]
skipFormulaSheetNames = ["dialogue", "mail"]

def eufuala2byte(fromPath, toPath):
	"""
	将单个文件从项目格式转为字节上传的表格的模式
	fromPath: 项目的翻译表原表
	toPath: 项目转换为的用于上传的表
	"""
	if not os.path.exists(fromPath) :
		print("未找到要转换的目标xlsx文件"+str(fromPath))

	print("fromPath:"+fromPath)
	sourceXlsx = xlrd.open_workbook(fromPath)
	targetXlsx = xlwt.Workbook()
	
	sourceSheetNames = sourceXlsx.sheet_names()
	for sheetName in sheetNames:
		if not sheetName in sourceSheetNames:
			print("翻译原表["+fromPath+"]缺少sheet页："+sheetName)
			continue
		
		# 获得原表sheet
		sourceSheet = sourceXlsx.sheet_by_name(sheetName)		
		sheet_rows = sourceSheet.nrows
		sheet_cols = sourceSheet.ncols

		targetSheet = targetXlsx.add_sheet(sheetName, True)
		for row in range(sheet_rows):
			# 遍历除表头行的全部行
			for col in range(3, sheet_cols):
				# 遍历Key开头的全部行
				curCellValue = sourceSheet.cell_value(row, col)
				targetSheet.write(row, col-3, label=curCellValue)
	targetXlsx.save(toPath)
	print("转换 eufula -> byte 格式")
	
def eufuala2byteList(fromPath, toPath):
	"""
	将单个文件或一个文件夹中的全部文件转换为字节上传的表格的模式
	fromPath: 项目的翻译表原表，或其所在的文件夹
	toPath：项目转换后需要存放到的位置
	"""
	targetXlsxs = []

	isDir = os.path.isdir(fromPath)
	if isDir :
		fileList = list(os.listdir(fromPath))
		for file in fileList:
			if (file.endswith(".xls")  or  file.endswith(".xlsx")) and ( file.find("~") < 0 ) :
				targetXlsxs.append(os.path.join(fromPath, file))
	else :
		targetXlsxs.append(fromPath)

	if not os.path.isdir(toPath) :
		toPath, _ = os.path.split(toPath)
	for xlsx in targetXlsxs :
		_, curToPath = os.path.split(xlsx)
		eufuala2byte(xlsx, os.path.join(toPath, curToPath))	

def byte2eufuala(fromPath, toPath, templatePath):
	"""
	将单个文件从字节下载后的表格格式转变为项目组中使用的翻译表格式
	fromPath: 下载下来的翻译表路径
	toPath: 要保存到的位置
	"""
	if not os.path.exists(fromPath):
		print("未找到要转换的目标xlsx文件"+str(fromPath))
		
	if not os.path.exists(templatePath) :
		print("未找到样式模板xlsx文件"+str(templatePath))

	sourceXlsx = xlrd.open_workbook(fromPath, formatting_info=True)
	targetXlsx = xlwt.Workbook(encoding='utf8')
	xlwt.Worksheet
	for index in range(len(sheetNames)):
		sheet = sourceXlsx.sheet_by_name(sheetNames[index]) # 按顺序
		if sheet is None:
			continue

		# 写入source中有的内容
		targetSheet = targetXlsx.add_sheet(sheet.name, cell_overwrite_ok=True)
		for row in range(sheet.nrows): #长度是没有前3列的
			writeColIndex = 3
			row0 = sheet.row_values(0)
			for colName in titleNames:
				if not colName in row0 :
					continue
				readCol = row0.index(colName)

				style = get_cell_style(templatePath, row, writeColIndex)
				cell = sheet.cell_value(row, readCol)
				targetSheet.write(row, writeColIndex, cell, style)
				
				writeColIndex += 1
			# for col, cell in enumerate(sheet.row_values(row)):
			# 	style = get_cell_style(templatePath, row, col + 3)
			# 	targetSheet.write(row, col + 3, cell, style)
	
		# 写入剩余空列
		for row in range(sheet.nrows):
			for readCol in range(sheet.ncols+3, len(titleNames)):
				style = get_cell_style(templatePath, row, readCol)

				cell_content = ""
				if row == 0 :
					cell_content = titleNames[readCol]
				targetSheet.write(row, readCol, cell_content, style)

		# 填充前3列内容
		for row in range(sheet.nrows):
			for readCol in range(3):
				style = get_cell_style(templatePath, row, readCol)

				cell_value = titleNames[readCol]
				if row > 0 :
					key_cell_value = sheet.cell_value(row, 0)
					cell_value = key_cell_value.split('_')[1+readCol]
					if readCol == 2:
						cell_value = '_'.join(key_cell_value.split('_')[3:])
				targetSheet.write(row, readCol, str(cell_value), style)

		# 设置Key列为公式
		if not sheet.name in skipFormulaSheetNames :
			for row in range(1, sheet.nrows):
				key_col = 3
				style = get_cell_style(templatePath, row, key_col)
				formula = xlwt.Formula('"LC_"&A{0}&"_"&B{0}&"_"&C{0}'.format(row+1))
				targetSheet.write(row, key_col, label=formula, style=style)

		copy_sheet_wh(templatePath, targetSheet)

	savetoPath = toPath + (".-{0}.xls".format(datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")))
	print(savetoPath)
	targetXlsx.save(savetoPath)
	print("转换 byte -> eufula 格式")

def byte2eufualaList(fromPath, toPath, templatePath):
	"""
	将单个文件从字节下载后的表格格式转变为项目组中使用的翻译表格式，允许指定的是文件所在的文件夹
	fromPath: 下载下来的翻译表路径，或其所在的文件夹路径
	toPath: 要保存到的位置
	"""

	convert_xlsx2xls(fromPath)

	targetXlsxs = []
	isDir = os.path.isdir(fromPath)
	if isDir :
		fileList = list(os.listdir(fromPath))
		for file in fileList:
			if file.endswith(".xls") and ( file.find("~") < 0 ) :
				targetXlsxs.append(os.path.join(fromPath, file))
	else :
		targetXlsxs.append(fromPath)

	if not os.path.isdir(toPath) :
		toPath, _ = os.path.split(toPath)
	for xlsx in targetXlsxs :
		_, curToPath = os.path.split(xlsx)
		byte2eufuala(xlsx, os.path.join(toPath, curToPath), templatePath)	

def convert_xlsx2xls(targetPath):
	"""
	将xlsx文件转为xls文件
	targetPath: 目标文件或目标文件夹路径
	"""
	isDir = os.path.isdir(targetPath)
	if isDir :
		fileList = list(os.listdir(targetPath))
		for file in fileList:
			if file.endswith(".xlsx") and ( file.find("~") < 0 ) :
				filePath = os.path.join(targetPath, file)
				xlsPath = filePath[0:-1]
				xlsx_to_xls(filePath, xlsPath, True)
	else :
		if targetPath.endswith(".xlsx") :
			xlsPath = targetPath[0:-1]
			xlsx_to_xls(targetPath, xlsPath, True)

def copy_sheet_wh(templateSheetPath, targetSheet):
	"""
	从templateSheet中复制单元格宽高
	"""
	templateXlsx = xlrd.open_workbook(templateSheetPath, formatting_info=True)
	templateSheet = templateXlsx.sheet_by_name(templateXlsx.sheet_names()[0])

	# for row in range(templateSheet.nrows) :
	# 	templateSheetRow = 1 if row > 0 else 0
	# 	colHeight = templateSheet.
	# 	targetSheet.row(row).height = colHeight

	for col in range(templateSheet.ncols) :
		colWidth = templateSheet.computed_column_width(col)
		targetSheet.col(col).width = colWidth

templateSheet = None
def get_cell_style(templateSheetPath, targetRow, targetCol):
	"""
	从模板xls中获取对应targetRow和targetCol的cell的格式
	templateSheetPath: 格式模板xls文件的路径
	targetRow: 要获取的是要写入到的xls文件的第几行
	targetCol: 要获取的是要写入到的xls文件的第几列
	"""
	global templateSheet
	global styleList

	if templateSheet is None:
		templateXlsx = xlrd.open_workbook(templateSheetPath, formatting_info=True)
		templateSheet = templateXlsx.sheet_by_name(templateXlsx.sheet_names()[0])
		w = XLWTWriter()
		process(XLRDReader(templateXlsx, 'unknow.xls'), w)
		styleList = w.style_list

	templateSheetRow = 1 if targetRow > 0 else 0
	templateSheetCol = targetCol

	style = styleList[templateSheet.cell_xf_index(templateSheetRow, templateSheetCol)]
	return style

def xlsx_to_xls(fname, export_name, delete_flag=True):
    """
    将xlsx文件转化为xls文件
    :param fname: 传入待转换的文件路径(可传绝对路径，也可传入相对路径，都可以)
    :param export_name: 传入转换后到哪个目录下的路径(可传绝对路径，也可传入相对路径，都可以)
    :param delete_flag: 转换成功后，是否删除原来的xlsx的文件,默认删除 布尔类型
    :return:    无返回值
    """
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    excel.Visible = False
    excel.DisplayAlerts = False
    absolute_path = os.path.join(os.path.dirname(os.path.abspath(fname)), os.path.basename(fname))
    save_path = os.path.join(os.path.dirname(os.path.abspath(export_name)), os.path.basename(export_name))
    wb = excel.Workbooks.Open(absolute_path)
    wb.SaveAs(save_path, FileFormat=56)  # FileFormat = 51 is for .xlsx extension
    wb.Close()  # FileFormat = 56 is for .xls extension
    excel.Application.Quit()
    if delete_flag:
        os.remove(absolute_path)

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--type', default='upload', required=False)

	# 上传
	parser.add_argument('--upload_source', default='../upload', required=False)
	parser.add_argument('--upload_handle', default='../uploadHandlePath', required=False)

	# 下载
	parser.add_argument('--download_handle', default='../downloadHandlePath', required=False)
	parser.add_argument('--download_output', default='../download')

	# 模板
	curFolder = os.path.dirname(os.path.abspath(__file__))
	parser.add_argument('--template_sheet', default=os.path.join(curFolder,'./template.xls'))

	args = parser.parse_args()
	
	# print(args.type)
	# print(args.upload_source)
	# print(args.upload_handle)

	# print(args.type)
	# print(args.download_handle)
	# print(args.download_output)
	
	if args.type == 'upload' :
		eufuala2byteList(args.upload_source, args.upload_handle)
	elif args.type == 'download' :
		byte2eufualaList(args.download_handle, args.download_output, args.template_sheet)
	else :
		print("指定了错误的工作类型【"+str(args['type'])+"】...")
		exit(1)

if __name__ == '__main__':
	main()