# 导入
1. **安装 [NodeJS](https://nodejs.org/download/release/v14.16.0/)**

2. **starling-xlsx-import-tool 根目录下执行 `npm install`，如果想要安装至全局，那么可以执行 `npm link`，无需执行 install 命令**

3. **配置文件 starling.xlsx.yml**

| 字段名称 | 类型 | 默认值 | 说明 |
| -- | -- | -- | -- |
| ak | string | - | Access Key |
| sk | string | - | Secret Key |
| projectId | number | - | 项目id |
| file | string | - | 上传文件读取路径，只支持excel相关类型文件，一次只支持导入一个文件 |
| keySheets | { key_column_name: { task: string, namespace: number[] } } | - | 任务名及key配置映射,详细见下面描述 |
| ignoreSheets | string[] | - | 忽略sheet，配置后，忽略同名sheet |
| includeSheets | string[] | - | 包含sheet，配置后，仅会导入该配置中的sheet |
| source | string | - | 源语言，需要存在「languages」映射语言中 |
| languages | { column_name: language } | - | 导入的目标语言，sheet 中列名与项目中目标语言映射 |
| lengthLimit | string | - | 最长字符数映射字段，不需要时可设置为'' |
| context | string | - | 注释映射字段，不需要时可设置为'' |

```yaml
# keySheets 相关配置说明举例
"keySheets": {
  # key 对应列，大小写不敏感。读取该列内容作为key，对应source和language中的 源语言 和 目标语言，该列单元格为空时会跳过
  "Key": {
    # 任务名称，任务不存在时会新创建，任务存在时会直接使用
    # %sheetName% 占位符，在创建任务时会进行替换，如： "%sheetName%_IOS" => "xxx_IOS" xxx 为具体的sheet名称
    "task": "%sheetName%_IOS",
    # 任务同步空间Id
    "namespaceId": [],
    # 新建空间名称
    "namespace": "%sheetName%",
    # 空间不存在时是否新增，设置为true后会忽略namespaceId配置
    "createNoneexistendSpace": true
  }
}
``` 

4. **可运行 `node index.js` 或 `npm start` 启动导入任务。如果已被安装至全局，可以执行 `starling-xlsx`**

5. **导入结果**
导入完成后会生成 logger 文件，包含：add 文案新增数量、update 文案更新数量、nodiff 无差异文案数量、fail 导入失败文案数量，以及导入结果校验报告文件url。
upload 目录中存放本次转换后的上传文件。

# 导出

> 目前仅支持导出项目下所有空间的文案

1. **配置文件 starling.xlsx.yml**

| 字段名称 | 类型 | 默认值 | 说明 |
| -- | -- | -- | -- |
| ak | string | - | Access Key |
| sk | string | - | Secret Key |
| projectId | number | - | 项目id |
| languages | { column_name: language } | - | 导入的目标语言，sheet 中列名与项目中目标语言映射 |
| lengthLimit | string | - | 最长字符数映射字段，不需要时可设置为'' |
| context | string | - | 注释映射字段，不需要时可设置为'' |
| output | string | - | 导出路径 |
| download | object | - | 导出相关配置 |

```yaml
# download 相关配置说明举例
"download": {
  # sheet 名称
  "sheetName": "%namespace%",
  # key 映射
  "key": "Key",
  # sheet 表头排序，为空时按照默认排序进行（key、source、length limit、context、lang）
  "headers": [],
  # sheet排序，为空时按照空间查询顺序进行排序
  "sheets": []
}
``` 

2. **可运行`node download.js`进行文件导出，如果已安装至全局，可运行`starling-xlsx download`**

