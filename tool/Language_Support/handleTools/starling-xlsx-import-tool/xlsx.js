const xlsx = require('xlsx');

const hideEmptyRows = false;

const abc = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

const getValue = (sheet, column, R) => sheet[`${column}${R}`];

const getSheetNames = (workbook) => workbook.SheetNames;

const getDesiredCells = (worksheet) => worksheet['!ref'];

const extractLetter = (str) => {
  const array = str.split(/[0-9]+/);
  return array[0];
}

const getData = (lastColRow, columns, headers, sheet, showNullProperties = false) => {
  const data = [];

  for (let R = 2; R <= lastColRow; R++) {
    const element = {};

    headers.forEach((header, index) => {
      const cellValue = getValue(sheet, columns[index], R);

      if (cellValue) {
        element[header] = cellValue.w ? cellValue.w : cellValue.v
      }
      else if (showNullProperties) {
        element[header] = null;
      }
    });
    if (Object.keys(element).length > 0) {
      data.push(element);
    }
  }

  return data;
}

const getColumnsAndHeaders = (worksheet, desired_cells) => {
  const cells = desired_cells.split(':');
  const lastCell = cells.length > 1 ? cells[1] : cells[0];
  const lastColLetter = extractLetter(lastCell);

  let iterator = 0;
  let accumulator = '';
  let accumulatorIterator = 0;
  const headers = [];
  const excelColumns = [];

  while (true) {

    const currentCell = `${accumulator}${abc[iterator++]}`;
    const cellHeader = worksheet[currentCell + 1];

    if (cellHeader) {
      headers.push(cellHeader.v)
      excelColumns.push(currentCell);
    }

    if (lastColLetter == currentCell) {
      return { headers: headers, excelColumns: excelColumns };
    }

    if (iterator >= abc.length) {
      const test = abc[accumulatorIterator++];
      iterator = 0;
      accumulator = test;
    }
  }
}

const getLastRowCol = (cells) => {
  const rows = cells.split(':');
  const lastColRow = rows.length > 1 ? rows[1] : rows[0];

  const lastColLetter = extractLetter(lastColRow);
  const array = lastColRow.split(lastColLetter);

  return Number(array[1]);
}

class XlsxReader {

  constructor(path) {
    this.workbook = xlsx.readFile(path);
  }

  getSheets() {
    return getSheetNames(this.workbook);
  }

  getHeaders() {
    return this.getSheets().map(name => {
      const sheet = this.workbook.Sheets[name];
      const desiredCells = getDesiredCells(sheet);
      return getColumnsAndHeaders(sheet, desiredCells).headers;
    });
  }

  getData() {
    const parsedXls = {};

    this.getSheets().forEach(name => {
      const sheet = this.workbook.Sheets[name];
      const desiredCells = getDesiredCells(sheet);
      const lastColRow = getLastRowCol(desiredCells);
      const columnsAndHeaders = getColumnsAndHeaders(sheet, desiredCells);

      const data = getData(lastColRow, columnsAndHeaders.excelColumns, columnsAndHeaders.headers, sheet);
      if (hideEmptyRows) {
        const finalData = [];
        data.forEach(element => {
          let isEmpty = true;
          columnsAndHeaders.headers.forEach(header => {
            if (element[header]) {
              isEmpty = false;
            }
          });
          if (!isEmpty) {
            finalData.push(element);
          }
        });

        parsedXls[name] = finalData;
      }
      else {
        parsedXls[name] = data;
      }
    });

    return parsedXls;
  }
}

module.exports = XlsxReader;