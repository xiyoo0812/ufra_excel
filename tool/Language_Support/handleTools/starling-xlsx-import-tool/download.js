const { getConfig } = require('./common/config');
const minimist = require('minimist');
const { getNamespaces, getSourceText } = require('./api');
const { loopSearch, asyncLoop, createXlsxWithSheets, resolve } = require('./common/utils');
const fs = require('fs');
const logger = require('./common/logger');

const args = minimist(process.argv.slice(2));

const config = getConfig(args.config, args, 'download');

const languageMap = Object.fromEntries(Object.entries(config.languages).map(([k, v]) => ([v, k])));

async function getNamespaceList() {
  const namespaces = await loopSearch(params => getNamespaces({ projectId: config.projectId, name: '', ...params }));

  return namespaces.map(({ id, namespace }) => ({ id, namespace }));
}

async function getNamespaceText(namespaceId) {
  const keyName = config.download?.key || 'keys';

  const text = await loopSearch(params => getSourceText({
    projectId: config.projectId,
    namespaceId,
    ...params,
  }));

  return text.map(({ lengthLimit, keyText, content, targetTexts, commentary }) => {
    const targetText = Object.fromEntries((targetTexts || []).map(({ lang, content }) => [lang, content]));

    const obj = {
      [keyName]: keyText,
      source: content,
      [config.lengthLimit || 'length limit']: lengthLimit,
      [config.context || 'context']: commentary,
      ...Object.fromEntries(Object.keys(languageMap).map(k => ([languageMap[k], targetText[k]]))),
    };

    if (config.download.headers?.length) {
      const _obj = {};

      config.download.headers.forEach(k => {
        _obj[k] = obj[k] || '';
      });

      return _obj;
    }

    if (config.download?.ignoreHeaders?.length) {
      const _obj = {};

      Object.keys(obj).filter(k => !config.download?.ignoreHeaders.includes(k)).forEach(k => {
        _obj[k] = obj[k] || '';
      });

      return _obj;
    }

    return obj;
  });
}


async function run() {
  const namespaces = await getNamespaceList();

  const list = [];

  await asyncLoop(namespaces.map(({ id, namespace }) => async () => {
    const text = await getNamespaceText(id);

    if (config.download?.sheetName) {
      list.push([config.download?.sheetName?.replace('%namespace%', namespace), text]);
      return;
    }

    list.push([namespace, text]);
  }));

  const buffer = createXlsxWithSheets(list, config.download?.sheets?.length ? config.download?.sheets : null);

  const output = resolve(process.cwd(), config.output);

  fs.writeFileSync(output, buffer);
}

try {
  run();
} catch (e) {
  logger.error(e);
}
