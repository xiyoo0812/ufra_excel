const YAML = require('yaml');
const fs = require('fs');
const Joi = require('joi');
const { checkConfigExist, resolve } = require('./utils');
const logger = require('./logger');

const Scheme = Joi.object({
  projectId: Joi.number().required(),
  ak: Joi.string().min(1).required().label('Access Key'),
  sk: Joi.string().min(1).required().label('Secret Key'),
  source: Joi.string().allow(''),
  keySheets: Joi.object(),
  languages: Joi.object(),
  file: Joi.string(),
  ignoreSheets: Joi.array().items(Joi.string()),
  includeSheets: Joi.array().items(Joi.string()),
  createLogFile: Joi.boolean(),
}).unknown(true);

const downloadScheme = Joi.object({
  projectId: Joi.number().required(),
  ak: Joi.string().min(1).required().label('Access Key'),
  sk: Joi.string().min(1).required().label('Secret Key'),
  languages: Joi.object(),
  output: Joi.string(),
  download: Joi.object(),
}).unknown(true);

let configCache;

function getConfig(configPath, extra = {}, type = 'upload') {
  if (configCache) {
    return configCache;
  }

  const filePath = resolve(process.cwd(), configPath || 'starling.xlsx.yml');

  checkConfigExist(true, filePath);

  const file = fs.readFileSync(filePath, 'utf-8');

  const config = YAML.parse(file);

  Object.assign(config, extra);

  let scheme = Scheme;

  if (type === 'download') {
    scheme = downloadScheme;
  }

  const validate = scheme.validate(config);

  if (validate.error && Array.isArray(validate.error.details)) {
    logger.error(validate.error.details.map(({ message }) => message).join('\n'));
    process.exit(1)
  }

  configCache = config;

  return configCache;
}

module.exports = {
  getConfig,
};