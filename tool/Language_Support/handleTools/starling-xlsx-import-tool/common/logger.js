const chalk = require('chalk');
const { tick, cross, warning, info } = require('figures');
const signale = require('signale');
const fs = require('fs');

const Logger = signale.Signale;
const _logger = [];

const options = {
  error: {
    badge: cross,
    color: 'red',
    label: 'error'
  },
  info: {
    badge: info,
    color: 'blue',
    label: 'info'
  },
  success: {
    badge: tick,
    color: 'green',
    label: 'success'
  },
  warn: {
    badge: warning,
    color: 'yellow',
    label: 'warning'
  },
  log: {
    badge: '',
    color: '',
    label: ''
  },
  fatal: {
    badge: cross,
    color: 'red',
    label: 'fatal'
  },
}

const extend = {
  stringify(data) {
    if (typeof data === 'object') {
      return JSON.stringify(data)
    } else {
      return data
    }
  },
  success(msg) {
    msg = this.stringify(msg);
    signale.success(chalk.green(msg));
    _logger.push(`success ${msg}`);
  },
  error(msg) {
    msg = this.stringify(msg);
    signale.error(chalk.red(msg));
    _logger.push(`error ${msg}`);
    // process.exit(1)
  },
  warn(msg) {
    msg = this.stringify(msg);
    signale.warn(chalk.yellow(msg));
    _logger.push(`warn ${msg}`);
  },
  info(msg) {
    msg = this.stringify(msg);
    signale.info(chalk.blue(msg));
    _logger.push(`info ${msg}`);
  },
  await(msg) {
    msg = this.stringify(msg);
    signale.await(chalk.blue(msg));
    _logger.push(`await ${msg}`);
  },
  fatal(msg) {
    msg = this.stringify(msg);
    signale.await(chalk.red(msg));
    _logger.push(`fatal ${msg}`);
  }
}

function output() {
  fs.writeFileSync('./logger', _logger.join('\n'));
}

let logger = new Logger(options)

logger = Object.assign(logger, extend, { output })

module.exports = logger
