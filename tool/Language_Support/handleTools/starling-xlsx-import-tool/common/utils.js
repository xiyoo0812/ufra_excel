const path = require('path');
const fs = require('fs');
const archiver = require('archiver');
const logger = require('./logger');
const { ConfigName } = require('./consts');
const XLSX = require('xlsx');


function convertPath(path) {
  if (path.indexOf('/') === 0) {
    return path.substring(1);
  }

  return path;
}

const resolve = (...$args) => {
  return path
    .resolve(
      ...$args.map((k, i) => (i === 0 ? k || '' : typeof k === 'string' ? convertPath(k) : ''))
    )
    .split(path.sep)
    .join('/');
};

function checkFileExist(file) {
  try {
    fs.accessSync(file, fs.constants.F_OK)
    return true
  } catch (e) {
    return false
  }
}

function checkConfigExist(exit = true, config) {
  const currentPath = process.cwd();

  const file = resolve(currentPath, ConfigName);

  console.log(config);

  const res = checkFileExist(config || file);

  if (res) {
    return true
  }

  if (exit) {
    logger.fatal(
      `Configuration file doesn't exist. Run the 'npm run init' to generate configuration skeleton`
    );

    process.exit(-1)
  } else {
    return false
  }
}


function createZip(data, name, source) {
  if (!Object.keys(data).length) {
    return false;
  }

  return new Promise(r => {
    const archive = archiver('zip')

    const output = fs.createWriteStream(resolve(__dirname, `../upload/${name || 'upload'}.zip`))

    archive.pipe(output)

    Object.entries(data).forEach(([locale, json]) => {
      archive.append(JSON.stringify(json, null, 2), { name: `${locale}/tmp.json` });

      if (locale === source) {
        archive.append(JSON.stringify(json, null, 2), { name: `source/tmp.json` });
      }
    });
    archive.finalize()

    archive.on('error', e => {
      logger.error(e.message)
    })

    output.on('close', () => {
      r(output)
    })
  })
}

function createXlsx(list, name) {
  if (!list?.length) {
    return false;
  }

  const buffer = XLSX.write(
    {
      SheetNames: ['sheet1'],
      Sheets: {
        sheet1: XLSX.utils.json_to_sheet(list)
      }
    },
    { type: 'buffer' }
  );

  fs.writeFileSync(resolve(__dirname, `../upload/${name || 'upload'}.xlsx`), buffer);
}

function createXlsxWithSheets(list, sheets) {
  if (!list?.length) {
    return false;
  }

  return XLSX.write({
    SheetNames: sheets || list.map(([name]) => name),
    Sheets: Object.fromEntries(list.map(([name, v]) => ([name, XLSX.utils.json_to_sheet(v)]))),
  }, {
    type: 'buffer'
  });
}

const asyncLoop = list => {
  const _list = list.slice()
  const item = _list.shift()

  if (!item) {
    return true
  }

  return Promise.resolve()
    .then(() => item())
    .then(() => asyncLoop(_list))
    .catch(e => {
      console.error(e);
      asyncLoop(_list);
    })
}

async function loopSearch(handle, limit = 100) {
  const res = await handle({ offset: 0, limit });
  if (res.total > limit) {
    return Promise.all(Array.from({ length: Math.ceil((res.total - limit) / limit) }).map((_, i) => {
      return handle({
        limit,
        offset: (i + 1) * limit,
      });
    })).then(list => {
      return list.reduce((acc, cur) => {
        if (cur?.data) {
          acc.push(...cur?.data);
        }

        return acc;
      }, [...res.data]);
    });
  }

  return res.data;
}

module.exports = {
  resolve,
  checkConfigExist,
  createZip,
  asyncLoop,
  createXlsx,
  loopSearch,
  createXlsxWithSheets,
};