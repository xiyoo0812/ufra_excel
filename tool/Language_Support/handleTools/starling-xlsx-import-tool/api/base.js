const logger = require('../common/logger')
const { Signer, Service } = require('@volcengine/openapi');

const qs = require('qs')
const fetch = require('node-fetch')
const { getConfig } = require('../common/config');

async function request({
  Action,
  url,
  method = 'GET',
  Version = '2021-05-21',
  region = 'cn-north-1',
  serviceName = 'i18n_console',
  data = {},
  headers = { 'Content-Type': 'application/json' },
  stream = null
}) {
  const { getConfig } = require('../common/config')
  const options = getConfig()

  let credentials = {
    accessKeyId: options.ak,
    secretKey: options.sk
  }
  let params = {}
  let body = {}

  if (method.toLocaleLowerCase() === 'post') {
    body = data
  } else {
    params = data
  }

  const obj = {
    Action,
    body,
    data,
    params: {
      ...params,
      Action,
      Version
    },
    query: params,
    headers,
    method,
    pathname: '/',
    region,
    Version
  }

  const signer = new Signer(obj, serviceName)

  signer.addAuthorization(credentials)

  const host = options.debug
    ? 'http://starling-boe-public.bytedance.net'
    : 'https://starling-public.zijieapi.com'

  const uri = `${host}${url}?${qs.stringify({
    Action,
    Version,
    ...params
  })}`

  if (stream) {
    data.append('file', stream)
  }

  if (!data.append) {
    if (method.toLocaleLowerCase() === 'post') {
      body = JSON.stringify(data)
    } else {
      body = null
    }
  }

  const res = await fetch(uri, {
    method,
    headers: {
      ...obj.headers,
      ...(options.headers || {}),
      'X-Bypass-Accesskey': credentials.accessKeyId,
      'x-starling-from': 'OpenApi'
    },
    body
  })

  try {
    const data = await res.json()

    if (data.code) {
      logger.error(`${res.headers.get('x-tt-logid')} ${data.message}`)
    }

    return data
  } catch (e) {
    const text = await res.text()
    logger.error(`${res.headers.get('x-tt-logid')} ${text}`)
  }
}

class I18n extends Service {
  constructor(options = {}) {
    super({
      ...options,
      defaultVersion: '2021-05-21',
      serviceName: 'i18n_openapi',
      timeout: 0
    })
  }

  async post(action, data) {
    const options = getConfig();
    return await this.request(action, data, {
      ...options,
      createOptions: {
        method: 'POST',
        contentType: "json",
      },
    });
  }

  async request(action, data, options) {
    if (!options) {
      options = getConfig()
    }

    this.setAccessKeyId(options.ak)
    this.setSecretKey(options.sk)

    options.debug && this.setHost('volcengineapi-boe.byted.org')

    const actionHandle = this.createAPI(action, options.createOptions)

    const res = await actionHandle(data, {
      headers: {
        ...(options.headers || {})
      }
    })

    if (res.ResponseMetadata && res.ResponseMetadata.Error) {
      logger.error(res.ResponseMetadata.Error.Message)
    }

    if (res.message && res.code !== 2006) {
      logger.error(`${res.code} ${res.message}`)
    }

    return res
  }
}

const i18nOpenAPI = new I18n()

module.exports = {
  request,
  i18nOpenAPI
}
