const { i18nOpenAPI, request } = require('./base');
const logger = require('../common/logger');
const fetch = require('node-fetch');
const Form = require('form-data');
const fs = require('fs');

const cache = {};

async function getLocale(projectId, languages = []) {
  const detail = await i18nOpenAPI.request('ProjectDetail', { projectId })

  if (Array.isArray(languages) && languages.length && cache[languages.join(',')]) {
    return cache[languages.join(',')]
  } else if (
    cache[projectId] &&
    Array.isArray(cache[projectId]) &&
    cache[projectId].length &&
    (!Array.isArray(languages) || !languages.length)
  ) {
    return cache[projectId]
  }

  const list = detail.data.targetLocales

  if (languages.length) {
    const langs = languages.filter(
      lang => !list.map(lang => lang.toLocaleLowerCase()).includes(lang.toLocaleLowerCase())
    )

    if (langs.length) {
      logger.error(`${langs.join('、')} are not project target language, please check.`)
    }

    return languages.map(lang =>
      list.find(item => item.toLocaleLowerCase() === lang.toLocaleLowerCase())
    )
  }

  if (Array.isArray(languages) && languages.length) {
    cache[languages.join(',')] = list
  } else {
    cache[projectId] = list
  }

  return list
}

async function getLocaleByConfig() {
  const res = await fetch(
    `https://lf3-cdn-tos.draftstatic.com/obj/ies.fe.starling/locale.json?_r=${Date.now()}`,
    { method: 'GET', timeout: 0 }
  )

  const listMap = await res.json()

  Object.keys(listMap).forEach(key => {
    if (!listMap[key.toLocaleLowerCase()]) {
      listMap[key.toLocaleLowerCase()] = listMap[key]
    }
  })

  return listMap
}

async function getTaskDetail(projectId, name, namespace) {
  const res = await request({
    url: '/openapi/project/tasks',
    method: 'GET',
    Action: 'ProjectTasks',
    data: {
      projectId,
      name,
      limit: 9999,
    }
  })
  try {
    const { data } = res;

    const id = data.filter(({ taskName, syncNamespaces }) => {
      return taskName === name; //&& namespace.every(id => syncNamespaces.includes(id));
    })[0]?.taskId;

    if (id) {
      logger.info(`${name} 已存在，使用当前任务`);
    }

    return id;
  } catch (e) {
    logger.error(`创建 ${name} 失败，${e.message}`);
  }
}

async function createTask(projectId, name, namespace) {
  const res = await i18nOpenAPI.post('ProjectTaskCreate', {
    projectId,
    name,
    syncNamespaces: namespace,
  });

  // if (res.code === 2006) {
  //   return await getTaskDetail(projectId, name, namespace);
  // }

  logger.info(`${name} 创建完成`);
  return res.data.taskId;
}

async function cancelWithCheckUpload(projectId) {
  return await request({
    url: '/openapi/project/import/text/withcheck/cancel',
    method: 'POST',
    Action: 'ProjectImportTextWithCheckCancel',
    data: {
      projectId
    }
  })
}

async function getUploadRecord(data) {
  return await request({
    url: '/openapi/project/import/record/query',
    method: 'GET',
    Action: 'ProjectImportRecordQuery',
    data
  })
}


async function cancelUpload({ projectId, recordId }) {
  return await request({
    url: '/openapi/project/import/text/cancel',
    method: 'POST',
    Action: 'ProjectImportTextCancel',
    data: {
      projectId,
      recordId
    }
  })
}

async function upload({ projectId, task, filePath }) {
  const form = new Form()
  form.append('projectId', projectId);
  form.append('taskId', task);

  const res = await request({
    url: '/openapi/project/text/import/check',
    method: 'POST',
    Action: 'ProjectTextImportCheck',
    data: form,
    stream: fs.createReadStream(filePath),
    headers: form.getHeaders()
  })

  return res
}

async function uploadConfirm({ recordId, projectId }) {
  const data = { recordId, projectId };

  const res = await request({
    url: '/openapi/project/text/import/confirm',
    method: 'POST',
    Action: 'ProjectTextImportConfirm',
    data
  })

  return res
}

async function getNamespaces(data) {
  return await request({
    url: '/openapi/project/namespaces',
    method: 'GET',
    Action: 'ProjectNamespaces',
    data,
  });
}

async function createNamespace(projectId, name) {
  return await request({
    url: '/openapi/project/namespace/create',
    method: 'POST',
    Action: 'ProjectNamespaceCreate',
    data: {
      projectId,
      name,
    }
  });
}

async function getSourceText(data) {
  return await request({
    url: '/openapi/project/namespace/sources',
    method: 'GET',
    Action: 'ProjectNamespaceSources',
    data,
    serviceName: 'i18n_openapi'
  });
}

async function getProjectDetail(projectId) {
  return await request({
    url: '/openapi/project/detail',
    method: 'GET',
    Action: 'ProjectDetail',
    data: {
      projectId,
    }
  });
}

module.exports = {
  getLocale,
  getLocaleByConfig,
  createTask,
  cancelWithCheckUpload,
  getUploadRecord,
  cancelUpload,
  upload,
  uploadConfirm,
  getNamespaces,
  getTaskDetail,
  createNamespace,
  getSourceText,
  getProjectDetail,
}