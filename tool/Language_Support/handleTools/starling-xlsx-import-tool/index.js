const XlsxReader = require('./xlsx');
const globby = require('globby');
const fs = require('fs');
const { createXlsx, asyncLoop, checkConfigExist, resolve } = require('./common/utils');
const logger = require('./common/logger');
const { getConfig } = require('./common/config');
const {
  createTask: createTaskApi,
  createNamespace: createNamespaceApi,
  getTaskDetail,
  getUploadRecord,
  cancelWithCheckUpload,
  cancelUpload,
  upload: uploadApi,
  uploadConfirm,
  getNamespaces,
  getProjectDetail,
} = require('./api');
const minimist = require('minimist');

const args = minimist(process.argv.slice(2));

const config = getConfig(args.config, args);

const xlsxPath = resolve(process.cwd(), config.file);

checkConfigExist(true, xlsxPath);

const xlsx = new XlsxReader(config.file);

/**
 * 获取项目id，不存在的任务、空间进行新建
 */
async function getTask(name, namespaceId, createNamespace, namespaceName) {
  try {
    const id = await getTaskDetail(config.projectId, name);

    const namespaceIds = namespaceId || [];

    if (!id && createNamespace) {
      const list = await getNamespaces({ projectId: config.projectId, name: namespaceName || name });

      const nid = list.data.find(({ namespace }) => namespace === (namespaceName || name))?.id;

      namespaceIds.length = 0;

      if (!nid) {
        const res = await createNamespaceApi(config.projectId, namespaceName || name);

        namespaceIds.push(res.data?.namespaceId);
      } else {
        namespaceIds.push(nid);
      }
    }

    if (id) {
      return id;
    }

    return await createTaskApi(config.projectId, name, namespaceIds);
  } catch (e) {
    logger.error(e);
  }
}

async function uploadHandle(task, path, _uploadRes) {
  let cancelRecordId
  try {
    const uploadList = await getUploadRecord({
      projectId: config.projectId,
      statusList: [0, 1, 2].join(','), // 查找正在导入的任务
      limit: 100,
      offset: 0
    })

    if (Array.isArray(uploadList.data) && uploadList.data.length) {
      if (uploadList.data.some(({ status }) => status === 0)) {
        await cancelWithCheckUpload(config.projectId)
      }

      return await new Promise(resolve => {
        if (uploadList.data.every(({ status }) => status === 0)) {
          resolve();
        } else {
          setTimeout(resolve, 5000)
        }
      })
        .then(() => uploadHandle(task, path, _uploadRes))
        .catch(e => {
          logger.error(e)
        })
    }

    if (_uploadRes) {
      return _uploadRes;
    }

    // 降低conflict概率
    await new Promise(resolve => setTimeout(resolve, 2000));

    const uploadRes = await uploadApi({
      projectId: config.projectId,
      filePath: path,
      task,
    })

    if (uploadRes?.code === 4) {
      logger.warn(`import conflict, import task count is ${uploadList.data?.length}`);
      return new Promise(resolve => {
        setTimeout(resolve, 5000)
      }).then(() => uploadHandle(task, path, _uploadRes));
    }

    // excel.push(uploadRes.data.tosUrl)

    const recordId = uploadRes.data.recordId

    cancelRecordId = recordId

    await uploadConfirm({ recordId, projectId: config.projectId })

    logger.info(`add: ${uploadRes.data.add}, update: ${uploadRes.data.update}, nodiff: ${uploadRes.data.noDiff}, fail: ${uploadRes.data.fail}`);

    return uploadHandle(task, path, {
      status: uploadRes.data.add || uploadRes.data.update || uploadRes.data.noDiff,
      url: uploadRes.data.tosUrl,
    });
  } catch (e) {
    cancelUpload({ projectId: config.projectId, recordId: cancelRecordId })

    logger.error(`upload failed: ${e.message}`)
  }
}

async function upload({ data, taskOption, list }) {
  const { task, namespace, sheet, createNoneexistendSpace, namespaceId } = taskOption;
  const taskName = task.replace('%sheetName%', sheet);
  const namespaceName = (namespace || '').replace('%sheetName%', sheet);
  const id = await getTask(taskName, namespaceId, !!createNoneexistendSpace, namespaceName);

  logger.info(`生成 ${taskName} 上传文件`);

  createXlsx(list, taskName);

  // await createZip(data, taskName, config.source);

  logger.info(`${taskName} 开始上传`);

  const { status, url } = await uploadHandle(id, resolve(__dirname, `./upload/${taskName || 'upload'}.xlsx`));

  if (status) {
    logger.success(`${taskName} 上传完成 \n ${url} \n`);
  } else {
    logger.fatal(`${taskName} 上传完成，没有新增/更新文案，请检查 \n ${url} \n`);
  }
}

async function run() {
  const sheets = config.keySheets;
  const languageMap = config.languages;
  const source = config.source;
  const ignore = Array.isArray(config.ignoreSheets) ? config.ignoreSheets : [];
  const include = Array.isArray(config.includeSheets) ? config.includeSheets : [];

  const project = await getProjectDetail(config.projectId);

  const sourceLang = project.data?.sourceLocale;

  const sheetNames = xlsx.getSheets().filter(name => !ignore.includes(name)).filter(name => !include.length || include.includes(name));
  const listMap = xlsx.getData();

  const list = Object.keys(sheets).flatMap(key => {
    const tmp = [];

    sheetNames.forEach(sheet => {
      let canCreateTask;

      const data = Object.fromEntries(Object.entries(languageMap).map(([_, v]) => ([v, {}])));

      const list = listMap[sheet];

      const res = [];

      // const data = [];

      list.forEach(item => {
        const languageCode = Object.keys(languageMap);

        Object.keys(item).forEach(k => {
          if (!item[k.trim()]) {
            item[k.trim()] = item[k];
          }
        });

        const obj = {
          keys: item[key],
          source: item[source],
          'length limit': item[config.lengthLimit],
          context: item[config.context],
        };

        languageCode.forEach(code => {
          if (item[code] && item[key]) {
            data[languageMap[code]][item[key].trim()] = item[code];
            canCreateTask = true;
          }

          if (item[code]) {
            obj[languageMap[code]] = item[code];
          }
        });

        if (!obj.source) {
          obj.source = obj[sourceLang] || '';
        }

        res.push(obj);
      });

      if (canCreateTask) {
        Object.keys(data).forEach(k => {
          if (!Object.keys(data[k]).length) {
            delete (data[k]);
          }
        });
        tmp.push({
          data,
          list: res,
          taskOption: {
            ...sheets[key],
            sheet,
          },
        })
      }
    });

    return tmp;
  });


  const paths = globby.sync(resolve(__dirname, `./upload/*`));

  paths.forEach(str => fs.unlinkSync(str));

  await asyncLoop(list.map((item, i) => () => {
    logger.info(`${i + 1}/${list.length}`);

    return upload(item);
  }));

  if (config.createLogFile) {
    logger.output();
  }

  logger.success('导入已完成');
}

try {
  run();
} catch (e) {
  console.error(e.message);
}

