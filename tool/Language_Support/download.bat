chcp 65001
@REM @echo off

SET DOWNLOADPATH=%~dp0download
SET DOWNLOADHANDLEPATH=%~dp0handleTools\downloadHandlePath

@REM start /b /wait cmd /k .\dlprocess.bat

@REM echo %~dp0handleTools\starling-xlsx-import-tool
cd %~dp0handleTools\starling-xlsx-import-tool

call starling-xlsx download --config=./starling.xlsx.yml --output=../downloadHandlePath/Common_text_download.xlsx

cd %~dp0
@REM echo %DOWNLOADPATH%
@REM echo %DOWNLOADHANDLEPATH%

python ./handleTools/convert_language_byte.py --type download --download_output %DOWNLOADPATH% --download_handle %DOWNLOADHANDLEPATH%

pause