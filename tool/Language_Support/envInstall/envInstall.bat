echo off

pip install xlwt -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install xlutils -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install pywin32 -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install argparse -i https://pypi.tuna.tsinghua.edu.cn/simple

cd %~dp0
cd %~dp0..\handleTools\starling-xlsx-import-tool
echo %~dp0..\handleTools\starling-xlsx-import-tool

@REM pause

start cmd /k "npm install"
start cmd /k "npm link"

@REM pause