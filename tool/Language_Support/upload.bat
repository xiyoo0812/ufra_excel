chcp 65001
@echo off
cd %~dp0
SET UPLOADPATH=%~dp0upload
SET UPLOADHANDLEPATH=%~dp0handleTools\uploadHandlePath

@REM echo %UPLOADPATH%
@REM echo %UPLOADHANDLEPATH%

python ./handleTools/convert_language_byte.py --type upload --upload_source %UPLOADPATH% --upload_handle %UPLOADHANDLEPATH%

if %ERRORLEVEL% NEQ 0 (
    echo 调用python出错，请检查系统环境变量是否已有python路径
    echo 如果没有安装Python请先安装
)

@REM echo %~dp0handleTools\starling-xlsx-import-tool
cd %~dp0handleTools\starling-xlsx-import-tool

starling-xlsx --config=./starling.xlsx.yml --file=../uploadHandlePath/Common_text.xls

if %ERRORLEVEL% NEQ 0 (
    echo 上传结果出错
)
if %ERRORLEVEL% NEQ 0 pause


