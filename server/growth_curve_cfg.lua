--growth_curve_cfg.lua
--source: 2_growth_curve_等级曲线表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local growth_curve = config_mgr:get_table("growth_curve")

--导出配置内容
growth_curve:upsert({
    id=101,
    lv=1,
    value=160
})

growth_curve:upsert({
    id=101,
    lv=2,
    value=320
})

growth_curve:upsert({
    id=101,
    lv=3,
    value=571
})

growth_curve:upsert({
    id=101,
    lv=4,
    value=873
})

growth_curve:upsert({
    id=101,
    lv=5,
    value=1000
})

growth_curve:upsert({
    id=101,
    lv=6,
    value=1100
})

growth_curve:upsert({
    id=101,
    lv=7,
    value=1214
})

growth_curve:upsert({
    id=101,
    lv=8,
    value=1335
})

growth_curve:upsert({
    id=101,
    lv=9,
    value=1459
})

growth_curve:upsert({
    id=101,
    lv=10,
    value=1587
})

growth_curve:upsert({
    id=101,
    lv=11,
    value=1718
})

growth_curve:upsert({
    id=101,
    lv=12,
    value=1850
})

growth_curve:upsert({
    id=101,
    lv=13,
    value=1985
})

growth_curve:upsert({
    id=101,
    lv=14,
    value=2121
})

growth_curve:upsert({
    id=101,
    lv=15,
    value=2400
})

growth_curve:upsert({
    id=101,
    lv=16,
    value=3000
})

growth_curve:upsert({
    id=101,
    lv=17,
    value=4097
})

growth_curve:upsert({
    id=101,
    lv=18,
    value=5518
})

growth_curve:upsert({
    id=101,
    lv=19,
    value=7200
})

growth_curve:upsert({
    id=101,
    lv=20,
    value=9108
})

growth_curve:upsert({
    id=101,
    lv=21,
    value=11218
})

growth_curve:upsert({
    id=101,
    lv=22,
    value=13512
})

growth_curve:upsert({
    id=101,
    lv=23,
    value=15976
})

growth_curve:upsert({
    id=101,
    lv=24,
    value=18600
})

growth_curve:upsert({
    id=101,
    lv=25,
    value=21374
})

growth_curve:upsert({
    id=101,
    lv=26,
    value=24290
})

growth_curve:upsert({
    id=101,
    lv=27,
    value=27342
})

growth_curve:upsert({
    id=101,
    lv=28,
    value=30523
})

growth_curve:upsert({
    id=101,
    lv=29,
    value=33830
})

growth_curve:upsert({
    id=101,
    lv=30,
    value=36000
})

growth_curve:upsert({
    id=101,
    lv=31,
    value=37800
})

growth_curve:upsert({
    id=101,
    lv=32,
    value=40918
})

growth_curve:upsert({
    id=101,
    lv=33,
    value=44853
})

growth_curve:upsert({
    id=101,
    lv=34,
    value=49436
})

growth_curve:upsert({
    id=101,
    lv=35,
    value=54569
})

growth_curve:upsert({
    id=101,
    lv=36,
    value=60188
})

growth_curve:upsert({
    id=101,
    lv=37,
    value=66246
})

growth_curve:upsert({
    id=101,
    lv=38,
    value=72707
})

growth_curve:upsert({
    id=101,
    lv=39,
    value=79544
})

growth_curve:upsert({
    id=101,
    lv=40,
    value=86731
})

growth_curve:upsert({
    id=101,
    lv=41,
    value=94249
})

growth_curve:upsert({
    id=101,
    lv=42,
    value=102082
})

growth_curve:upsert({
    id=101,
    lv=43,
    value=110215
})

growth_curve:upsert({
    id=101,
    lv=44,
    value=118634
})

growth_curve:upsert({
    id=101,
    lv=45,
    value=127328
})

growth_curve:upsert({
    id=101,
    lv=46,
    value=136287
})

growth_curve:upsert({
    id=101,
    lv=47,
    value=145502
})

growth_curve:upsert({
    id=101,
    lv=48,
    value=154965
})

growth_curve:upsert({
    id=101,
    lv=49,
    value=164666
})

growth_curve:upsert({
    id=101,
    lv=50,
    value=174601
})

growth_curve:upsert({
    id=101,
    lv=51,
    value=184761
})

growth_curve:upsert({
    id=101,
    lv=52,
    value=195142
})

growth_curve:upsert({
    id=101,
    lv=53,
    value=205738
})

growth_curve:upsert({
    id=101,
    lv=54,
    value=216542
})

growth_curve:upsert({
    id=101,
    lv=55,
    value=227551
})

growth_curve:upsert({
    id=101,
    lv=56,
    value=238761
})

growth_curve:upsert({
    id=101,
    lv=57,
    value=250166
})

growth_curve:upsert({
    id=101,
    lv=58,
    value=261762
})

growth_curve:upsert({
    id=101,
    lv=59,
    value=273547
})

growth_curve:upsert({
    id=101,
    lv=60,
    value=300000
})

growth_curve:upsert({
    id=101,
    lv=61,
    value=320000
})

growth_curve:upsert({
    id=101,
    lv=62,
    value=340000
})

growth_curve:upsert({
    id=101,
    lv=63,
    value=360000
})

growth_curve:upsert({
    id=101,
    lv=64,
    value=380000
})

growth_curve:upsert({
    id=101,
    lv=65,
    value=400000
})

growth_curve:upsert({
    id=101,
    lv=66,
    value=420000
})

growth_curve:upsert({
    id=101,
    lv=67,
    value=440000
})

growth_curve:upsert({
    id=101,
    lv=68,
    value=460000
})

growth_curve:upsert({
    id=101,
    lv=69,
    value=480000
})

growth_curve:upsert({
    id=101,
    lv=70,
    value=500000
})

growth_curve:upsert({
    id=101,
    lv=71,
    value=520000
})

growth_curve:upsert({
    id=101,
    lv=72,
    value=540000
})

growth_curve:upsert({
    id=101,
    lv=73,
    value=560000
})

growth_curve:upsert({
    id=101,
    lv=74,
    value=580000
})

growth_curve:upsert({
    id=101,
    lv=75,
    value=600000
})

growth_curve:upsert({
    id=101,
    lv=76,
    value=620000
})

growth_curve:upsert({
    id=101,
    lv=77,
    value=640000
})

growth_curve:upsert({
    id=101,
    lv=78,
    value=660000
})

growth_curve:upsert({
    id=101,
    lv=79,
    value=680000
})

growth_curve:upsert({
    id=101,
    lv=80,
    value=700000
})

growth_curve:upsert({
    id=101,
    lv=81,
    value=720000
})

growth_curve:upsert({
    id=101,
    lv=82,
    value=740000
})

growth_curve:upsert({
    id=101,
    lv=83,
    value=760000
})

growth_curve:upsert({
    id=101,
    lv=84,
    value=780000
})

growth_curve:upsert({
    id=101,
    lv=85,
    value=800000
})

growth_curve:upsert({
    id=101,
    lv=86,
    value=820000
})

growth_curve:upsert({
    id=101,
    lv=87,
    value=840000
})

growth_curve:upsert({
    id=101,
    lv=88,
    value=860000
})

growth_curve:upsert({
    id=101,
    lv=89,
    value=880000
})

growth_curve:upsert({
    id=101,
    lv=90,
    value=900000
})

growth_curve:upsert({
    id=101,
    lv=91,
    value=920000
})

growth_curve:upsert({
    id=101,
    lv=92,
    value=940000
})

growth_curve:upsert({
    id=101,
    lv=93,
    value=960000
})

growth_curve:upsert({
    id=101,
    lv=94,
    value=980000
})

growth_curve:upsert({
    id=101,
    lv=95,
    value=1000000
})

growth_curve:upsert({
    id=101,
    lv=96,
    value=1020000
})

growth_curve:upsert({
    id=101,
    lv=97,
    value=1040000
})

growth_curve:upsert({
    id=101,
    lv=98,
    value=1060000
})

growth_curve:upsert({
    id=101,
    lv=99,
    value=1080000
})

growth_curve:upsert({
    id=101,
    lv=100,
    value=1100000
})

growth_curve:upsert({
    id=102,
    lv=1,
    value=360
})

growth_curve:upsert({
    id=102,
    lv=2,
    value=480
})

growth_curve:upsert({
    id=102,
    lv=3,
    value=600
})

growth_curve:upsert({
    id=102,
    lv=4,
    value=720
})

growth_curve:upsert({
    id=102,
    lv=5,
    value=840
})

growth_curve:upsert({
    id=102,
    lv=6,
    value=960
})

growth_curve:upsert({
    id=102,
    lv=7,
    value=1080
})

growth_curve:upsert({
    id=102,
    lv=8,
    value=1200
})

growth_curve:upsert({
    id=102,
    lv=9,
    value=1320
})

growth_curve:upsert({
    id=102,
    lv=10,
    value=1440
})

growth_curve:upsert({
    id=102,
    lv=11,
    value=1560
})

growth_curve:upsert({
    id=102,
    lv=12,
    value=1680
})

growth_curve:upsert({
    id=102,
    lv=13,
    value=1800
})

growth_curve:upsert({
    id=102,
    lv=14,
    value=1920
})

growth_curve:upsert({
    id=102,
    lv=15,
    value=2040
})

growth_curve:upsert({
    id=102,
    lv=16,
    value=2160
})

growth_curve:upsert({
    id=102,
    lv=17,
    value=2280
})

growth_curve:upsert({
    id=102,
    lv=18,
    value=2400
})

growth_curve:upsert({
    id=102,
    lv=19,
    value=2520
})

growth_curve:upsert({
    id=102,
    lv=20,
    value=2640
})

growth_curve:upsert({
    id=102,
    lv=21,
    value=2760
})

growth_curve:upsert({
    id=102,
    lv=22,
    value=2880
})

growth_curve:upsert({
    id=102,
    lv=23,
    value=3000
})

growth_curve:upsert({
    id=102,
    lv=24,
    value=3120
})

growth_curve:upsert({
    id=102,
    lv=25,
    value=3240
})

growth_curve:upsert({
    id=102,
    lv=26,
    value=3360
})

growth_curve:upsert({
    id=102,
    lv=27,
    value=3480
})

growth_curve:upsert({
    id=102,
    lv=28,
    value=3600
})

growth_curve:upsert({
    id=102,
    lv=29,
    value=3720
})

growth_curve:upsert({
    id=102,
    lv=30,
    value=3840
})

growth_curve:upsert({
    id=102,
    lv=31,
    value=3960
})

growth_curve:upsert({
    id=102,
    lv=32,
    value=4080
})

growth_curve:upsert({
    id=102,
    lv=33,
    value=4200
})

growth_curve:upsert({
    id=102,
    lv=34,
    value=4320
})

growth_curve:upsert({
    id=102,
    lv=35,
    value=4440
})

growth_curve:upsert({
    id=102,
    lv=36,
    value=4560
})

growth_curve:upsert({
    id=102,
    lv=37,
    value=4680
})

growth_curve:upsert({
    id=102,
    lv=38,
    value=4800
})

growth_curve:upsert({
    id=102,
    lv=39,
    value=4920
})

growth_curve:upsert({
    id=102,
    lv=40,
    value=5040
})

growth_curve:upsert({
    id=102,
    lv=41,
    value=5160
})

growth_curve:upsert({
    id=102,
    lv=42,
    value=5280
})

growth_curve:upsert({
    id=102,
    lv=43,
    value=5400
})

growth_curve:upsert({
    id=102,
    lv=44,
    value=5520
})

growth_curve:upsert({
    id=102,
    lv=45,
    value=5640
})

growth_curve:upsert({
    id=102,
    lv=46,
    value=5760
})

growth_curve:upsert({
    id=102,
    lv=47,
    value=5880
})

growth_curve:upsert({
    id=102,
    lv=48,
    value=6000
})

growth_curve:upsert({
    id=102,
    lv=49,
    value=6120
})

growth_curve:upsert({
    id=102,
    lv=50,
    value=6240
})

growth_curve:upsert({
    id=102,
    lv=51,
    value=6360
})

growth_curve:upsert({
    id=102,
    lv=52,
    value=6480
})

growth_curve:upsert({
    id=102,
    lv=53,
    value=6600
})

growth_curve:upsert({
    id=102,
    lv=54,
    value=6720
})

growth_curve:upsert({
    id=102,
    lv=55,
    value=6840
})

growth_curve:upsert({
    id=102,
    lv=56,
    value=6960
})

growth_curve:upsert({
    id=102,
    lv=57,
    value=7080
})

growth_curve:upsert({
    id=102,
    lv=58,
    value=7200
})

growth_curve:upsert({
    id=102,
    lv=59,
    value=7320
})

growth_curve:upsert({
    id=102,
    lv=60,
    value=7440
})

growth_curve:upsert({
    id=102,
    lv=61,
    value=7560
})

growth_curve:upsert({
    id=102,
    lv=62,
    value=7680
})

growth_curve:upsert({
    id=102,
    lv=63,
    value=7800
})

growth_curve:upsert({
    id=102,
    lv=64,
    value=7920
})

growth_curve:upsert({
    id=102,
    lv=65,
    value=8040
})

growth_curve:upsert({
    id=102,
    lv=66,
    value=8160
})

growth_curve:upsert({
    id=102,
    lv=67,
    value=8280
})

growth_curve:upsert({
    id=102,
    lv=68,
    value=8400
})

growth_curve:upsert({
    id=102,
    lv=69,
    value=8520
})

growth_curve:upsert({
    id=102,
    lv=70,
    value=8640
})

growth_curve:upsert({
    id=102,
    lv=71,
    value=8760
})

growth_curve:upsert({
    id=102,
    lv=72,
    value=8880
})

growth_curve:upsert({
    id=102,
    lv=73,
    value=9000
})

growth_curve:upsert({
    id=102,
    lv=74,
    value=9120
})

growth_curve:upsert({
    id=102,
    lv=75,
    value=9240
})

growth_curve:upsert({
    id=102,
    lv=76,
    value=9360
})

growth_curve:upsert({
    id=102,
    lv=77,
    value=9480
})

growth_curve:upsert({
    id=102,
    lv=78,
    value=9600
})

growth_curve:upsert({
    id=102,
    lv=79,
    value=9720
})

growth_curve:upsert({
    id=102,
    lv=80,
    value=9840
})

growth_curve:upsert({
    id=102,
    lv=81,
    value=9960
})

growth_curve:upsert({
    id=102,
    lv=82,
    value=10080
})

growth_curve:upsert({
    id=102,
    lv=83,
    value=10200
})

growth_curve:upsert({
    id=102,
    lv=84,
    value=10320
})

growth_curve:upsert({
    id=102,
    lv=85,
    value=10440
})

growth_curve:upsert({
    id=102,
    lv=86,
    value=10560
})

growth_curve:upsert({
    id=102,
    lv=87,
    value=10680
})

growth_curve:upsert({
    id=102,
    lv=88,
    value=10800
})

growth_curve:upsert({
    id=102,
    lv=89,
    value=10920
})

growth_curve:upsert({
    id=102,
    lv=90,
    value=11040
})

growth_curve:upsert({
    id=102,
    lv=91,
    value=11160
})

growth_curve:upsert({
    id=102,
    lv=92,
    value=11280
})

growth_curve:upsert({
    id=102,
    lv=93,
    value=11400
})

growth_curve:upsert({
    id=102,
    lv=94,
    value=11520
})

growth_curve:upsert({
    id=102,
    lv=95,
    value=11640
})

growth_curve:upsert({
    id=102,
    lv=96,
    value=11760
})

growth_curve:upsert({
    id=102,
    lv=97,
    value=11880
})

growth_curve:upsert({
    id=102,
    lv=98,
    value=12000
})

growth_curve:upsert({
    id=102,
    lv=99,
    value=12120
})

growth_curve:upsert({
    id=102,
    lv=100,
    value=12240
})

growth_curve:upsert({
    id=103,
    lv=1,
    value=90
})

growth_curve:upsert({
    id=103,
    lv=2,
    value=120
})

growth_curve:upsert({
    id=103,
    lv=3,
    value=150
})

growth_curve:upsert({
    id=103,
    lv=4,
    value=180
})

growth_curve:upsert({
    id=103,
    lv=5,
    value=210
})

growth_curve:upsert({
    id=103,
    lv=6,
    value=240
})

growth_curve:upsert({
    id=103,
    lv=7,
    value=270
})

growth_curve:upsert({
    id=103,
    lv=8,
    value=300
})

growth_curve:upsert({
    id=103,
    lv=9,
    value=330
})

growth_curve:upsert({
    id=103,
    lv=10,
    value=360
})

growth_curve:upsert({
    id=103,
    lv=11,
    value=390
})

growth_curve:upsert({
    id=103,
    lv=12,
    value=420
})

growth_curve:upsert({
    id=103,
    lv=13,
    value=450
})

growth_curve:upsert({
    id=103,
    lv=14,
    value=480
})

growth_curve:upsert({
    id=103,
    lv=15,
    value=510
})

growth_curve:upsert({
    id=103,
    lv=16,
    value=540
})

growth_curve:upsert({
    id=103,
    lv=17,
    value=570
})

growth_curve:upsert({
    id=103,
    lv=18,
    value=600
})

growth_curve:upsert({
    id=103,
    lv=19,
    value=630
})

growth_curve:upsert({
    id=103,
    lv=20,
    value=660
})

growth_curve:upsert({
    id=103,
    lv=21,
    value=690
})

growth_curve:upsert({
    id=103,
    lv=22,
    value=720
})

growth_curve:upsert({
    id=103,
    lv=23,
    value=750
})

growth_curve:upsert({
    id=103,
    lv=24,
    value=780
})

growth_curve:upsert({
    id=103,
    lv=25,
    value=810
})

growth_curve:upsert({
    id=103,
    lv=26,
    value=840
})

growth_curve:upsert({
    id=103,
    lv=27,
    value=870
})

growth_curve:upsert({
    id=103,
    lv=28,
    value=900
})

growth_curve:upsert({
    id=103,
    lv=29,
    value=930
})

growth_curve:upsert({
    id=103,
    lv=30,
    value=960
})

growth_curve:upsert({
    id=103,
    lv=31,
    value=990
})

growth_curve:upsert({
    id=103,
    lv=32,
    value=1020
})

growth_curve:upsert({
    id=103,
    lv=33,
    value=1050
})

growth_curve:upsert({
    id=103,
    lv=34,
    value=1080
})

growth_curve:upsert({
    id=103,
    lv=35,
    value=1110
})

growth_curve:upsert({
    id=103,
    lv=36,
    value=1140
})

growth_curve:upsert({
    id=103,
    lv=37,
    value=1170
})

growth_curve:upsert({
    id=103,
    lv=38,
    value=1200
})

growth_curve:upsert({
    id=103,
    lv=39,
    value=1230
})

growth_curve:upsert({
    id=103,
    lv=40,
    value=1260
})

growth_curve:upsert({
    id=103,
    lv=41,
    value=1290
})

growth_curve:upsert({
    id=103,
    lv=42,
    value=1320
})

growth_curve:upsert({
    id=103,
    lv=43,
    value=1350
})

growth_curve:upsert({
    id=103,
    lv=44,
    value=1380
})

growth_curve:upsert({
    id=103,
    lv=45,
    value=1410
})

growth_curve:upsert({
    id=103,
    lv=46,
    value=1440
})

growth_curve:upsert({
    id=103,
    lv=47,
    value=1470
})

growth_curve:upsert({
    id=103,
    lv=48,
    value=1500
})

growth_curve:upsert({
    id=103,
    lv=49,
    value=1530
})

growth_curve:upsert({
    id=103,
    lv=50,
    value=1560
})

growth_curve:upsert({
    id=103,
    lv=51,
    value=1590
})

growth_curve:upsert({
    id=103,
    lv=52,
    value=1620
})

growth_curve:upsert({
    id=103,
    lv=53,
    value=1650
})

growth_curve:upsert({
    id=103,
    lv=54,
    value=1680
})

growth_curve:upsert({
    id=103,
    lv=55,
    value=1710
})

growth_curve:upsert({
    id=103,
    lv=56,
    value=1740
})

growth_curve:upsert({
    id=103,
    lv=57,
    value=1770
})

growth_curve:upsert({
    id=103,
    lv=58,
    value=1800
})

growth_curve:upsert({
    id=103,
    lv=59,
    value=1830
})

growth_curve:upsert({
    id=103,
    lv=60,
    value=1860
})

growth_curve:upsert({
    id=103,
    lv=61,
    value=1890
})

growth_curve:upsert({
    id=103,
    lv=62,
    value=1920
})

growth_curve:upsert({
    id=103,
    lv=63,
    value=1950
})

growth_curve:upsert({
    id=103,
    lv=64,
    value=1980
})

growth_curve:upsert({
    id=103,
    lv=65,
    value=2010
})

growth_curve:upsert({
    id=103,
    lv=66,
    value=2040
})

growth_curve:upsert({
    id=103,
    lv=67,
    value=2070
})

growth_curve:upsert({
    id=103,
    lv=68,
    value=2100
})

growth_curve:upsert({
    id=103,
    lv=69,
    value=2130
})

growth_curve:upsert({
    id=103,
    lv=70,
    value=2160
})

growth_curve:upsert({
    id=103,
    lv=71,
    value=2190
})

growth_curve:upsert({
    id=103,
    lv=72,
    value=2220
})

growth_curve:upsert({
    id=103,
    lv=73,
    value=2250
})

growth_curve:upsert({
    id=103,
    lv=74,
    value=2280
})

growth_curve:upsert({
    id=103,
    lv=75,
    value=2310
})

growth_curve:upsert({
    id=103,
    lv=76,
    value=2340
})

growth_curve:upsert({
    id=103,
    lv=77,
    value=2370
})

growth_curve:upsert({
    id=103,
    lv=78,
    value=2400
})

growth_curve:upsert({
    id=103,
    lv=79,
    value=2430
})

growth_curve:upsert({
    id=103,
    lv=80,
    value=2460
})

growth_curve:upsert({
    id=103,
    lv=81,
    value=2490
})

growth_curve:upsert({
    id=103,
    lv=82,
    value=2520
})

growth_curve:upsert({
    id=103,
    lv=83,
    value=2550
})

growth_curve:upsert({
    id=103,
    lv=84,
    value=2580
})

growth_curve:upsert({
    id=103,
    lv=85,
    value=2610
})

growth_curve:upsert({
    id=103,
    lv=86,
    value=2640
})

growth_curve:upsert({
    id=103,
    lv=87,
    value=2670
})

growth_curve:upsert({
    id=103,
    lv=88,
    value=2700
})

growth_curve:upsert({
    id=103,
    lv=89,
    value=2730
})

growth_curve:upsert({
    id=103,
    lv=90,
    value=2760
})

growth_curve:upsert({
    id=103,
    lv=91,
    value=2790
})

growth_curve:upsert({
    id=103,
    lv=92,
    value=2820
})

growth_curve:upsert({
    id=103,
    lv=93,
    value=2850
})

growth_curve:upsert({
    id=103,
    lv=94,
    value=2880
})

growth_curve:upsert({
    id=103,
    lv=95,
    value=2910
})

growth_curve:upsert({
    id=103,
    lv=96,
    value=2940
})

growth_curve:upsert({
    id=103,
    lv=97,
    value=2970
})

growth_curve:upsert({
    id=103,
    lv=98,
    value=3000
})

growth_curve:upsert({
    id=103,
    lv=99,
    value=3030
})

growth_curve:upsert({
    id=103,
    lv=100,
    value=3060
})

growth_curve:upsert({
    id=104,
    lv=1,
    value=55
})

growth_curve:upsert({
    id=104,
    lv=2,
    value=70
})

growth_curve:upsert({
    id=104,
    lv=3,
    value=85
})

growth_curve:upsert({
    id=104,
    lv=4,
    value=100
})

growth_curve:upsert({
    id=104,
    lv=5,
    value=115
})

growth_curve:upsert({
    id=104,
    lv=6,
    value=130
})

growth_curve:upsert({
    id=104,
    lv=7,
    value=145
})

growth_curve:upsert({
    id=104,
    lv=8,
    value=160
})

growth_curve:upsert({
    id=104,
    lv=9,
    value=175
})

growth_curve:upsert({
    id=104,
    lv=10,
    value=190
})

growth_curve:upsert({
    id=104,
    lv=11,
    value=205
})

growth_curve:upsert({
    id=104,
    lv=12,
    value=220
})

growth_curve:upsert({
    id=104,
    lv=13,
    value=235
})

growth_curve:upsert({
    id=104,
    lv=14,
    value=250
})

growth_curve:upsert({
    id=104,
    lv=15,
    value=265
})

growth_curve:upsert({
    id=104,
    lv=16,
    value=280
})

growth_curve:upsert({
    id=104,
    lv=17,
    value=295
})

growth_curve:upsert({
    id=104,
    lv=18,
    value=310
})

growth_curve:upsert({
    id=104,
    lv=19,
    value=325
})

growth_curve:upsert({
    id=104,
    lv=20,
    value=340
})

growth_curve:upsert({
    id=104,
    lv=21,
    value=355
})

growth_curve:upsert({
    id=104,
    lv=22,
    value=370
})

growth_curve:upsert({
    id=104,
    lv=23,
    value=385
})

growth_curve:upsert({
    id=104,
    lv=24,
    value=400
})

growth_curve:upsert({
    id=104,
    lv=25,
    value=415
})

growth_curve:upsert({
    id=104,
    lv=26,
    value=430
})

growth_curve:upsert({
    id=104,
    lv=27,
    value=445
})

growth_curve:upsert({
    id=104,
    lv=28,
    value=460
})

growth_curve:upsert({
    id=104,
    lv=29,
    value=475
})

growth_curve:upsert({
    id=104,
    lv=30,
    value=490
})

growth_curve:upsert({
    id=104,
    lv=31,
    value=505
})

growth_curve:upsert({
    id=104,
    lv=32,
    value=520
})

growth_curve:upsert({
    id=104,
    lv=33,
    value=535
})

growth_curve:upsert({
    id=104,
    lv=34,
    value=550
})

growth_curve:upsert({
    id=104,
    lv=35,
    value=565
})

growth_curve:upsert({
    id=104,
    lv=36,
    value=580
})

growth_curve:upsert({
    id=104,
    lv=37,
    value=595
})

growth_curve:upsert({
    id=104,
    lv=38,
    value=610
})

growth_curve:upsert({
    id=104,
    lv=39,
    value=625
})

growth_curve:upsert({
    id=104,
    lv=40,
    value=640
})

growth_curve:upsert({
    id=104,
    lv=41,
    value=655
})

growth_curve:upsert({
    id=104,
    lv=42,
    value=670
})

growth_curve:upsert({
    id=104,
    lv=43,
    value=685
})

growth_curve:upsert({
    id=104,
    lv=44,
    value=700
})

growth_curve:upsert({
    id=104,
    lv=45,
    value=715
})

growth_curve:upsert({
    id=104,
    lv=46,
    value=730
})

growth_curve:upsert({
    id=104,
    lv=47,
    value=745
})

growth_curve:upsert({
    id=104,
    lv=48,
    value=760
})

growth_curve:upsert({
    id=104,
    lv=49,
    value=775
})

growth_curve:upsert({
    id=104,
    lv=50,
    value=790
})

growth_curve:upsert({
    id=104,
    lv=51,
    value=805
})

growth_curve:upsert({
    id=104,
    lv=52,
    value=820
})

growth_curve:upsert({
    id=104,
    lv=53,
    value=835
})

growth_curve:upsert({
    id=104,
    lv=54,
    value=850
})

growth_curve:upsert({
    id=104,
    lv=55,
    value=865
})

growth_curve:upsert({
    id=104,
    lv=56,
    value=880
})

growth_curve:upsert({
    id=104,
    lv=57,
    value=895
})

growth_curve:upsert({
    id=104,
    lv=58,
    value=910
})

growth_curve:upsert({
    id=104,
    lv=59,
    value=925
})

growth_curve:upsert({
    id=104,
    lv=60,
    value=940
})

growth_curve:upsert({
    id=104,
    lv=61,
    value=955
})

growth_curve:upsert({
    id=104,
    lv=62,
    value=970
})

growth_curve:upsert({
    id=104,
    lv=63,
    value=985
})

growth_curve:upsert({
    id=104,
    lv=64,
    value=1000
})

growth_curve:upsert({
    id=104,
    lv=65,
    value=1015
})

growth_curve:upsert({
    id=104,
    lv=66,
    value=1030
})

growth_curve:upsert({
    id=104,
    lv=67,
    value=1045
})

growth_curve:upsert({
    id=104,
    lv=68,
    value=1060
})

growth_curve:upsert({
    id=104,
    lv=69,
    value=1075
})

growth_curve:upsert({
    id=104,
    lv=70,
    value=1090
})

growth_curve:upsert({
    id=104,
    lv=71,
    value=1105
})

growth_curve:upsert({
    id=104,
    lv=72,
    value=1120
})

growth_curve:upsert({
    id=104,
    lv=73,
    value=1135
})

growth_curve:upsert({
    id=104,
    lv=74,
    value=1150
})

growth_curve:upsert({
    id=104,
    lv=75,
    value=1165
})

growth_curve:upsert({
    id=104,
    lv=76,
    value=1180
})

growth_curve:upsert({
    id=104,
    lv=77,
    value=1195
})

growth_curve:upsert({
    id=104,
    lv=78,
    value=1210
})

growth_curve:upsert({
    id=104,
    lv=79,
    value=1225
})

growth_curve:upsert({
    id=104,
    lv=80,
    value=1240
})

growth_curve:upsert({
    id=104,
    lv=81,
    value=1255
})

growth_curve:upsert({
    id=104,
    lv=82,
    value=1270
})

growth_curve:upsert({
    id=104,
    lv=83,
    value=1285
})

growth_curve:upsert({
    id=104,
    lv=84,
    value=1300
})

growth_curve:upsert({
    id=104,
    lv=85,
    value=1315
})

growth_curve:upsert({
    id=104,
    lv=86,
    value=1330
})

growth_curve:upsert({
    id=104,
    lv=87,
    value=1345
})

growth_curve:upsert({
    id=104,
    lv=88,
    value=1360
})

growth_curve:upsert({
    id=104,
    lv=89,
    value=1375
})

growth_curve:upsert({
    id=104,
    lv=90,
    value=1390
})

growth_curve:upsert({
    id=104,
    lv=91,
    value=1405
})

growth_curve:upsert({
    id=104,
    lv=92,
    value=1420
})

growth_curve:upsert({
    id=104,
    lv=93,
    value=1435
})

growth_curve:upsert({
    id=104,
    lv=94,
    value=1450
})

growth_curve:upsert({
    id=104,
    lv=95,
    value=1465
})

growth_curve:upsert({
    id=104,
    lv=96,
    value=1480
})

growth_curve:upsert({
    id=104,
    lv=97,
    value=1495
})

growth_curve:upsert({
    id=104,
    lv=98,
    value=1510
})

growth_curve:upsert({
    id=104,
    lv=99,
    value=1525
})

growth_curve:upsert({
    id=104,
    lv=100,
    value=1540
})

growth_curve:upsert({
    id=105,
    lv=1,
    value=1000
})

growth_curve:upsert({
    id=105,
    lv=2,
    value=1005
})

growth_curve:upsert({
    id=105,
    lv=3,
    value=1010
})

growth_curve:upsert({
    id=105,
    lv=4,
    value=1015
})

growth_curve:upsert({
    id=105,
    lv=5,
    value=1020
})

growth_curve:upsert({
    id=105,
    lv=6,
    value=1025
})

growth_curve:upsert({
    id=105,
    lv=7,
    value=1030
})

growth_curve:upsert({
    id=105,
    lv=8,
    value=1035
})

growth_curve:upsert({
    id=105,
    lv=9,
    value=1040
})

growth_curve:upsert({
    id=105,
    lv=10,
    value=1045
})

growth_curve:upsert({
    id=105,
    lv=11,
    value=1050
})

growth_curve:upsert({
    id=105,
    lv=12,
    value=1055
})

growth_curve:upsert({
    id=105,
    lv=13,
    value=1060
})

growth_curve:upsert({
    id=105,
    lv=14,
    value=1065
})

growth_curve:upsert({
    id=105,
    lv=15,
    value=1070
})

growth_curve:upsert({
    id=105,
    lv=16,
    value=1075
})

growth_curve:upsert({
    id=105,
    lv=17,
    value=1080
})

growth_curve:upsert({
    id=105,
    lv=18,
    value=1085
})

growth_curve:upsert({
    id=105,
    lv=19,
    value=1090
})

growth_curve:upsert({
    id=105,
    lv=20,
    value=1095
})

growth_curve:upsert({
    id=105,
    lv=21,
    value=1100
})

growth_curve:upsert({
    id=105,
    lv=22,
    value=1105
})

growth_curve:upsert({
    id=105,
    lv=23,
    value=1110
})

growth_curve:upsert({
    id=105,
    lv=24,
    value=1115
})

growth_curve:upsert({
    id=105,
    lv=25,
    value=1120
})

growth_curve:upsert({
    id=105,
    lv=26,
    value=1125
})

growth_curve:upsert({
    id=105,
    lv=27,
    value=1130
})

growth_curve:upsert({
    id=105,
    lv=28,
    value=1135
})

growth_curve:upsert({
    id=105,
    lv=29,
    value=1140
})

growth_curve:upsert({
    id=105,
    lv=30,
    value=1145
})

growth_curve:upsert({
    id=105,
    lv=31,
    value=1150
})

growth_curve:upsert({
    id=105,
    lv=32,
    value=1155
})

growth_curve:upsert({
    id=105,
    lv=33,
    value=1160
})

growth_curve:upsert({
    id=105,
    lv=34,
    value=1165
})

growth_curve:upsert({
    id=105,
    lv=35,
    value=1170
})

growth_curve:upsert({
    id=105,
    lv=36,
    value=1175
})

growth_curve:upsert({
    id=105,
    lv=37,
    value=1180
})

growth_curve:upsert({
    id=105,
    lv=38,
    value=1185
})

growth_curve:upsert({
    id=105,
    lv=39,
    value=1190
})

growth_curve:upsert({
    id=105,
    lv=40,
    value=1195
})

growth_curve:upsert({
    id=105,
    lv=41,
    value=1200
})

growth_curve:upsert({
    id=105,
    lv=42,
    value=1205
})

growth_curve:upsert({
    id=105,
    lv=43,
    value=1210
})

growth_curve:upsert({
    id=105,
    lv=44,
    value=1215
})

growth_curve:upsert({
    id=105,
    lv=45,
    value=1220
})

growth_curve:upsert({
    id=105,
    lv=46,
    value=1225
})

growth_curve:upsert({
    id=105,
    lv=47,
    value=1230
})

growth_curve:upsert({
    id=105,
    lv=48,
    value=1235
})

growth_curve:upsert({
    id=105,
    lv=49,
    value=1240
})

growth_curve:upsert({
    id=105,
    lv=50,
    value=1245
})

growth_curve:upsert({
    id=105,
    lv=51,
    value=1250
})

growth_curve:upsert({
    id=105,
    lv=52,
    value=1255
})

growth_curve:upsert({
    id=105,
    lv=53,
    value=1260
})

growth_curve:upsert({
    id=105,
    lv=54,
    value=1265
})

growth_curve:upsert({
    id=105,
    lv=55,
    value=1270
})

growth_curve:upsert({
    id=105,
    lv=56,
    value=1275
})

growth_curve:upsert({
    id=105,
    lv=57,
    value=1280
})

growth_curve:upsert({
    id=105,
    lv=58,
    value=1285
})

growth_curve:upsert({
    id=105,
    lv=59,
    value=1290
})

growth_curve:upsert({
    id=105,
    lv=60,
    value=1295
})

growth_curve:upsert({
    id=105,
    lv=61,
    value=1300
})

growth_curve:upsert({
    id=105,
    lv=62,
    value=1305
})

growth_curve:upsert({
    id=105,
    lv=63,
    value=1310
})

growth_curve:upsert({
    id=105,
    lv=64,
    value=1315
})

growth_curve:upsert({
    id=105,
    lv=65,
    value=1320
})

growth_curve:upsert({
    id=105,
    lv=66,
    value=1325
})

growth_curve:upsert({
    id=105,
    lv=67,
    value=1330
})

growth_curve:upsert({
    id=105,
    lv=68,
    value=1335
})

growth_curve:upsert({
    id=105,
    lv=69,
    value=1340
})

growth_curve:upsert({
    id=105,
    lv=70,
    value=1345
})

growth_curve:upsert({
    id=105,
    lv=71,
    value=1350
})

growth_curve:upsert({
    id=105,
    lv=72,
    value=1355
})

growth_curve:upsert({
    id=105,
    lv=73,
    value=1360
})

growth_curve:upsert({
    id=105,
    lv=74,
    value=1365
})

growth_curve:upsert({
    id=105,
    lv=75,
    value=1370
})

growth_curve:upsert({
    id=105,
    lv=76,
    value=1375
})

growth_curve:upsert({
    id=105,
    lv=77,
    value=1380
})

growth_curve:upsert({
    id=105,
    lv=78,
    value=1385
})

growth_curve:upsert({
    id=105,
    lv=79,
    value=1390
})

growth_curve:upsert({
    id=105,
    lv=80,
    value=1395
})

growth_curve:upsert({
    id=105,
    lv=81,
    value=1400
})

growth_curve:upsert({
    id=105,
    lv=82,
    value=1405
})

growth_curve:upsert({
    id=105,
    lv=83,
    value=1410
})

growth_curve:upsert({
    id=105,
    lv=84,
    value=1415
})

growth_curve:upsert({
    id=105,
    lv=85,
    value=1420
})

growth_curve:upsert({
    id=105,
    lv=86,
    value=1425
})

growth_curve:upsert({
    id=105,
    lv=87,
    value=1430
})

growth_curve:upsert({
    id=105,
    lv=88,
    value=1435
})

growth_curve:upsert({
    id=105,
    lv=89,
    value=1440
})

growth_curve:upsert({
    id=105,
    lv=90,
    value=1445
})

growth_curve:upsert({
    id=105,
    lv=91,
    value=1450
})

growth_curve:upsert({
    id=105,
    lv=92,
    value=1455
})

growth_curve:upsert({
    id=105,
    lv=93,
    value=1460
})

growth_curve:upsert({
    id=105,
    lv=94,
    value=1465
})

growth_curve:upsert({
    id=105,
    lv=95,
    value=1470
})

growth_curve:upsert({
    id=105,
    lv=96,
    value=1475
})

growth_curve:upsert({
    id=105,
    lv=97,
    value=1480
})

growth_curve:upsert({
    id=105,
    lv=98,
    value=1485
})

growth_curve:upsert({
    id=105,
    lv=99,
    value=1490
})

growth_curve:upsert({
    id=105,
    lv=100,
    value=1495
})

growth_curve:upsert({
    id=106,
    lv=1,
    value=15000
})

growth_curve:upsert({
    id=106,
    lv=2,
    value=15100
})

growth_curve:upsert({
    id=106,
    lv=3,
    value=15200
})

growth_curve:upsert({
    id=106,
    lv=4,
    value=15300
})

growth_curve:upsert({
    id=106,
    lv=5,
    value=15400
})

growth_curve:upsert({
    id=106,
    lv=6,
    value=15500
})

growth_curve:upsert({
    id=106,
    lv=7,
    value=15600
})

growth_curve:upsert({
    id=106,
    lv=8,
    value=15700
})

growth_curve:upsert({
    id=106,
    lv=9,
    value=15800
})

growth_curve:upsert({
    id=106,
    lv=10,
    value=15900
})

growth_curve:upsert({
    id=106,
    lv=11,
    value=16000
})

growth_curve:upsert({
    id=106,
    lv=12,
    value=16100
})

growth_curve:upsert({
    id=106,
    lv=13,
    value=16200
})

growth_curve:upsert({
    id=106,
    lv=14,
    value=16300
})

growth_curve:upsert({
    id=106,
    lv=15,
    value=16400
})

growth_curve:upsert({
    id=106,
    lv=16,
    value=16500
})

growth_curve:upsert({
    id=106,
    lv=17,
    value=16600
})

growth_curve:upsert({
    id=106,
    lv=18,
    value=16700
})

growth_curve:upsert({
    id=106,
    lv=19,
    value=16800
})

growth_curve:upsert({
    id=106,
    lv=20,
    value=16900
})

growth_curve:upsert({
    id=106,
    lv=21,
    value=17000
})

growth_curve:upsert({
    id=106,
    lv=22,
    value=17100
})

growth_curve:upsert({
    id=106,
    lv=23,
    value=17200
})

growth_curve:upsert({
    id=106,
    lv=24,
    value=17300
})

growth_curve:upsert({
    id=106,
    lv=25,
    value=17400
})

growth_curve:upsert({
    id=106,
    lv=26,
    value=17500
})

growth_curve:upsert({
    id=106,
    lv=27,
    value=17600
})

growth_curve:upsert({
    id=106,
    lv=28,
    value=17700
})

growth_curve:upsert({
    id=106,
    lv=29,
    value=17800
})

growth_curve:upsert({
    id=106,
    lv=30,
    value=17900
})

growth_curve:upsert({
    id=106,
    lv=31,
    value=18000
})

growth_curve:upsert({
    id=106,
    lv=32,
    value=18100
})

growth_curve:upsert({
    id=106,
    lv=33,
    value=18200
})

growth_curve:upsert({
    id=106,
    lv=34,
    value=18300
})

growth_curve:upsert({
    id=106,
    lv=35,
    value=18400
})

growth_curve:upsert({
    id=106,
    lv=36,
    value=18500
})

growth_curve:upsert({
    id=106,
    lv=37,
    value=18600
})

growth_curve:upsert({
    id=106,
    lv=38,
    value=18700
})

growth_curve:upsert({
    id=106,
    lv=39,
    value=18800
})

growth_curve:upsert({
    id=106,
    lv=40,
    value=18900
})

growth_curve:upsert({
    id=106,
    lv=41,
    value=19000
})

growth_curve:upsert({
    id=106,
    lv=42,
    value=19100
})

growth_curve:upsert({
    id=106,
    lv=43,
    value=19200
})

growth_curve:upsert({
    id=106,
    lv=44,
    value=19300
})

growth_curve:upsert({
    id=106,
    lv=45,
    value=19400
})

growth_curve:upsert({
    id=106,
    lv=46,
    value=19500
})

growth_curve:upsert({
    id=106,
    lv=47,
    value=19600
})

growth_curve:upsert({
    id=106,
    lv=48,
    value=19700
})

growth_curve:upsert({
    id=106,
    lv=49,
    value=19800
})

growth_curve:upsert({
    id=106,
    lv=50,
    value=19900
})

growth_curve:upsert({
    id=106,
    lv=51,
    value=20000
})

growth_curve:upsert({
    id=106,
    lv=52,
    value=20100
})

growth_curve:upsert({
    id=106,
    lv=53,
    value=20200
})

growth_curve:upsert({
    id=106,
    lv=54,
    value=20300
})

growth_curve:upsert({
    id=106,
    lv=55,
    value=20400
})

growth_curve:upsert({
    id=106,
    lv=56,
    value=20500
})

growth_curve:upsert({
    id=106,
    lv=57,
    value=20600
})

growth_curve:upsert({
    id=106,
    lv=58,
    value=20700
})

growth_curve:upsert({
    id=106,
    lv=59,
    value=20800
})

growth_curve:upsert({
    id=106,
    lv=60,
    value=20900
})

growth_curve:upsert({
    id=106,
    lv=61,
    value=21000
})

growth_curve:upsert({
    id=106,
    lv=62,
    value=21100
})

growth_curve:upsert({
    id=106,
    lv=63,
    value=21200
})

growth_curve:upsert({
    id=106,
    lv=64,
    value=21300
})

growth_curve:upsert({
    id=106,
    lv=65,
    value=21400
})

growth_curve:upsert({
    id=106,
    lv=66,
    value=21500
})

growth_curve:upsert({
    id=106,
    lv=67,
    value=21600
})

growth_curve:upsert({
    id=106,
    lv=68,
    value=21700
})

growth_curve:upsert({
    id=106,
    lv=69,
    value=21800
})

growth_curve:upsert({
    id=106,
    lv=70,
    value=21900
})

growth_curve:upsert({
    id=106,
    lv=71,
    value=22000
})

growth_curve:upsert({
    id=106,
    lv=72,
    value=22100
})

growth_curve:upsert({
    id=106,
    lv=73,
    value=22200
})

growth_curve:upsert({
    id=106,
    lv=74,
    value=22300
})

growth_curve:upsert({
    id=106,
    lv=75,
    value=22400
})

growth_curve:upsert({
    id=106,
    lv=76,
    value=22500
})

growth_curve:upsert({
    id=106,
    lv=77,
    value=22600
})

growth_curve:upsert({
    id=106,
    lv=78,
    value=22700
})

growth_curve:upsert({
    id=106,
    lv=79,
    value=22800
})

growth_curve:upsert({
    id=106,
    lv=80,
    value=22900
})

growth_curve:upsert({
    id=106,
    lv=81,
    value=23000
})

growth_curve:upsert({
    id=106,
    lv=82,
    value=23100
})

growth_curve:upsert({
    id=106,
    lv=83,
    value=23200
})

growth_curve:upsert({
    id=106,
    lv=84,
    value=23300
})

growth_curve:upsert({
    id=106,
    lv=85,
    value=23400
})

growth_curve:upsert({
    id=106,
    lv=86,
    value=23500
})

growth_curve:upsert({
    id=106,
    lv=87,
    value=23600
})

growth_curve:upsert({
    id=106,
    lv=88,
    value=23700
})

growth_curve:upsert({
    id=106,
    lv=89,
    value=23800
})

growth_curve:upsert({
    id=106,
    lv=90,
    value=23900
})

growth_curve:upsert({
    id=106,
    lv=91,
    value=24000
})

growth_curve:upsert({
    id=106,
    lv=92,
    value=24100
})

growth_curve:upsert({
    id=106,
    lv=93,
    value=24200
})

growth_curve:upsert({
    id=106,
    lv=94,
    value=24300
})

growth_curve:upsert({
    id=106,
    lv=95,
    value=24400
})

growth_curve:upsert({
    id=106,
    lv=96,
    value=24500
})

growth_curve:upsert({
    id=106,
    lv=97,
    value=24600
})

growth_curve:upsert({
    id=106,
    lv=98,
    value=24700
})

growth_curve:upsert({
    id=106,
    lv=99,
    value=24800
})

growth_curve:upsert({
    id=106,
    lv=100,
    value=24900
})

growth_curve:upsert({
    id=107,
    lv=1,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=2,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=3,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=4,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=5,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=6,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=7,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=8,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=9,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=10,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=11,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=12,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=13,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=14,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=15,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=16,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=17,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=18,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=19,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=20,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=21,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=22,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=23,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=24,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=25,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=26,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=27,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=28,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=29,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=30,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=31,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=32,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=33,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=34,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=35,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=36,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=37,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=38,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=39,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=40,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=41,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=42,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=43,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=44,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=45,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=46,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=47,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=48,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=49,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=50,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=51,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=52,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=53,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=54,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=55,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=56,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=57,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=58,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=59,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=60,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=61,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=62,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=63,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=64,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=65,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=66,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=67,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=68,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=69,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=70,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=71,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=72,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=73,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=74,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=75,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=76,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=77,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=78,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=79,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=80,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=81,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=82,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=83,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=84,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=85,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=86,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=87,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=88,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=89,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=90,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=91,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=92,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=93,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=94,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=95,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=96,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=97,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=98,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=99,
    value=300
})

growth_curve:upsert({
    id=107,
    lv=100,
    value=300
})

growth_curve:upsert({
    id=1002,
    lv=1,
    value=300
})

growth_curve:upsert({
    id=1002,
    lv=2,
    value=400
})

growth_curve:upsert({
    id=1002,
    lv=3,
    value=500
})

growth_curve:upsert({
    id=1002,
    lv=4,
    value=600
})

growth_curve:upsert({
    id=1002,
    lv=5,
    value=700
})

growth_curve:upsert({
    id=1002,
    lv=6,
    value=800
})

growth_curve:upsert({
    id=1002,
    lv=7,
    value=900
})

growth_curve:upsert({
    id=1002,
    lv=8,
    value=1000
})

growth_curve:upsert({
    id=1002,
    lv=9,
    value=1100
})

growth_curve:upsert({
    id=1002,
    lv=10,
    value=1200
})

growth_curve:upsert({
    id=1002,
    lv=11,
    value=1300
})

growth_curve:upsert({
    id=1002,
    lv=12,
    value=1400
})

growth_curve:upsert({
    id=1002,
    lv=13,
    value=1500
})

growth_curve:upsert({
    id=1002,
    lv=14,
    value=1600
})

growth_curve:upsert({
    id=1002,
    lv=15,
    value=1700
})

growth_curve:upsert({
    id=1002,
    lv=16,
    value=1800
})

growth_curve:upsert({
    id=1002,
    lv=17,
    value=1900
})

growth_curve:upsert({
    id=1002,
    lv=18,
    value=2000
})

growth_curve:upsert({
    id=1002,
    lv=19,
    value=2100
})

growth_curve:upsert({
    id=1002,
    lv=20,
    value=2200
})

growth_curve:upsert({
    id=1002,
    lv=21,
    value=2300
})

growth_curve:upsert({
    id=1002,
    lv=22,
    value=2400
})

growth_curve:upsert({
    id=1002,
    lv=23,
    value=2500
})

growth_curve:upsert({
    id=1002,
    lv=24,
    value=2600
})

growth_curve:upsert({
    id=1002,
    lv=25,
    value=2700
})

growth_curve:upsert({
    id=1002,
    lv=26,
    value=2800
})

growth_curve:upsert({
    id=1002,
    lv=27,
    value=2900
})

growth_curve:upsert({
    id=1002,
    lv=28,
    value=3000
})

growth_curve:upsert({
    id=1002,
    lv=29,
    value=3100
})

growth_curve:upsert({
    id=1002,
    lv=30,
    value=3200
})

growth_curve:upsert({
    id=1002,
    lv=31,
    value=3300
})

growth_curve:upsert({
    id=1002,
    lv=32,
    value=3400
})

growth_curve:upsert({
    id=1002,
    lv=33,
    value=3500
})

growth_curve:upsert({
    id=1002,
    lv=34,
    value=3600
})

growth_curve:upsert({
    id=1002,
    lv=35,
    value=3700
})

growth_curve:upsert({
    id=1002,
    lv=36,
    value=3800
})

growth_curve:upsert({
    id=1002,
    lv=37,
    value=3900
})

growth_curve:upsert({
    id=1002,
    lv=38,
    value=4000
})

growth_curve:upsert({
    id=1002,
    lv=39,
    value=4100
})

growth_curve:upsert({
    id=1002,
    lv=40,
    value=4200
})

growth_curve:upsert({
    id=1002,
    lv=41,
    value=4300
})

growth_curve:upsert({
    id=1002,
    lv=42,
    value=4400
})

growth_curve:upsert({
    id=1002,
    lv=43,
    value=4500
})

growth_curve:upsert({
    id=1002,
    lv=44,
    value=4600
})

growth_curve:upsert({
    id=1002,
    lv=45,
    value=4700
})

growth_curve:upsert({
    id=1002,
    lv=46,
    value=4800
})

growth_curve:upsert({
    id=1002,
    lv=47,
    value=4900
})

growth_curve:upsert({
    id=1002,
    lv=48,
    value=5000
})

growth_curve:upsert({
    id=1002,
    lv=49,
    value=5100
})

growth_curve:upsert({
    id=1002,
    lv=50,
    value=5200
})

growth_curve:upsert({
    id=1002,
    lv=51,
    value=5300
})

growth_curve:upsert({
    id=1002,
    lv=52,
    value=5400
})

growth_curve:upsert({
    id=1002,
    lv=53,
    value=5500
})

growth_curve:upsert({
    id=1002,
    lv=54,
    value=5600
})

growth_curve:upsert({
    id=1002,
    lv=55,
    value=5700
})

growth_curve:upsert({
    id=1002,
    lv=56,
    value=5800
})

growth_curve:upsert({
    id=1002,
    lv=57,
    value=5900
})

growth_curve:upsert({
    id=1002,
    lv=58,
    value=6000
})

growth_curve:upsert({
    id=1002,
    lv=59,
    value=6100
})

growth_curve:upsert({
    id=1002,
    lv=60,
    value=6200
})

growth_curve:upsert({
    id=1002,
    lv=61,
    value=6300
})

growth_curve:upsert({
    id=1002,
    lv=62,
    value=6400
})

growth_curve:upsert({
    id=1002,
    lv=63,
    value=6500
})

growth_curve:upsert({
    id=1002,
    lv=64,
    value=6600
})

growth_curve:upsert({
    id=1002,
    lv=65,
    value=6700
})

growth_curve:upsert({
    id=1002,
    lv=66,
    value=6800
})

growth_curve:upsert({
    id=1002,
    lv=67,
    value=6900
})

growth_curve:upsert({
    id=1002,
    lv=68,
    value=7000
})

growth_curve:upsert({
    id=1002,
    lv=69,
    value=7100
})

growth_curve:upsert({
    id=1002,
    lv=70,
    value=7200
})

growth_curve:upsert({
    id=1002,
    lv=71,
    value=7300
})

growth_curve:upsert({
    id=1002,
    lv=72,
    value=7400
})

growth_curve:upsert({
    id=1002,
    lv=73,
    value=7500
})

growth_curve:upsert({
    id=1002,
    lv=74,
    value=7600
})

growth_curve:upsert({
    id=1002,
    lv=75,
    value=7700
})

growth_curve:upsert({
    id=1002,
    lv=76,
    value=7800
})

growth_curve:upsert({
    id=1002,
    lv=77,
    value=7900
})

growth_curve:upsert({
    id=1002,
    lv=78,
    value=8000
})

growth_curve:upsert({
    id=1002,
    lv=79,
    value=8100
})

growth_curve:upsert({
    id=1002,
    lv=80,
    value=8200
})

growth_curve:upsert({
    id=1002,
    lv=81,
    value=8300
})

growth_curve:upsert({
    id=1002,
    lv=82,
    value=8400
})

growth_curve:upsert({
    id=1002,
    lv=83,
    value=8500
})

growth_curve:upsert({
    id=1002,
    lv=84,
    value=8600
})

growth_curve:upsert({
    id=1002,
    lv=85,
    value=8700
})

growth_curve:upsert({
    id=1002,
    lv=86,
    value=8800
})

growth_curve:upsert({
    id=1002,
    lv=87,
    value=8900
})

growth_curve:upsert({
    id=1002,
    lv=88,
    value=9000
})

growth_curve:upsert({
    id=1002,
    lv=89,
    value=9100
})

growth_curve:upsert({
    id=1002,
    lv=90,
    value=9200
})

growth_curve:upsert({
    id=1002,
    lv=91,
    value=9300
})

growth_curve:upsert({
    id=1002,
    lv=92,
    value=9400
})

growth_curve:upsert({
    id=1002,
    lv=93,
    value=9500
})

growth_curve:upsert({
    id=1002,
    lv=94,
    value=9600
})

growth_curve:upsert({
    id=1002,
    lv=95,
    value=9700
})

growth_curve:upsert({
    id=1002,
    lv=96,
    value=9800
})

growth_curve:upsert({
    id=1002,
    lv=97,
    value=9900
})

growth_curve:upsert({
    id=1002,
    lv=98,
    value=10000
})

growth_curve:upsert({
    id=1002,
    lv=99,
    value=10100
})

growth_curve:upsert({
    id=1002,
    lv=100,
    value=10200
})

growth_curve:upsert({
    id=1003,
    lv=1,
    value=60
})

growth_curve:upsert({
    id=1003,
    lv=2,
    value=80
})

growth_curve:upsert({
    id=1003,
    lv=3,
    value=100
})

growth_curve:upsert({
    id=1003,
    lv=4,
    value=120
})

growth_curve:upsert({
    id=1003,
    lv=5,
    value=140
})

growth_curve:upsert({
    id=1003,
    lv=6,
    value=160
})

growth_curve:upsert({
    id=1003,
    lv=7,
    value=180
})

growth_curve:upsert({
    id=1003,
    lv=8,
    value=200
})

growth_curve:upsert({
    id=1003,
    lv=9,
    value=220
})

growth_curve:upsert({
    id=1003,
    lv=10,
    value=240
})

growth_curve:upsert({
    id=1003,
    lv=11,
    value=260
})

growth_curve:upsert({
    id=1003,
    lv=12,
    value=280
})

growth_curve:upsert({
    id=1003,
    lv=13,
    value=300
})

growth_curve:upsert({
    id=1003,
    lv=14,
    value=320
})

growth_curve:upsert({
    id=1003,
    lv=15,
    value=340
})

growth_curve:upsert({
    id=1003,
    lv=16,
    value=360
})

growth_curve:upsert({
    id=1003,
    lv=17,
    value=380
})

growth_curve:upsert({
    id=1003,
    lv=18,
    value=400
})

growth_curve:upsert({
    id=1003,
    lv=19,
    value=420
})

growth_curve:upsert({
    id=1003,
    lv=20,
    value=440
})

growth_curve:upsert({
    id=1003,
    lv=21,
    value=460
})

growth_curve:upsert({
    id=1003,
    lv=22,
    value=480
})

growth_curve:upsert({
    id=1003,
    lv=23,
    value=500
})

growth_curve:upsert({
    id=1003,
    lv=24,
    value=520
})

growth_curve:upsert({
    id=1003,
    lv=25,
    value=540
})

growth_curve:upsert({
    id=1003,
    lv=26,
    value=560
})

growth_curve:upsert({
    id=1003,
    lv=27,
    value=580
})

growth_curve:upsert({
    id=1003,
    lv=28,
    value=600
})

growth_curve:upsert({
    id=1003,
    lv=29,
    value=620
})

growth_curve:upsert({
    id=1003,
    lv=30,
    value=640
})

growth_curve:upsert({
    id=1003,
    lv=31,
    value=660
})

growth_curve:upsert({
    id=1003,
    lv=32,
    value=680
})

growth_curve:upsert({
    id=1003,
    lv=33,
    value=700
})

growth_curve:upsert({
    id=1003,
    lv=34,
    value=720
})

growth_curve:upsert({
    id=1003,
    lv=35,
    value=740
})

growth_curve:upsert({
    id=1003,
    lv=36,
    value=760
})

growth_curve:upsert({
    id=1003,
    lv=37,
    value=780
})

growth_curve:upsert({
    id=1003,
    lv=38,
    value=800
})

growth_curve:upsert({
    id=1003,
    lv=39,
    value=820
})

growth_curve:upsert({
    id=1003,
    lv=40,
    value=840
})

growth_curve:upsert({
    id=1003,
    lv=41,
    value=860
})

growth_curve:upsert({
    id=1003,
    lv=42,
    value=880
})

growth_curve:upsert({
    id=1003,
    lv=43,
    value=900
})

growth_curve:upsert({
    id=1003,
    lv=44,
    value=920
})

growth_curve:upsert({
    id=1003,
    lv=45,
    value=940
})

growth_curve:upsert({
    id=1003,
    lv=46,
    value=960
})

growth_curve:upsert({
    id=1003,
    lv=47,
    value=980
})

growth_curve:upsert({
    id=1003,
    lv=48,
    value=1000
})

growth_curve:upsert({
    id=1003,
    lv=49,
    value=1020
})

growth_curve:upsert({
    id=1003,
    lv=50,
    value=1040
})

growth_curve:upsert({
    id=1003,
    lv=51,
    value=1060
})

growth_curve:upsert({
    id=1003,
    lv=52,
    value=1080
})

growth_curve:upsert({
    id=1003,
    lv=53,
    value=1100
})

growth_curve:upsert({
    id=1003,
    lv=54,
    value=1120
})

growth_curve:upsert({
    id=1003,
    lv=55,
    value=1140
})

growth_curve:upsert({
    id=1003,
    lv=56,
    value=1160
})

growth_curve:upsert({
    id=1003,
    lv=57,
    value=1180
})

growth_curve:upsert({
    id=1003,
    lv=58,
    value=1200
})

growth_curve:upsert({
    id=1003,
    lv=59,
    value=1220
})

growth_curve:upsert({
    id=1003,
    lv=60,
    value=1240
})

growth_curve:upsert({
    id=1003,
    lv=61,
    value=1260
})

growth_curve:upsert({
    id=1003,
    lv=62,
    value=1280
})

growth_curve:upsert({
    id=1003,
    lv=63,
    value=1300
})

growth_curve:upsert({
    id=1003,
    lv=64,
    value=1320
})

growth_curve:upsert({
    id=1003,
    lv=65,
    value=1340
})

growth_curve:upsert({
    id=1003,
    lv=66,
    value=1360
})

growth_curve:upsert({
    id=1003,
    lv=67,
    value=1380
})

growth_curve:upsert({
    id=1003,
    lv=68,
    value=1400
})

growth_curve:upsert({
    id=1003,
    lv=69,
    value=1420
})

growth_curve:upsert({
    id=1003,
    lv=70,
    value=1440
})

growth_curve:upsert({
    id=1003,
    lv=71,
    value=1460
})

growth_curve:upsert({
    id=1003,
    lv=72,
    value=1480
})

growth_curve:upsert({
    id=1003,
    lv=73,
    value=1500
})

growth_curve:upsert({
    id=1003,
    lv=74,
    value=1520
})

growth_curve:upsert({
    id=1003,
    lv=75,
    value=1540
})

growth_curve:upsert({
    id=1003,
    lv=76,
    value=1560
})

growth_curve:upsert({
    id=1003,
    lv=77,
    value=1580
})

growth_curve:upsert({
    id=1003,
    lv=78,
    value=1600
})

growth_curve:upsert({
    id=1003,
    lv=79,
    value=1620
})

growth_curve:upsert({
    id=1003,
    lv=80,
    value=1640
})

growth_curve:upsert({
    id=1003,
    lv=81,
    value=1660
})

growth_curve:upsert({
    id=1003,
    lv=82,
    value=1680
})

growth_curve:upsert({
    id=1003,
    lv=83,
    value=1700
})

growth_curve:upsert({
    id=1003,
    lv=84,
    value=1720
})

growth_curve:upsert({
    id=1003,
    lv=85,
    value=1740
})

growth_curve:upsert({
    id=1003,
    lv=86,
    value=1760
})

growth_curve:upsert({
    id=1003,
    lv=87,
    value=1780
})

growth_curve:upsert({
    id=1003,
    lv=88,
    value=1800
})

growth_curve:upsert({
    id=1003,
    lv=89,
    value=1820
})

growth_curve:upsert({
    id=1003,
    lv=90,
    value=1840
})

growth_curve:upsert({
    id=1003,
    lv=91,
    value=1860
})

growth_curve:upsert({
    id=1003,
    lv=92,
    value=1880
})

growth_curve:upsert({
    id=1003,
    lv=93,
    value=1900
})

growth_curve:upsert({
    id=1003,
    lv=94,
    value=1920
})

growth_curve:upsert({
    id=1003,
    lv=95,
    value=1940
})

growth_curve:upsert({
    id=1003,
    lv=96,
    value=1960
})

growth_curve:upsert({
    id=1003,
    lv=97,
    value=1980
})

growth_curve:upsert({
    id=1003,
    lv=98,
    value=2000
})

growth_curve:upsert({
    id=1003,
    lv=99,
    value=2020
})

growth_curve:upsert({
    id=1003,
    lv=100,
    value=2040
})

growth_curve:upsert({
    id=1004,
    lv=1,
    value=36
})

growth_curve:upsert({
    id=1004,
    lv=2,
    value=48
})

growth_curve:upsert({
    id=1004,
    lv=3,
    value=60
})

growth_curve:upsert({
    id=1004,
    lv=4,
    value=72
})

growth_curve:upsert({
    id=1004,
    lv=5,
    value=84
})

growth_curve:upsert({
    id=1004,
    lv=6,
    value=96
})

growth_curve:upsert({
    id=1004,
    lv=7,
    value=108
})

growth_curve:upsert({
    id=1004,
    lv=8,
    value=120
})

growth_curve:upsert({
    id=1004,
    lv=9,
    value=132
})

growth_curve:upsert({
    id=1004,
    lv=10,
    value=144
})

growth_curve:upsert({
    id=1004,
    lv=11,
    value=156
})

growth_curve:upsert({
    id=1004,
    lv=12,
    value=168
})

growth_curve:upsert({
    id=1004,
    lv=13,
    value=180
})

growth_curve:upsert({
    id=1004,
    lv=14,
    value=192
})

growth_curve:upsert({
    id=1004,
    lv=15,
    value=204
})

growth_curve:upsert({
    id=1004,
    lv=16,
    value=216
})

growth_curve:upsert({
    id=1004,
    lv=17,
    value=228
})

growth_curve:upsert({
    id=1004,
    lv=18,
    value=240
})

growth_curve:upsert({
    id=1004,
    lv=19,
    value=252
})

growth_curve:upsert({
    id=1004,
    lv=20,
    value=264
})

growth_curve:upsert({
    id=1004,
    lv=21,
    value=276
})

growth_curve:upsert({
    id=1004,
    lv=22,
    value=288
})

growth_curve:upsert({
    id=1004,
    lv=23,
    value=300
})

growth_curve:upsert({
    id=1004,
    lv=24,
    value=312
})

growth_curve:upsert({
    id=1004,
    lv=25,
    value=324
})

growth_curve:upsert({
    id=1004,
    lv=26,
    value=336
})

growth_curve:upsert({
    id=1004,
    lv=27,
    value=348
})

growth_curve:upsert({
    id=1004,
    lv=28,
    value=360
})

growth_curve:upsert({
    id=1004,
    lv=29,
    value=372
})

growth_curve:upsert({
    id=1004,
    lv=30,
    value=384
})

growth_curve:upsert({
    id=1004,
    lv=31,
    value=396
})

growth_curve:upsert({
    id=1004,
    lv=32,
    value=408
})

growth_curve:upsert({
    id=1004,
    lv=33,
    value=420
})

growth_curve:upsert({
    id=1004,
    lv=34,
    value=432
})

growth_curve:upsert({
    id=1004,
    lv=35,
    value=444
})

growth_curve:upsert({
    id=1004,
    lv=36,
    value=456
})

growth_curve:upsert({
    id=1004,
    lv=37,
    value=468
})

growth_curve:upsert({
    id=1004,
    lv=38,
    value=480
})

growth_curve:upsert({
    id=1004,
    lv=39,
    value=492
})

growth_curve:upsert({
    id=1004,
    lv=40,
    value=504
})

growth_curve:upsert({
    id=1004,
    lv=41,
    value=516
})

growth_curve:upsert({
    id=1004,
    lv=42,
    value=528
})

growth_curve:upsert({
    id=1004,
    lv=43,
    value=540
})

growth_curve:upsert({
    id=1004,
    lv=44,
    value=552
})

growth_curve:upsert({
    id=1004,
    lv=45,
    value=564
})

growth_curve:upsert({
    id=1004,
    lv=46,
    value=576
})

growth_curve:upsert({
    id=1004,
    lv=47,
    value=588
})

growth_curve:upsert({
    id=1004,
    lv=48,
    value=600
})

growth_curve:upsert({
    id=1004,
    lv=49,
    value=612
})

growth_curve:upsert({
    id=1004,
    lv=50,
    value=624
})

growth_curve:upsert({
    id=1004,
    lv=51,
    value=636
})

growth_curve:upsert({
    id=1004,
    lv=52,
    value=648
})

growth_curve:upsert({
    id=1004,
    lv=53,
    value=660
})

growth_curve:upsert({
    id=1004,
    lv=54,
    value=672
})

growth_curve:upsert({
    id=1004,
    lv=55,
    value=684
})

growth_curve:upsert({
    id=1004,
    lv=56,
    value=696
})

growth_curve:upsert({
    id=1004,
    lv=57,
    value=708
})

growth_curve:upsert({
    id=1004,
    lv=58,
    value=720
})

growth_curve:upsert({
    id=1004,
    lv=59,
    value=732
})

growth_curve:upsert({
    id=1004,
    lv=60,
    value=744
})

growth_curve:upsert({
    id=1004,
    lv=61,
    value=756
})

growth_curve:upsert({
    id=1004,
    lv=62,
    value=768
})

growth_curve:upsert({
    id=1004,
    lv=63,
    value=780
})

growth_curve:upsert({
    id=1004,
    lv=64,
    value=792
})

growth_curve:upsert({
    id=1004,
    lv=65,
    value=804
})

growth_curve:upsert({
    id=1004,
    lv=66,
    value=816
})

growth_curve:upsert({
    id=1004,
    lv=67,
    value=828
})

growth_curve:upsert({
    id=1004,
    lv=68,
    value=840
})

growth_curve:upsert({
    id=1004,
    lv=69,
    value=852
})

growth_curve:upsert({
    id=1004,
    lv=70,
    value=864
})

growth_curve:upsert({
    id=1004,
    lv=71,
    value=876
})

growth_curve:upsert({
    id=1004,
    lv=72,
    value=888
})

growth_curve:upsert({
    id=1004,
    lv=73,
    value=900
})

growth_curve:upsert({
    id=1004,
    lv=74,
    value=912
})

growth_curve:upsert({
    id=1004,
    lv=75,
    value=924
})

growth_curve:upsert({
    id=1004,
    lv=76,
    value=936
})

growth_curve:upsert({
    id=1004,
    lv=77,
    value=948
})

growth_curve:upsert({
    id=1004,
    lv=78,
    value=960
})

growth_curve:upsert({
    id=1004,
    lv=79,
    value=972
})

growth_curve:upsert({
    id=1004,
    lv=80,
    value=984
})

growth_curve:upsert({
    id=1004,
    lv=81,
    value=996
})

growth_curve:upsert({
    id=1004,
    lv=82,
    value=1008
})

growth_curve:upsert({
    id=1004,
    lv=83,
    value=1020
})

growth_curve:upsert({
    id=1004,
    lv=84,
    value=1032
})

growth_curve:upsert({
    id=1004,
    lv=85,
    value=1044
})

growth_curve:upsert({
    id=1004,
    lv=86,
    value=1056
})

growth_curve:upsert({
    id=1004,
    lv=87,
    value=1068
})

growth_curve:upsert({
    id=1004,
    lv=88,
    value=1080
})

growth_curve:upsert({
    id=1004,
    lv=89,
    value=1092
})

growth_curve:upsert({
    id=1004,
    lv=90,
    value=1104
})

growth_curve:upsert({
    id=1004,
    lv=91,
    value=1116
})

growth_curve:upsert({
    id=1004,
    lv=92,
    value=1128
})

growth_curve:upsert({
    id=1004,
    lv=93,
    value=1140
})

growth_curve:upsert({
    id=1004,
    lv=94,
    value=1152
})

growth_curve:upsert({
    id=1004,
    lv=95,
    value=1164
})

growth_curve:upsert({
    id=1004,
    lv=96,
    value=1176
})

growth_curve:upsert({
    id=1004,
    lv=97,
    value=1188
})

growth_curve:upsert({
    id=1004,
    lv=98,
    value=1200
})

growth_curve:upsert({
    id=1004,
    lv=99,
    value=1212
})

growth_curve:upsert({
    id=1004,
    lv=100,
    value=1224
})

growth_curve:upsert({
    id=1005,
    lv=1,
    value=505
})

growth_curve:upsert({
    id=1005,
    lv=2,
    value=510
})

growth_curve:upsert({
    id=1005,
    lv=3,
    value=515
})

growth_curve:upsert({
    id=1005,
    lv=4,
    value=520
})

growth_curve:upsert({
    id=1005,
    lv=5,
    value=525
})

growth_curve:upsert({
    id=1005,
    lv=6,
    value=530
})

growth_curve:upsert({
    id=1005,
    lv=7,
    value=535
})

growth_curve:upsert({
    id=1005,
    lv=8,
    value=540
})

growth_curve:upsert({
    id=1005,
    lv=9,
    value=545
})

growth_curve:upsert({
    id=1005,
    lv=10,
    value=550
})

growth_curve:upsert({
    id=1005,
    lv=11,
    value=555
})

growth_curve:upsert({
    id=1005,
    lv=12,
    value=560
})

growth_curve:upsert({
    id=1005,
    lv=13,
    value=565
})

growth_curve:upsert({
    id=1005,
    lv=14,
    value=570
})

growth_curve:upsert({
    id=1005,
    lv=15,
    value=575
})

growth_curve:upsert({
    id=1005,
    lv=16,
    value=580
})

growth_curve:upsert({
    id=1005,
    lv=17,
    value=585
})

growth_curve:upsert({
    id=1005,
    lv=18,
    value=590
})

growth_curve:upsert({
    id=1005,
    lv=19,
    value=595
})

growth_curve:upsert({
    id=1005,
    lv=20,
    value=600
})

growth_curve:upsert({
    id=1005,
    lv=21,
    value=605
})

growth_curve:upsert({
    id=1005,
    lv=22,
    value=610
})

growth_curve:upsert({
    id=1005,
    lv=23,
    value=615
})

growth_curve:upsert({
    id=1005,
    lv=24,
    value=620
})

growth_curve:upsert({
    id=1005,
    lv=25,
    value=625
})

growth_curve:upsert({
    id=1005,
    lv=26,
    value=630
})

growth_curve:upsert({
    id=1005,
    lv=27,
    value=635
})

growth_curve:upsert({
    id=1005,
    lv=28,
    value=640
})

growth_curve:upsert({
    id=1005,
    lv=29,
    value=645
})

growth_curve:upsert({
    id=1005,
    lv=30,
    value=650
})

growth_curve:upsert({
    id=1005,
    lv=31,
    value=655
})

growth_curve:upsert({
    id=1005,
    lv=32,
    value=660
})

growth_curve:upsert({
    id=1005,
    lv=33,
    value=665
})

growth_curve:upsert({
    id=1005,
    lv=34,
    value=670
})

growth_curve:upsert({
    id=1005,
    lv=35,
    value=675
})

growth_curve:upsert({
    id=1005,
    lv=36,
    value=680
})

growth_curve:upsert({
    id=1005,
    lv=37,
    value=685
})

growth_curve:upsert({
    id=1005,
    lv=38,
    value=690
})

growth_curve:upsert({
    id=1005,
    lv=39,
    value=695
})

growth_curve:upsert({
    id=1005,
    lv=40,
    value=700
})

growth_curve:upsert({
    id=1005,
    lv=41,
    value=705
})

growth_curve:upsert({
    id=1005,
    lv=42,
    value=710
})

growth_curve:upsert({
    id=1005,
    lv=43,
    value=715
})

growth_curve:upsert({
    id=1005,
    lv=44,
    value=720
})

growth_curve:upsert({
    id=1005,
    lv=45,
    value=725
})

growth_curve:upsert({
    id=1005,
    lv=46,
    value=730
})

growth_curve:upsert({
    id=1005,
    lv=47,
    value=735
})

growth_curve:upsert({
    id=1005,
    lv=48,
    value=740
})

growth_curve:upsert({
    id=1005,
    lv=49,
    value=745
})

growth_curve:upsert({
    id=1005,
    lv=50,
    value=750
})

growth_curve:upsert({
    id=1005,
    lv=51,
    value=755
})

growth_curve:upsert({
    id=1005,
    lv=52,
    value=760
})

growth_curve:upsert({
    id=1005,
    lv=53,
    value=765
})

growth_curve:upsert({
    id=1005,
    lv=54,
    value=770
})

growth_curve:upsert({
    id=1005,
    lv=55,
    value=775
})

growth_curve:upsert({
    id=1005,
    lv=56,
    value=780
})

growth_curve:upsert({
    id=1005,
    lv=57,
    value=785
})

growth_curve:upsert({
    id=1005,
    lv=58,
    value=790
})

growth_curve:upsert({
    id=1005,
    lv=59,
    value=795
})

growth_curve:upsert({
    id=1005,
    lv=60,
    value=800
})

growth_curve:upsert({
    id=1005,
    lv=61,
    value=805
})

growth_curve:upsert({
    id=1005,
    lv=62,
    value=810
})

growth_curve:upsert({
    id=1005,
    lv=63,
    value=815
})

growth_curve:upsert({
    id=1005,
    lv=64,
    value=820
})

growth_curve:upsert({
    id=1005,
    lv=65,
    value=825
})

growth_curve:upsert({
    id=1005,
    lv=66,
    value=830
})

growth_curve:upsert({
    id=1005,
    lv=67,
    value=835
})

growth_curve:upsert({
    id=1005,
    lv=68,
    value=840
})

growth_curve:upsert({
    id=1005,
    lv=69,
    value=845
})

growth_curve:upsert({
    id=1005,
    lv=70,
    value=850
})

growth_curve:upsert({
    id=1005,
    lv=71,
    value=855
})

growth_curve:upsert({
    id=1005,
    lv=72,
    value=860
})

growth_curve:upsert({
    id=1005,
    lv=73,
    value=865
})

growth_curve:upsert({
    id=1005,
    lv=74,
    value=870
})

growth_curve:upsert({
    id=1005,
    lv=75,
    value=875
})

growth_curve:upsert({
    id=1005,
    lv=76,
    value=880
})

growth_curve:upsert({
    id=1005,
    lv=77,
    value=885
})

growth_curve:upsert({
    id=1005,
    lv=78,
    value=890
})

growth_curve:upsert({
    id=1005,
    lv=79,
    value=895
})

growth_curve:upsert({
    id=1005,
    lv=80,
    value=900
})

growth_curve:upsert({
    id=1005,
    lv=81,
    value=905
})

growth_curve:upsert({
    id=1005,
    lv=82,
    value=910
})

growth_curve:upsert({
    id=1005,
    lv=83,
    value=915
})

growth_curve:upsert({
    id=1005,
    lv=84,
    value=920
})

growth_curve:upsert({
    id=1005,
    lv=85,
    value=925
})

growth_curve:upsert({
    id=1005,
    lv=86,
    value=930
})

growth_curve:upsert({
    id=1005,
    lv=87,
    value=935
})

growth_curve:upsert({
    id=1005,
    lv=88,
    value=940
})

growth_curve:upsert({
    id=1005,
    lv=89,
    value=945
})

growth_curve:upsert({
    id=1005,
    lv=90,
    value=950
})

growth_curve:upsert({
    id=1005,
    lv=91,
    value=955
})

growth_curve:upsert({
    id=1005,
    lv=92,
    value=960
})

growth_curve:upsert({
    id=1005,
    lv=93,
    value=965
})

growth_curve:upsert({
    id=1005,
    lv=94,
    value=970
})

growth_curve:upsert({
    id=1005,
    lv=95,
    value=975
})

growth_curve:upsert({
    id=1005,
    lv=96,
    value=980
})

growth_curve:upsert({
    id=1005,
    lv=97,
    value=985
})

growth_curve:upsert({
    id=1005,
    lv=98,
    value=990
})

growth_curve:upsert({
    id=1005,
    lv=99,
    value=995
})

growth_curve:upsert({
    id=1005,
    lv=100,
    value=1000
})

growth_curve:upsert({
    id=1006,
    lv=1,
    value=15000
})

growth_curve:upsert({
    id=1006,
    lv=2,
    value=15050
})

growth_curve:upsert({
    id=1006,
    lv=3,
    value=15100
})

growth_curve:upsert({
    id=1006,
    lv=4,
    value=15150
})

growth_curve:upsert({
    id=1006,
    lv=5,
    value=15200
})

growth_curve:upsert({
    id=1006,
    lv=6,
    value=15250
})

growth_curve:upsert({
    id=1006,
    lv=7,
    value=15300
})

growth_curve:upsert({
    id=1006,
    lv=8,
    value=15350
})

growth_curve:upsert({
    id=1006,
    lv=9,
    value=15400
})

growth_curve:upsert({
    id=1006,
    lv=10,
    value=15450
})

growth_curve:upsert({
    id=1006,
    lv=11,
    value=15500
})

growth_curve:upsert({
    id=1006,
    lv=12,
    value=15550
})

growth_curve:upsert({
    id=1006,
    lv=13,
    value=15600
})

growth_curve:upsert({
    id=1006,
    lv=14,
    value=15650
})

growth_curve:upsert({
    id=1006,
    lv=15,
    value=15700
})

growth_curve:upsert({
    id=1006,
    lv=16,
    value=15750
})

growth_curve:upsert({
    id=1006,
    lv=17,
    value=15800
})

growth_curve:upsert({
    id=1006,
    lv=18,
    value=15850
})

growth_curve:upsert({
    id=1006,
    lv=19,
    value=15900
})

growth_curve:upsert({
    id=1006,
    lv=20,
    value=15950
})

growth_curve:upsert({
    id=1006,
    lv=21,
    value=16000
})

growth_curve:upsert({
    id=1006,
    lv=22,
    value=16050
})

growth_curve:upsert({
    id=1006,
    lv=23,
    value=16100
})

growth_curve:upsert({
    id=1006,
    lv=24,
    value=16150
})

growth_curve:upsert({
    id=1006,
    lv=25,
    value=16200
})

growth_curve:upsert({
    id=1006,
    lv=26,
    value=16250
})

growth_curve:upsert({
    id=1006,
    lv=27,
    value=16300
})

growth_curve:upsert({
    id=1006,
    lv=28,
    value=16350
})

growth_curve:upsert({
    id=1006,
    lv=29,
    value=16400
})

growth_curve:upsert({
    id=1006,
    lv=30,
    value=16450
})

growth_curve:upsert({
    id=1006,
    lv=31,
    value=16500
})

growth_curve:upsert({
    id=1006,
    lv=32,
    value=16550
})

growth_curve:upsert({
    id=1006,
    lv=33,
    value=16600
})

growth_curve:upsert({
    id=1006,
    lv=34,
    value=16650
})

growth_curve:upsert({
    id=1006,
    lv=35,
    value=16700
})

growth_curve:upsert({
    id=1006,
    lv=36,
    value=16750
})

growth_curve:upsert({
    id=1006,
    lv=37,
    value=16800
})

growth_curve:upsert({
    id=1006,
    lv=38,
    value=16850
})

growth_curve:upsert({
    id=1006,
    lv=39,
    value=16900
})

growth_curve:upsert({
    id=1006,
    lv=40,
    value=16950
})

growth_curve:upsert({
    id=1006,
    lv=41,
    value=17000
})

growth_curve:upsert({
    id=1006,
    lv=42,
    value=17050
})

growth_curve:upsert({
    id=1006,
    lv=43,
    value=17100
})

growth_curve:upsert({
    id=1006,
    lv=44,
    value=17150
})

growth_curve:upsert({
    id=1006,
    lv=45,
    value=17200
})

growth_curve:upsert({
    id=1006,
    lv=46,
    value=17250
})

growth_curve:upsert({
    id=1006,
    lv=47,
    value=17300
})

growth_curve:upsert({
    id=1006,
    lv=48,
    value=17350
})

growth_curve:upsert({
    id=1006,
    lv=49,
    value=17400
})

growth_curve:upsert({
    id=1006,
    lv=50,
    value=17450
})

growth_curve:upsert({
    id=1006,
    lv=51,
    value=17500
})

growth_curve:upsert({
    id=1006,
    lv=52,
    value=17550
})

growth_curve:upsert({
    id=1006,
    lv=53,
    value=17600
})

growth_curve:upsert({
    id=1006,
    lv=54,
    value=17650
})

growth_curve:upsert({
    id=1006,
    lv=55,
    value=17700
})

growth_curve:upsert({
    id=1006,
    lv=56,
    value=17750
})

growth_curve:upsert({
    id=1006,
    lv=57,
    value=17800
})

growth_curve:upsert({
    id=1006,
    lv=58,
    value=17850
})

growth_curve:upsert({
    id=1006,
    lv=59,
    value=17900
})

growth_curve:upsert({
    id=1006,
    lv=60,
    value=17950
})

growth_curve:upsert({
    id=1006,
    lv=61,
    value=18000
})

growth_curve:upsert({
    id=1006,
    lv=62,
    value=18050
})

growth_curve:upsert({
    id=1006,
    lv=63,
    value=18100
})

growth_curve:upsert({
    id=1006,
    lv=64,
    value=18150
})

growth_curve:upsert({
    id=1006,
    lv=65,
    value=18200
})

growth_curve:upsert({
    id=1006,
    lv=66,
    value=18250
})

growth_curve:upsert({
    id=1006,
    lv=67,
    value=18300
})

growth_curve:upsert({
    id=1006,
    lv=68,
    value=18350
})

growth_curve:upsert({
    id=1006,
    lv=69,
    value=18400
})

growth_curve:upsert({
    id=1006,
    lv=70,
    value=18450
})

growth_curve:upsert({
    id=1006,
    lv=71,
    value=18500
})

growth_curve:upsert({
    id=1006,
    lv=72,
    value=18550
})

growth_curve:upsert({
    id=1006,
    lv=73,
    value=18600
})

growth_curve:upsert({
    id=1006,
    lv=74,
    value=18650
})

growth_curve:upsert({
    id=1006,
    lv=75,
    value=18700
})

growth_curve:upsert({
    id=1006,
    lv=76,
    value=18750
})

growth_curve:upsert({
    id=1006,
    lv=77,
    value=18800
})

growth_curve:upsert({
    id=1006,
    lv=78,
    value=18850
})

growth_curve:upsert({
    id=1006,
    lv=79,
    value=18900
})

growth_curve:upsert({
    id=1006,
    lv=80,
    value=18950
})

growth_curve:upsert({
    id=1006,
    lv=81,
    value=19000
})

growth_curve:upsert({
    id=1006,
    lv=82,
    value=19050
})

growth_curve:upsert({
    id=1006,
    lv=83,
    value=19100
})

growth_curve:upsert({
    id=1006,
    lv=84,
    value=19150
})

growth_curve:upsert({
    id=1006,
    lv=85,
    value=19200
})

growth_curve:upsert({
    id=1006,
    lv=86,
    value=19250
})

growth_curve:upsert({
    id=1006,
    lv=87,
    value=19300
})

growth_curve:upsert({
    id=1006,
    lv=88,
    value=19350
})

growth_curve:upsert({
    id=1006,
    lv=89,
    value=19400
})

growth_curve:upsert({
    id=1006,
    lv=90,
    value=19450
})

growth_curve:upsert({
    id=1006,
    lv=91,
    value=19500
})

growth_curve:upsert({
    id=1006,
    lv=92,
    value=19550
})

growth_curve:upsert({
    id=1006,
    lv=93,
    value=19600
})

growth_curve:upsert({
    id=1006,
    lv=94,
    value=19650
})

growth_curve:upsert({
    id=1006,
    lv=95,
    value=19700
})

growth_curve:upsert({
    id=1006,
    lv=96,
    value=19750
})

growth_curve:upsert({
    id=1006,
    lv=97,
    value=19800
})

growth_curve:upsert({
    id=1006,
    lv=98,
    value=19850
})

growth_curve:upsert({
    id=1006,
    lv=99,
    value=19900
})

growth_curve:upsert({
    id=1006,
    lv=100,
    value=19950
})

growth_curve:upsert({
    id=1102,
    lv=1,
    value=450
})

growth_curve:upsert({
    id=1102,
    lv=2,
    value=600
})

growth_curve:upsert({
    id=1102,
    lv=3,
    value=750
})

growth_curve:upsert({
    id=1102,
    lv=4,
    value=900
})

growth_curve:upsert({
    id=1102,
    lv=5,
    value=1050
})

growth_curve:upsert({
    id=1102,
    lv=6,
    value=1200
})

growth_curve:upsert({
    id=1102,
    lv=7,
    value=1350
})

growth_curve:upsert({
    id=1102,
    lv=8,
    value=1500
})

growth_curve:upsert({
    id=1102,
    lv=9,
    value=1650
})

growth_curve:upsert({
    id=1102,
    lv=10,
    value=1800
})

growth_curve:upsert({
    id=1102,
    lv=11,
    value=1950
})

growth_curve:upsert({
    id=1102,
    lv=12,
    value=2100
})

growth_curve:upsert({
    id=1102,
    lv=13,
    value=2250
})

growth_curve:upsert({
    id=1102,
    lv=14,
    value=2400
})

growth_curve:upsert({
    id=1102,
    lv=15,
    value=2550
})

growth_curve:upsert({
    id=1102,
    lv=16,
    value=2700
})

growth_curve:upsert({
    id=1102,
    lv=17,
    value=2850
})

growth_curve:upsert({
    id=1102,
    lv=18,
    value=3000
})

growth_curve:upsert({
    id=1102,
    lv=19,
    value=3150
})

growth_curve:upsert({
    id=1102,
    lv=20,
    value=3300
})

growth_curve:upsert({
    id=1102,
    lv=21,
    value=3450
})

growth_curve:upsert({
    id=1102,
    lv=22,
    value=3600
})

growth_curve:upsert({
    id=1102,
    lv=23,
    value=3750
})

growth_curve:upsert({
    id=1102,
    lv=24,
    value=3900
})

growth_curve:upsert({
    id=1102,
    lv=25,
    value=4050
})

growth_curve:upsert({
    id=1102,
    lv=26,
    value=4200
})

growth_curve:upsert({
    id=1102,
    lv=27,
    value=4350
})

growth_curve:upsert({
    id=1102,
    lv=28,
    value=4500
})

growth_curve:upsert({
    id=1102,
    lv=29,
    value=4650
})

growth_curve:upsert({
    id=1102,
    lv=30,
    value=4800
})

growth_curve:upsert({
    id=1102,
    lv=31,
    value=4950
})

growth_curve:upsert({
    id=1102,
    lv=32,
    value=5100
})

growth_curve:upsert({
    id=1102,
    lv=33,
    value=5250
})

growth_curve:upsert({
    id=1102,
    lv=34,
    value=5400
})

growth_curve:upsert({
    id=1102,
    lv=35,
    value=5550
})

growth_curve:upsert({
    id=1102,
    lv=36,
    value=5700
})

growth_curve:upsert({
    id=1102,
    lv=37,
    value=5850
})

growth_curve:upsert({
    id=1102,
    lv=38,
    value=6000
})

growth_curve:upsert({
    id=1102,
    lv=39,
    value=6150
})

growth_curve:upsert({
    id=1102,
    lv=40,
    value=6300
})

growth_curve:upsert({
    id=1102,
    lv=41,
    value=6450
})

growth_curve:upsert({
    id=1102,
    lv=42,
    value=6600
})

growth_curve:upsert({
    id=1102,
    lv=43,
    value=6750
})

growth_curve:upsert({
    id=1102,
    lv=44,
    value=6900
})

growth_curve:upsert({
    id=1102,
    lv=45,
    value=7050
})

growth_curve:upsert({
    id=1102,
    lv=46,
    value=7200
})

growth_curve:upsert({
    id=1102,
    lv=47,
    value=7350
})

growth_curve:upsert({
    id=1102,
    lv=48,
    value=7500
})

growth_curve:upsert({
    id=1102,
    lv=49,
    value=7650
})

growth_curve:upsert({
    id=1102,
    lv=50,
    value=7800
})

growth_curve:upsert({
    id=1102,
    lv=51,
    value=7950
})

growth_curve:upsert({
    id=1102,
    lv=52,
    value=8100
})

growth_curve:upsert({
    id=1102,
    lv=53,
    value=8250
})

growth_curve:upsert({
    id=1102,
    lv=54,
    value=8400
})

growth_curve:upsert({
    id=1102,
    lv=55,
    value=8550
})

growth_curve:upsert({
    id=1102,
    lv=56,
    value=8700
})

growth_curve:upsert({
    id=1102,
    lv=57,
    value=8850
})

growth_curve:upsert({
    id=1102,
    lv=58,
    value=9000
})

growth_curve:upsert({
    id=1102,
    lv=59,
    value=9150
})

growth_curve:upsert({
    id=1102,
    lv=60,
    value=9300
})

growth_curve:upsert({
    id=1102,
    lv=61,
    value=9450
})

growth_curve:upsert({
    id=1102,
    lv=62,
    value=9600
})

growth_curve:upsert({
    id=1102,
    lv=63,
    value=9750
})

growth_curve:upsert({
    id=1102,
    lv=64,
    value=9900
})

growth_curve:upsert({
    id=1102,
    lv=65,
    value=10050
})

growth_curve:upsert({
    id=1102,
    lv=66,
    value=10200
})

growth_curve:upsert({
    id=1102,
    lv=67,
    value=10350
})

growth_curve:upsert({
    id=1102,
    lv=68,
    value=10500
})

growth_curve:upsert({
    id=1102,
    lv=69,
    value=10650
})

growth_curve:upsert({
    id=1102,
    lv=70,
    value=10800
})

growth_curve:upsert({
    id=1102,
    lv=71,
    value=10950
})

growth_curve:upsert({
    id=1102,
    lv=72,
    value=11100
})

growth_curve:upsert({
    id=1102,
    lv=73,
    value=11250
})

growth_curve:upsert({
    id=1102,
    lv=74,
    value=11400
})

growth_curve:upsert({
    id=1102,
    lv=75,
    value=11550
})

growth_curve:upsert({
    id=1102,
    lv=76,
    value=11700
})

growth_curve:upsert({
    id=1102,
    lv=77,
    value=11850
})

growth_curve:upsert({
    id=1102,
    lv=78,
    value=12000
})

growth_curve:upsert({
    id=1102,
    lv=79,
    value=12150
})

growth_curve:upsert({
    id=1102,
    lv=80,
    value=12300
})

growth_curve:upsert({
    id=1102,
    lv=81,
    value=12450
})

growth_curve:upsert({
    id=1102,
    lv=82,
    value=12600
})

growth_curve:upsert({
    id=1102,
    lv=83,
    value=12750
})

growth_curve:upsert({
    id=1102,
    lv=84,
    value=12900
})

growth_curve:upsert({
    id=1102,
    lv=85,
    value=13050
})

growth_curve:upsert({
    id=1102,
    lv=86,
    value=13200
})

growth_curve:upsert({
    id=1102,
    lv=87,
    value=13350
})

growth_curve:upsert({
    id=1102,
    lv=88,
    value=13500
})

growth_curve:upsert({
    id=1102,
    lv=89,
    value=13650
})

growth_curve:upsert({
    id=1102,
    lv=90,
    value=13800
})

growth_curve:upsert({
    id=1102,
    lv=91,
    value=13950
})

growth_curve:upsert({
    id=1102,
    lv=92,
    value=14100
})

growth_curve:upsert({
    id=1102,
    lv=93,
    value=14250
})

growth_curve:upsert({
    id=1102,
    lv=94,
    value=14400
})

growth_curve:upsert({
    id=1102,
    lv=95,
    value=14550
})

growth_curve:upsert({
    id=1102,
    lv=96,
    value=14700
})

growth_curve:upsert({
    id=1102,
    lv=97,
    value=14850
})

growth_curve:upsert({
    id=1102,
    lv=98,
    value=15000
})

growth_curve:upsert({
    id=1102,
    lv=99,
    value=15150
})

growth_curve:upsert({
    id=1102,
    lv=100,
    value=15300
})

growth_curve:upsert({
    id=1103,
    lv=1,
    value=75
})

growth_curve:upsert({
    id=1103,
    lv=2,
    value=100
})

growth_curve:upsert({
    id=1103,
    lv=3,
    value=125
})

growth_curve:upsert({
    id=1103,
    lv=4,
    value=150
})

growth_curve:upsert({
    id=1103,
    lv=5,
    value=175
})

growth_curve:upsert({
    id=1103,
    lv=6,
    value=200
})

growth_curve:upsert({
    id=1103,
    lv=7,
    value=225
})

growth_curve:upsert({
    id=1103,
    lv=8,
    value=250
})

growth_curve:upsert({
    id=1103,
    lv=9,
    value=275
})

growth_curve:upsert({
    id=1103,
    lv=10,
    value=300
})

growth_curve:upsert({
    id=1103,
    lv=11,
    value=325
})

growth_curve:upsert({
    id=1103,
    lv=12,
    value=350
})

growth_curve:upsert({
    id=1103,
    lv=13,
    value=375
})

growth_curve:upsert({
    id=1103,
    lv=14,
    value=400
})

growth_curve:upsert({
    id=1103,
    lv=15,
    value=425
})

growth_curve:upsert({
    id=1103,
    lv=16,
    value=450
})

growth_curve:upsert({
    id=1103,
    lv=17,
    value=475
})

growth_curve:upsert({
    id=1103,
    lv=18,
    value=500
})

growth_curve:upsert({
    id=1103,
    lv=19,
    value=525
})

growth_curve:upsert({
    id=1103,
    lv=20,
    value=550
})

growth_curve:upsert({
    id=1103,
    lv=21,
    value=575
})

growth_curve:upsert({
    id=1103,
    lv=22,
    value=600
})

growth_curve:upsert({
    id=1103,
    lv=23,
    value=625
})

growth_curve:upsert({
    id=1103,
    lv=24,
    value=650
})

growth_curve:upsert({
    id=1103,
    lv=25,
    value=675
})

growth_curve:upsert({
    id=1103,
    lv=26,
    value=700
})

growth_curve:upsert({
    id=1103,
    lv=27,
    value=725
})

growth_curve:upsert({
    id=1103,
    lv=28,
    value=750
})

growth_curve:upsert({
    id=1103,
    lv=29,
    value=775
})

growth_curve:upsert({
    id=1103,
    lv=30,
    value=800
})

growth_curve:upsert({
    id=1103,
    lv=31,
    value=825
})

growth_curve:upsert({
    id=1103,
    lv=32,
    value=850
})

growth_curve:upsert({
    id=1103,
    lv=33,
    value=875
})

growth_curve:upsert({
    id=1103,
    lv=34,
    value=900
})

growth_curve:upsert({
    id=1103,
    lv=35,
    value=925
})

growth_curve:upsert({
    id=1103,
    lv=36,
    value=950
})

growth_curve:upsert({
    id=1103,
    lv=37,
    value=975
})

growth_curve:upsert({
    id=1103,
    lv=38,
    value=1000
})

growth_curve:upsert({
    id=1103,
    lv=39,
    value=1025
})

growth_curve:upsert({
    id=1103,
    lv=40,
    value=1050
})

growth_curve:upsert({
    id=1103,
    lv=41,
    value=1075
})

growth_curve:upsert({
    id=1103,
    lv=42,
    value=1100
})

growth_curve:upsert({
    id=1103,
    lv=43,
    value=1125
})

growth_curve:upsert({
    id=1103,
    lv=44,
    value=1150
})

growth_curve:upsert({
    id=1103,
    lv=45,
    value=1175
})

growth_curve:upsert({
    id=1103,
    lv=46,
    value=1200
})

growth_curve:upsert({
    id=1103,
    lv=47,
    value=1225
})

growth_curve:upsert({
    id=1103,
    lv=48,
    value=1250
})

growth_curve:upsert({
    id=1103,
    lv=49,
    value=1275
})

growth_curve:upsert({
    id=1103,
    lv=50,
    value=1300
})

growth_curve:upsert({
    id=1103,
    lv=51,
    value=1325
})

growth_curve:upsert({
    id=1103,
    lv=52,
    value=1350
})

growth_curve:upsert({
    id=1103,
    lv=53,
    value=1375
})

growth_curve:upsert({
    id=1103,
    lv=54,
    value=1400
})

growth_curve:upsert({
    id=1103,
    lv=55,
    value=1425
})

growth_curve:upsert({
    id=1103,
    lv=56,
    value=1450
})

growth_curve:upsert({
    id=1103,
    lv=57,
    value=1475
})

growth_curve:upsert({
    id=1103,
    lv=58,
    value=1500
})

growth_curve:upsert({
    id=1103,
    lv=59,
    value=1525
})

growth_curve:upsert({
    id=1103,
    lv=60,
    value=1550
})

growth_curve:upsert({
    id=1103,
    lv=61,
    value=1575
})

growth_curve:upsert({
    id=1103,
    lv=62,
    value=1600
})

growth_curve:upsert({
    id=1103,
    lv=63,
    value=1625
})

growth_curve:upsert({
    id=1103,
    lv=64,
    value=1650
})

growth_curve:upsert({
    id=1103,
    lv=65,
    value=1675
})

growth_curve:upsert({
    id=1103,
    lv=66,
    value=1700
})

growth_curve:upsert({
    id=1103,
    lv=67,
    value=1725
})

growth_curve:upsert({
    id=1103,
    lv=68,
    value=1750
})

growth_curve:upsert({
    id=1103,
    lv=69,
    value=1775
})

growth_curve:upsert({
    id=1103,
    lv=70,
    value=1800
})

growth_curve:upsert({
    id=1103,
    lv=71,
    value=1825
})

growth_curve:upsert({
    id=1103,
    lv=72,
    value=1850
})

growth_curve:upsert({
    id=1103,
    lv=73,
    value=1875
})

growth_curve:upsert({
    id=1103,
    lv=74,
    value=1900
})

growth_curve:upsert({
    id=1103,
    lv=75,
    value=1925
})

growth_curve:upsert({
    id=1103,
    lv=76,
    value=1950
})

growth_curve:upsert({
    id=1103,
    lv=77,
    value=1975
})

growth_curve:upsert({
    id=1103,
    lv=78,
    value=2000
})

growth_curve:upsert({
    id=1103,
    lv=79,
    value=2025
})

growth_curve:upsert({
    id=1103,
    lv=80,
    value=2050
})

growth_curve:upsert({
    id=1103,
    lv=81,
    value=2075
})

growth_curve:upsert({
    id=1103,
    lv=82,
    value=2100
})

growth_curve:upsert({
    id=1103,
    lv=83,
    value=2125
})

growth_curve:upsert({
    id=1103,
    lv=84,
    value=2150
})

growth_curve:upsert({
    id=1103,
    lv=85,
    value=2175
})

growth_curve:upsert({
    id=1103,
    lv=86,
    value=2200
})

growth_curve:upsert({
    id=1103,
    lv=87,
    value=2225
})

growth_curve:upsert({
    id=1103,
    lv=88,
    value=2250
})

growth_curve:upsert({
    id=1103,
    lv=89,
    value=2275
})

growth_curve:upsert({
    id=1103,
    lv=90,
    value=2300
})

growth_curve:upsert({
    id=1103,
    lv=91,
    value=2325
})

growth_curve:upsert({
    id=1103,
    lv=92,
    value=2350
})

growth_curve:upsert({
    id=1103,
    lv=93,
    value=2375
})

growth_curve:upsert({
    id=1103,
    lv=94,
    value=2400
})

growth_curve:upsert({
    id=1103,
    lv=95,
    value=2425
})

growth_curve:upsert({
    id=1103,
    lv=96,
    value=2450
})

growth_curve:upsert({
    id=1103,
    lv=97,
    value=2475
})

growth_curve:upsert({
    id=1103,
    lv=98,
    value=2500
})

growth_curve:upsert({
    id=1103,
    lv=99,
    value=2525
})

growth_curve:upsert({
    id=1103,
    lv=100,
    value=2550
})

growth_curve:upsert({
    id=1104,
    lv=1,
    value=38
})

growth_curve:upsert({
    id=1104,
    lv=2,
    value=51
})

growth_curve:upsert({
    id=1104,
    lv=3,
    value=64
})

growth_curve:upsert({
    id=1104,
    lv=4,
    value=77
})

growth_curve:upsert({
    id=1104,
    lv=5,
    value=90
})

growth_curve:upsert({
    id=1104,
    lv=6,
    value=103
})

growth_curve:upsert({
    id=1104,
    lv=7,
    value=116
})

growth_curve:upsert({
    id=1104,
    lv=8,
    value=129
})

growth_curve:upsert({
    id=1104,
    lv=9,
    value=142
})

growth_curve:upsert({
    id=1104,
    lv=10,
    value=155
})

growth_curve:upsert({
    id=1104,
    lv=11,
    value=168
})

growth_curve:upsert({
    id=1104,
    lv=12,
    value=181
})

growth_curve:upsert({
    id=1104,
    lv=13,
    value=194
})

growth_curve:upsert({
    id=1104,
    lv=14,
    value=207
})

growth_curve:upsert({
    id=1104,
    lv=15,
    value=220
})

growth_curve:upsert({
    id=1104,
    lv=16,
    value=233
})

growth_curve:upsert({
    id=1104,
    lv=17,
    value=246
})

growth_curve:upsert({
    id=1104,
    lv=18,
    value=259
})

growth_curve:upsert({
    id=1104,
    lv=19,
    value=272
})

growth_curve:upsert({
    id=1104,
    lv=20,
    value=285
})

growth_curve:upsert({
    id=1104,
    lv=21,
    value=298
})

growth_curve:upsert({
    id=1104,
    lv=22,
    value=311
})

growth_curve:upsert({
    id=1104,
    lv=23,
    value=324
})

growth_curve:upsert({
    id=1104,
    lv=24,
    value=337
})

growth_curve:upsert({
    id=1104,
    lv=25,
    value=350
})

growth_curve:upsert({
    id=1104,
    lv=26,
    value=363
})

growth_curve:upsert({
    id=1104,
    lv=27,
    value=376
})

growth_curve:upsert({
    id=1104,
    lv=28,
    value=389
})

growth_curve:upsert({
    id=1104,
    lv=29,
    value=402
})

growth_curve:upsert({
    id=1104,
    lv=30,
    value=415
})

growth_curve:upsert({
    id=1104,
    lv=31,
    value=428
})

growth_curve:upsert({
    id=1104,
    lv=32,
    value=441
})

growth_curve:upsert({
    id=1104,
    lv=33,
    value=454
})

growth_curve:upsert({
    id=1104,
    lv=34,
    value=467
})

growth_curve:upsert({
    id=1104,
    lv=35,
    value=480
})

growth_curve:upsert({
    id=1104,
    lv=36,
    value=493
})

growth_curve:upsert({
    id=1104,
    lv=37,
    value=506
})

growth_curve:upsert({
    id=1104,
    lv=38,
    value=519
})

growth_curve:upsert({
    id=1104,
    lv=39,
    value=532
})

growth_curve:upsert({
    id=1104,
    lv=40,
    value=545
})

growth_curve:upsert({
    id=1104,
    lv=41,
    value=558
})

growth_curve:upsert({
    id=1104,
    lv=42,
    value=571
})

growth_curve:upsert({
    id=1104,
    lv=43,
    value=584
})

growth_curve:upsert({
    id=1104,
    lv=44,
    value=597
})

growth_curve:upsert({
    id=1104,
    lv=45,
    value=610
})

growth_curve:upsert({
    id=1104,
    lv=46,
    value=623
})

growth_curve:upsert({
    id=1104,
    lv=47,
    value=636
})

growth_curve:upsert({
    id=1104,
    lv=48,
    value=649
})

growth_curve:upsert({
    id=1104,
    lv=49,
    value=662
})

growth_curve:upsert({
    id=1104,
    lv=50,
    value=675
})

growth_curve:upsert({
    id=1104,
    lv=51,
    value=688
})

growth_curve:upsert({
    id=1104,
    lv=52,
    value=701
})

growth_curve:upsert({
    id=1104,
    lv=53,
    value=714
})

growth_curve:upsert({
    id=1104,
    lv=54,
    value=727
})

growth_curve:upsert({
    id=1104,
    lv=55,
    value=740
})

growth_curve:upsert({
    id=1104,
    lv=56,
    value=753
})

growth_curve:upsert({
    id=1104,
    lv=57,
    value=766
})

growth_curve:upsert({
    id=1104,
    lv=58,
    value=779
})

growth_curve:upsert({
    id=1104,
    lv=59,
    value=792
})

growth_curve:upsert({
    id=1104,
    lv=60,
    value=805
})

growth_curve:upsert({
    id=1104,
    lv=61,
    value=818
})

growth_curve:upsert({
    id=1104,
    lv=62,
    value=831
})

growth_curve:upsert({
    id=1104,
    lv=63,
    value=844
})

growth_curve:upsert({
    id=1104,
    lv=64,
    value=857
})

growth_curve:upsert({
    id=1104,
    lv=65,
    value=870
})

growth_curve:upsert({
    id=1104,
    lv=66,
    value=883
})

growth_curve:upsert({
    id=1104,
    lv=67,
    value=896
})

growth_curve:upsert({
    id=1104,
    lv=68,
    value=909
})

growth_curve:upsert({
    id=1104,
    lv=69,
    value=922
})

growth_curve:upsert({
    id=1104,
    lv=70,
    value=935
})

growth_curve:upsert({
    id=1104,
    lv=71,
    value=948
})

growth_curve:upsert({
    id=1104,
    lv=72,
    value=961
})

growth_curve:upsert({
    id=1104,
    lv=73,
    value=974
})

growth_curve:upsert({
    id=1104,
    lv=74,
    value=987
})

growth_curve:upsert({
    id=1104,
    lv=75,
    value=1000
})

growth_curve:upsert({
    id=1104,
    lv=76,
    value=1013
})

growth_curve:upsert({
    id=1104,
    lv=77,
    value=1026
})

growth_curve:upsert({
    id=1104,
    lv=78,
    value=1039
})

growth_curve:upsert({
    id=1104,
    lv=79,
    value=1052
})

growth_curve:upsert({
    id=1104,
    lv=80,
    value=1065
})

growth_curve:upsert({
    id=1104,
    lv=81,
    value=1078
})

growth_curve:upsert({
    id=1104,
    lv=82,
    value=1091
})

growth_curve:upsert({
    id=1104,
    lv=83,
    value=1104
})

growth_curve:upsert({
    id=1104,
    lv=84,
    value=1117
})

growth_curve:upsert({
    id=1104,
    lv=85,
    value=1130
})

growth_curve:upsert({
    id=1104,
    lv=86,
    value=1143
})

growth_curve:upsert({
    id=1104,
    lv=87,
    value=1156
})

growth_curve:upsert({
    id=1104,
    lv=88,
    value=1169
})

growth_curve:upsert({
    id=1104,
    lv=89,
    value=1182
})

growth_curve:upsert({
    id=1104,
    lv=90,
    value=1195
})

growth_curve:upsert({
    id=1104,
    lv=91,
    value=1208
})

growth_curve:upsert({
    id=1104,
    lv=92,
    value=1221
})

growth_curve:upsert({
    id=1104,
    lv=93,
    value=1234
})

growth_curve:upsert({
    id=1104,
    lv=94,
    value=1247
})

growth_curve:upsert({
    id=1104,
    lv=95,
    value=1260
})

growth_curve:upsert({
    id=1104,
    lv=96,
    value=1273
})

growth_curve:upsert({
    id=1104,
    lv=97,
    value=1286
})

growth_curve:upsert({
    id=1104,
    lv=98,
    value=1299
})

growth_curve:upsert({
    id=1104,
    lv=99,
    value=1312
})

growth_curve:upsert({
    id=1104,
    lv=100,
    value=1325
})

growth_curve:upsert({
    id=1105,
    lv=1,
    value=606
})

growth_curve:upsert({
    id=1105,
    lv=2,
    value=612
})

growth_curve:upsert({
    id=1105,
    lv=3,
    value=618
})

growth_curve:upsert({
    id=1105,
    lv=4,
    value=624
})

growth_curve:upsert({
    id=1105,
    lv=5,
    value=630
})

growth_curve:upsert({
    id=1105,
    lv=6,
    value=636
})

growth_curve:upsert({
    id=1105,
    lv=7,
    value=642
})

growth_curve:upsert({
    id=1105,
    lv=8,
    value=648
})

growth_curve:upsert({
    id=1105,
    lv=9,
    value=654
})

growth_curve:upsert({
    id=1105,
    lv=10,
    value=660
})

growth_curve:upsert({
    id=1105,
    lv=11,
    value=666
})

growth_curve:upsert({
    id=1105,
    lv=12,
    value=672
})

growth_curve:upsert({
    id=1105,
    lv=13,
    value=678
})

growth_curve:upsert({
    id=1105,
    lv=14,
    value=684
})

growth_curve:upsert({
    id=1105,
    lv=15,
    value=690
})

growth_curve:upsert({
    id=1105,
    lv=16,
    value=696
})

growth_curve:upsert({
    id=1105,
    lv=17,
    value=702
})

growth_curve:upsert({
    id=1105,
    lv=18,
    value=708
})

growth_curve:upsert({
    id=1105,
    lv=19,
    value=714
})

growth_curve:upsert({
    id=1105,
    lv=20,
    value=720
})

growth_curve:upsert({
    id=1105,
    lv=21,
    value=726
})

growth_curve:upsert({
    id=1105,
    lv=22,
    value=732
})

growth_curve:upsert({
    id=1105,
    lv=23,
    value=738
})

growth_curve:upsert({
    id=1105,
    lv=24,
    value=744
})

growth_curve:upsert({
    id=1105,
    lv=25,
    value=750
})

growth_curve:upsert({
    id=1105,
    lv=26,
    value=756
})

growth_curve:upsert({
    id=1105,
    lv=27,
    value=762
})

growth_curve:upsert({
    id=1105,
    lv=28,
    value=768
})

growth_curve:upsert({
    id=1105,
    lv=29,
    value=774
})

growth_curve:upsert({
    id=1105,
    lv=30,
    value=780
})

growth_curve:upsert({
    id=1105,
    lv=31,
    value=786
})

growth_curve:upsert({
    id=1105,
    lv=32,
    value=792
})

growth_curve:upsert({
    id=1105,
    lv=33,
    value=798
})

growth_curve:upsert({
    id=1105,
    lv=34,
    value=804
})

growth_curve:upsert({
    id=1105,
    lv=35,
    value=810
})

growth_curve:upsert({
    id=1105,
    lv=36,
    value=816
})

growth_curve:upsert({
    id=1105,
    lv=37,
    value=822
})

growth_curve:upsert({
    id=1105,
    lv=38,
    value=828
})

growth_curve:upsert({
    id=1105,
    lv=39,
    value=834
})

growth_curve:upsert({
    id=1105,
    lv=40,
    value=840
})

growth_curve:upsert({
    id=1105,
    lv=41,
    value=846
})

growth_curve:upsert({
    id=1105,
    lv=42,
    value=852
})

growth_curve:upsert({
    id=1105,
    lv=43,
    value=858
})

growth_curve:upsert({
    id=1105,
    lv=44,
    value=864
})

growth_curve:upsert({
    id=1105,
    lv=45,
    value=870
})

growth_curve:upsert({
    id=1105,
    lv=46,
    value=876
})

growth_curve:upsert({
    id=1105,
    lv=47,
    value=882
})

growth_curve:upsert({
    id=1105,
    lv=48,
    value=888
})

growth_curve:upsert({
    id=1105,
    lv=49,
    value=894
})

growth_curve:upsert({
    id=1105,
    lv=50,
    value=900
})

growth_curve:upsert({
    id=1105,
    lv=51,
    value=906
})

growth_curve:upsert({
    id=1105,
    lv=52,
    value=912
})

growth_curve:upsert({
    id=1105,
    lv=53,
    value=918
})

growth_curve:upsert({
    id=1105,
    lv=54,
    value=924
})

growth_curve:upsert({
    id=1105,
    lv=55,
    value=930
})

growth_curve:upsert({
    id=1105,
    lv=56,
    value=936
})

growth_curve:upsert({
    id=1105,
    lv=57,
    value=942
})

growth_curve:upsert({
    id=1105,
    lv=58,
    value=948
})

growth_curve:upsert({
    id=1105,
    lv=59,
    value=954
})

growth_curve:upsert({
    id=1105,
    lv=60,
    value=960
})

growth_curve:upsert({
    id=1105,
    lv=61,
    value=966
})

growth_curve:upsert({
    id=1105,
    lv=62,
    value=972
})

growth_curve:upsert({
    id=1105,
    lv=63,
    value=978
})

growth_curve:upsert({
    id=1105,
    lv=64,
    value=984
})

growth_curve:upsert({
    id=1105,
    lv=65,
    value=990
})

growth_curve:upsert({
    id=1105,
    lv=66,
    value=996
})

growth_curve:upsert({
    id=1105,
    lv=67,
    value=1002
})

growth_curve:upsert({
    id=1105,
    lv=68,
    value=1008
})

growth_curve:upsert({
    id=1105,
    lv=69,
    value=1014
})

growth_curve:upsert({
    id=1105,
    lv=70,
    value=1020
})

growth_curve:upsert({
    id=1105,
    lv=71,
    value=1026
})

growth_curve:upsert({
    id=1105,
    lv=72,
    value=1032
})

growth_curve:upsert({
    id=1105,
    lv=73,
    value=1038
})

growth_curve:upsert({
    id=1105,
    lv=74,
    value=1044
})

growth_curve:upsert({
    id=1105,
    lv=75,
    value=1050
})

growth_curve:upsert({
    id=1105,
    lv=76,
    value=1056
})

growth_curve:upsert({
    id=1105,
    lv=77,
    value=1062
})

growth_curve:upsert({
    id=1105,
    lv=78,
    value=1068
})

growth_curve:upsert({
    id=1105,
    lv=79,
    value=1074
})

growth_curve:upsert({
    id=1105,
    lv=80,
    value=1080
})

growth_curve:upsert({
    id=1105,
    lv=81,
    value=1086
})

growth_curve:upsert({
    id=1105,
    lv=82,
    value=1092
})

growth_curve:upsert({
    id=1105,
    lv=83,
    value=1098
})

growth_curve:upsert({
    id=1105,
    lv=84,
    value=1104
})

growth_curve:upsert({
    id=1105,
    lv=85,
    value=1110
})

growth_curve:upsert({
    id=1105,
    lv=86,
    value=1116
})

growth_curve:upsert({
    id=1105,
    lv=87,
    value=1122
})

growth_curve:upsert({
    id=1105,
    lv=88,
    value=1128
})

growth_curve:upsert({
    id=1105,
    lv=89,
    value=1134
})

growth_curve:upsert({
    id=1105,
    lv=90,
    value=1140
})

growth_curve:upsert({
    id=1105,
    lv=91,
    value=1146
})

growth_curve:upsert({
    id=1105,
    lv=92,
    value=1152
})

growth_curve:upsert({
    id=1105,
    lv=93,
    value=1158
})

growth_curve:upsert({
    id=1105,
    lv=94,
    value=1164
})

growth_curve:upsert({
    id=1105,
    lv=95,
    value=1170
})

growth_curve:upsert({
    id=1105,
    lv=96,
    value=1176
})

growth_curve:upsert({
    id=1105,
    lv=97,
    value=1182
})

growth_curve:upsert({
    id=1105,
    lv=98,
    value=1188
})

growth_curve:upsert({
    id=1105,
    lv=99,
    value=1194
})

growth_curve:upsert({
    id=1105,
    lv=100,
    value=1200
})

growth_curve:upsert({
    id=1106,
    lv=1,
    value=15000
})

growth_curve:upsert({
    id=1106,
    lv=2,
    value=15050
})

growth_curve:upsert({
    id=1106,
    lv=3,
    value=15100
})

growth_curve:upsert({
    id=1106,
    lv=4,
    value=15150
})

growth_curve:upsert({
    id=1106,
    lv=5,
    value=15200
})

growth_curve:upsert({
    id=1106,
    lv=6,
    value=15250
})

growth_curve:upsert({
    id=1106,
    lv=7,
    value=15300
})

growth_curve:upsert({
    id=1106,
    lv=8,
    value=15350
})

growth_curve:upsert({
    id=1106,
    lv=9,
    value=15400
})

growth_curve:upsert({
    id=1106,
    lv=10,
    value=15450
})

growth_curve:upsert({
    id=1106,
    lv=11,
    value=15500
})

growth_curve:upsert({
    id=1106,
    lv=12,
    value=15550
})

growth_curve:upsert({
    id=1106,
    lv=13,
    value=15600
})

growth_curve:upsert({
    id=1106,
    lv=14,
    value=15650
})

growth_curve:upsert({
    id=1106,
    lv=15,
    value=15700
})

growth_curve:upsert({
    id=1106,
    lv=16,
    value=15750
})

growth_curve:upsert({
    id=1106,
    lv=17,
    value=15800
})

growth_curve:upsert({
    id=1106,
    lv=18,
    value=15850
})

growth_curve:upsert({
    id=1106,
    lv=19,
    value=15900
})

growth_curve:upsert({
    id=1106,
    lv=20,
    value=15950
})

growth_curve:upsert({
    id=1106,
    lv=21,
    value=16000
})

growth_curve:upsert({
    id=1106,
    lv=22,
    value=16050
})

growth_curve:upsert({
    id=1106,
    lv=23,
    value=16100
})

growth_curve:upsert({
    id=1106,
    lv=24,
    value=16150
})

growth_curve:upsert({
    id=1106,
    lv=25,
    value=16200
})

growth_curve:upsert({
    id=1106,
    lv=26,
    value=16250
})

growth_curve:upsert({
    id=1106,
    lv=27,
    value=16300
})

growth_curve:upsert({
    id=1106,
    lv=28,
    value=16350
})

growth_curve:upsert({
    id=1106,
    lv=29,
    value=16400
})

growth_curve:upsert({
    id=1106,
    lv=30,
    value=16450
})

growth_curve:upsert({
    id=1106,
    lv=31,
    value=16500
})

growth_curve:upsert({
    id=1106,
    lv=32,
    value=16550
})

growth_curve:upsert({
    id=1106,
    lv=33,
    value=16600
})

growth_curve:upsert({
    id=1106,
    lv=34,
    value=16650
})

growth_curve:upsert({
    id=1106,
    lv=35,
    value=16700
})

growth_curve:upsert({
    id=1106,
    lv=36,
    value=16750
})

growth_curve:upsert({
    id=1106,
    lv=37,
    value=16800
})

growth_curve:upsert({
    id=1106,
    lv=38,
    value=16850
})

growth_curve:upsert({
    id=1106,
    lv=39,
    value=16900
})

growth_curve:upsert({
    id=1106,
    lv=40,
    value=16950
})

growth_curve:upsert({
    id=1106,
    lv=41,
    value=17000
})

growth_curve:upsert({
    id=1106,
    lv=42,
    value=17050
})

growth_curve:upsert({
    id=1106,
    lv=43,
    value=17100
})

growth_curve:upsert({
    id=1106,
    lv=44,
    value=17150
})

growth_curve:upsert({
    id=1106,
    lv=45,
    value=17200
})

growth_curve:upsert({
    id=1106,
    lv=46,
    value=17250
})

growth_curve:upsert({
    id=1106,
    lv=47,
    value=17300
})

growth_curve:upsert({
    id=1106,
    lv=48,
    value=17350
})

growth_curve:upsert({
    id=1106,
    lv=49,
    value=17400
})

growth_curve:upsert({
    id=1106,
    lv=50,
    value=17450
})

growth_curve:upsert({
    id=1106,
    lv=51,
    value=17500
})

growth_curve:upsert({
    id=1106,
    lv=52,
    value=17550
})

growth_curve:upsert({
    id=1106,
    lv=53,
    value=17600
})

growth_curve:upsert({
    id=1106,
    lv=54,
    value=17650
})

growth_curve:upsert({
    id=1106,
    lv=55,
    value=17700
})

growth_curve:upsert({
    id=1106,
    lv=56,
    value=17750
})

growth_curve:upsert({
    id=1106,
    lv=57,
    value=17800
})

growth_curve:upsert({
    id=1106,
    lv=58,
    value=17850
})

growth_curve:upsert({
    id=1106,
    lv=59,
    value=17900
})

growth_curve:upsert({
    id=1106,
    lv=60,
    value=17950
})

growth_curve:upsert({
    id=1106,
    lv=61,
    value=18000
})

growth_curve:upsert({
    id=1106,
    lv=62,
    value=18050
})

growth_curve:upsert({
    id=1106,
    lv=63,
    value=18100
})

growth_curve:upsert({
    id=1106,
    lv=64,
    value=18150
})

growth_curve:upsert({
    id=1106,
    lv=65,
    value=18200
})

growth_curve:upsert({
    id=1106,
    lv=66,
    value=18250
})

growth_curve:upsert({
    id=1106,
    lv=67,
    value=18300
})

growth_curve:upsert({
    id=1106,
    lv=68,
    value=18350
})

growth_curve:upsert({
    id=1106,
    lv=69,
    value=18400
})

growth_curve:upsert({
    id=1106,
    lv=70,
    value=18450
})

growth_curve:upsert({
    id=1106,
    lv=71,
    value=18500
})

growth_curve:upsert({
    id=1106,
    lv=72,
    value=18550
})

growth_curve:upsert({
    id=1106,
    lv=73,
    value=18600
})

growth_curve:upsert({
    id=1106,
    lv=74,
    value=18650
})

growth_curve:upsert({
    id=1106,
    lv=75,
    value=18700
})

growth_curve:upsert({
    id=1106,
    lv=76,
    value=18750
})

growth_curve:upsert({
    id=1106,
    lv=77,
    value=18800
})

growth_curve:upsert({
    id=1106,
    lv=78,
    value=18850
})

growth_curve:upsert({
    id=1106,
    lv=79,
    value=18900
})

growth_curve:upsert({
    id=1106,
    lv=80,
    value=18950
})

growth_curve:upsert({
    id=1106,
    lv=81,
    value=19000
})

growth_curve:upsert({
    id=1106,
    lv=82,
    value=19050
})

growth_curve:upsert({
    id=1106,
    lv=83,
    value=19100
})

growth_curve:upsert({
    id=1106,
    lv=84,
    value=19150
})

growth_curve:upsert({
    id=1106,
    lv=85,
    value=19200
})

growth_curve:upsert({
    id=1106,
    lv=86,
    value=19250
})

growth_curve:upsert({
    id=1106,
    lv=87,
    value=19300
})

growth_curve:upsert({
    id=1106,
    lv=88,
    value=19350
})

growth_curve:upsert({
    id=1106,
    lv=89,
    value=19400
})

growth_curve:upsert({
    id=1106,
    lv=90,
    value=19450
})

growth_curve:upsert({
    id=1106,
    lv=91,
    value=19500
})

growth_curve:upsert({
    id=1106,
    lv=92,
    value=19550
})

growth_curve:upsert({
    id=1106,
    lv=93,
    value=19600
})

growth_curve:upsert({
    id=1106,
    lv=94,
    value=19650
})

growth_curve:upsert({
    id=1106,
    lv=95,
    value=19700
})

growth_curve:upsert({
    id=1106,
    lv=96,
    value=19750
})

growth_curve:upsert({
    id=1106,
    lv=97,
    value=19800
})

growth_curve:upsert({
    id=1106,
    lv=98,
    value=19850
})

growth_curve:upsert({
    id=1106,
    lv=99,
    value=19900
})

growth_curve:upsert({
    id=1106,
    lv=100,
    value=19950
})

growth_curve:upsert({
    id=1202,
    lv=1,
    value=600
})

growth_curve:upsert({
    id=1202,
    lv=2,
    value=800
})

growth_curve:upsert({
    id=1202,
    lv=3,
    value=1000
})

growth_curve:upsert({
    id=1202,
    lv=4,
    value=1200
})

growth_curve:upsert({
    id=1202,
    lv=5,
    value=1400
})

growth_curve:upsert({
    id=1202,
    lv=6,
    value=1600
})

growth_curve:upsert({
    id=1202,
    lv=7,
    value=1800
})

growth_curve:upsert({
    id=1202,
    lv=8,
    value=2000
})

growth_curve:upsert({
    id=1202,
    lv=9,
    value=2200
})

growth_curve:upsert({
    id=1202,
    lv=10,
    value=2400
})

growth_curve:upsert({
    id=1202,
    lv=11,
    value=2600
})

growth_curve:upsert({
    id=1202,
    lv=12,
    value=2800
})

growth_curve:upsert({
    id=1202,
    lv=13,
    value=3000
})

growth_curve:upsert({
    id=1202,
    lv=14,
    value=3200
})

growth_curve:upsert({
    id=1202,
    lv=15,
    value=3400
})

growth_curve:upsert({
    id=1202,
    lv=16,
    value=3600
})

growth_curve:upsert({
    id=1202,
    lv=17,
    value=3800
})

growth_curve:upsert({
    id=1202,
    lv=18,
    value=4000
})

growth_curve:upsert({
    id=1202,
    lv=19,
    value=4200
})

growth_curve:upsert({
    id=1202,
    lv=20,
    value=4400
})

growth_curve:upsert({
    id=1202,
    lv=21,
    value=4600
})

growth_curve:upsert({
    id=1202,
    lv=22,
    value=4800
})

growth_curve:upsert({
    id=1202,
    lv=23,
    value=5000
})

growth_curve:upsert({
    id=1202,
    lv=24,
    value=5200
})

growth_curve:upsert({
    id=1202,
    lv=25,
    value=5400
})

growth_curve:upsert({
    id=1202,
    lv=26,
    value=5600
})

growth_curve:upsert({
    id=1202,
    lv=27,
    value=5800
})

growth_curve:upsert({
    id=1202,
    lv=28,
    value=6000
})

growth_curve:upsert({
    id=1202,
    lv=29,
    value=6200
})

growth_curve:upsert({
    id=1202,
    lv=30,
    value=6400
})

growth_curve:upsert({
    id=1202,
    lv=31,
    value=6600
})

growth_curve:upsert({
    id=1202,
    lv=32,
    value=6800
})

growth_curve:upsert({
    id=1202,
    lv=33,
    value=7000
})

growth_curve:upsert({
    id=1202,
    lv=34,
    value=7200
})

growth_curve:upsert({
    id=1202,
    lv=35,
    value=7400
})

growth_curve:upsert({
    id=1202,
    lv=36,
    value=7600
})

growth_curve:upsert({
    id=1202,
    lv=37,
    value=7800
})

growth_curve:upsert({
    id=1202,
    lv=38,
    value=8000
})

growth_curve:upsert({
    id=1202,
    lv=39,
    value=8200
})

growth_curve:upsert({
    id=1202,
    lv=40,
    value=8400
})

growth_curve:upsert({
    id=1202,
    lv=41,
    value=8600
})

growth_curve:upsert({
    id=1202,
    lv=42,
    value=8800
})

growth_curve:upsert({
    id=1202,
    lv=43,
    value=9000
})

growth_curve:upsert({
    id=1202,
    lv=44,
    value=9200
})

growth_curve:upsert({
    id=1202,
    lv=45,
    value=9400
})

growth_curve:upsert({
    id=1202,
    lv=46,
    value=9600
})

growth_curve:upsert({
    id=1202,
    lv=47,
    value=9800
})

growth_curve:upsert({
    id=1202,
    lv=48,
    value=10000
})

growth_curve:upsert({
    id=1202,
    lv=49,
    value=10200
})

growth_curve:upsert({
    id=1202,
    lv=50,
    value=10400
})

growth_curve:upsert({
    id=1202,
    lv=51,
    value=10600
})

growth_curve:upsert({
    id=1202,
    lv=52,
    value=10800
})

growth_curve:upsert({
    id=1202,
    lv=53,
    value=11000
})

growth_curve:upsert({
    id=1202,
    lv=54,
    value=11200
})

growth_curve:upsert({
    id=1202,
    lv=55,
    value=11400
})

growth_curve:upsert({
    id=1202,
    lv=56,
    value=11600
})

growth_curve:upsert({
    id=1202,
    lv=57,
    value=11800
})

growth_curve:upsert({
    id=1202,
    lv=58,
    value=12000
})

growth_curve:upsert({
    id=1202,
    lv=59,
    value=12200
})

growth_curve:upsert({
    id=1202,
    lv=60,
    value=12400
})

growth_curve:upsert({
    id=1202,
    lv=61,
    value=12600
})

growth_curve:upsert({
    id=1202,
    lv=62,
    value=12800
})

growth_curve:upsert({
    id=1202,
    lv=63,
    value=13000
})

growth_curve:upsert({
    id=1202,
    lv=64,
    value=13200
})

growth_curve:upsert({
    id=1202,
    lv=65,
    value=13400
})

growth_curve:upsert({
    id=1202,
    lv=66,
    value=13600
})

growth_curve:upsert({
    id=1202,
    lv=67,
    value=13800
})

growth_curve:upsert({
    id=1202,
    lv=68,
    value=14000
})

growth_curve:upsert({
    id=1202,
    lv=69,
    value=14200
})

growth_curve:upsert({
    id=1202,
    lv=70,
    value=14400
})

growth_curve:upsert({
    id=1202,
    lv=71,
    value=14600
})

growth_curve:upsert({
    id=1202,
    lv=72,
    value=14800
})

growth_curve:upsert({
    id=1202,
    lv=73,
    value=15000
})

growth_curve:upsert({
    id=1202,
    lv=74,
    value=15200
})

growth_curve:upsert({
    id=1202,
    lv=75,
    value=15400
})

growth_curve:upsert({
    id=1202,
    lv=76,
    value=15600
})

growth_curve:upsert({
    id=1202,
    lv=77,
    value=15800
})

growth_curve:upsert({
    id=1202,
    lv=78,
    value=16000
})

growth_curve:upsert({
    id=1202,
    lv=79,
    value=16200
})

growth_curve:upsert({
    id=1202,
    lv=80,
    value=16400
})

growth_curve:upsert({
    id=1202,
    lv=81,
    value=16600
})

growth_curve:upsert({
    id=1202,
    lv=82,
    value=16800
})

growth_curve:upsert({
    id=1202,
    lv=83,
    value=17000
})

growth_curve:upsert({
    id=1202,
    lv=84,
    value=17200
})

growth_curve:upsert({
    id=1202,
    lv=85,
    value=17400
})

growth_curve:upsert({
    id=1202,
    lv=86,
    value=17600
})

growth_curve:upsert({
    id=1202,
    lv=87,
    value=17800
})

growth_curve:upsert({
    id=1202,
    lv=88,
    value=18000
})

growth_curve:upsert({
    id=1202,
    lv=89,
    value=18200
})

growth_curve:upsert({
    id=1202,
    lv=90,
    value=18400
})

growth_curve:upsert({
    id=1202,
    lv=91,
    value=18600
})

growth_curve:upsert({
    id=1202,
    lv=92,
    value=18800
})

growth_curve:upsert({
    id=1202,
    lv=93,
    value=19000
})

growth_curve:upsert({
    id=1202,
    lv=94,
    value=19200
})

growth_curve:upsert({
    id=1202,
    lv=95,
    value=19400
})

growth_curve:upsert({
    id=1202,
    lv=96,
    value=19600
})

growth_curve:upsert({
    id=1202,
    lv=97,
    value=19800
})

growth_curve:upsert({
    id=1202,
    lv=98,
    value=20000
})

growth_curve:upsert({
    id=1202,
    lv=99,
    value=20200
})

growth_curve:upsert({
    id=1202,
    lv=100,
    value=20400
})

growth_curve:upsert({
    id=1203,
    lv=1,
    value=90
})

growth_curve:upsert({
    id=1203,
    lv=2,
    value=120
})

growth_curve:upsert({
    id=1203,
    lv=3,
    value=150
})

growth_curve:upsert({
    id=1203,
    lv=4,
    value=180
})

growth_curve:upsert({
    id=1203,
    lv=5,
    value=210
})

growth_curve:upsert({
    id=1203,
    lv=6,
    value=240
})

growth_curve:upsert({
    id=1203,
    lv=7,
    value=270
})

growth_curve:upsert({
    id=1203,
    lv=8,
    value=300
})

growth_curve:upsert({
    id=1203,
    lv=9,
    value=330
})

growth_curve:upsert({
    id=1203,
    lv=10,
    value=360
})

growth_curve:upsert({
    id=1203,
    lv=11,
    value=390
})

growth_curve:upsert({
    id=1203,
    lv=12,
    value=420
})

growth_curve:upsert({
    id=1203,
    lv=13,
    value=450
})

growth_curve:upsert({
    id=1203,
    lv=14,
    value=480
})

growth_curve:upsert({
    id=1203,
    lv=15,
    value=510
})

growth_curve:upsert({
    id=1203,
    lv=16,
    value=540
})

growth_curve:upsert({
    id=1203,
    lv=17,
    value=570
})

growth_curve:upsert({
    id=1203,
    lv=18,
    value=600
})

growth_curve:upsert({
    id=1203,
    lv=19,
    value=630
})

growth_curve:upsert({
    id=1203,
    lv=20,
    value=660
})

growth_curve:upsert({
    id=1203,
    lv=21,
    value=690
})

growth_curve:upsert({
    id=1203,
    lv=22,
    value=720
})

growth_curve:upsert({
    id=1203,
    lv=23,
    value=750
})

growth_curve:upsert({
    id=1203,
    lv=24,
    value=780
})

growth_curve:upsert({
    id=1203,
    lv=25,
    value=810
})

growth_curve:upsert({
    id=1203,
    lv=26,
    value=840
})

growth_curve:upsert({
    id=1203,
    lv=27,
    value=870
})

growth_curve:upsert({
    id=1203,
    lv=28,
    value=900
})

growth_curve:upsert({
    id=1203,
    lv=29,
    value=930
})

growth_curve:upsert({
    id=1203,
    lv=30,
    value=960
})

growth_curve:upsert({
    id=1203,
    lv=31,
    value=990
})

growth_curve:upsert({
    id=1203,
    lv=32,
    value=1020
})

growth_curve:upsert({
    id=1203,
    lv=33,
    value=1050
})

growth_curve:upsert({
    id=1203,
    lv=34,
    value=1080
})

growth_curve:upsert({
    id=1203,
    lv=35,
    value=1110
})

growth_curve:upsert({
    id=1203,
    lv=36,
    value=1140
})

growth_curve:upsert({
    id=1203,
    lv=37,
    value=1170
})

growth_curve:upsert({
    id=1203,
    lv=38,
    value=1200
})

growth_curve:upsert({
    id=1203,
    lv=39,
    value=1230
})

growth_curve:upsert({
    id=1203,
    lv=40,
    value=1260
})

growth_curve:upsert({
    id=1203,
    lv=41,
    value=1290
})

growth_curve:upsert({
    id=1203,
    lv=42,
    value=1320
})

growth_curve:upsert({
    id=1203,
    lv=43,
    value=1350
})

growth_curve:upsert({
    id=1203,
    lv=44,
    value=1380
})

growth_curve:upsert({
    id=1203,
    lv=45,
    value=1410
})

growth_curve:upsert({
    id=1203,
    lv=46,
    value=1440
})

growth_curve:upsert({
    id=1203,
    lv=47,
    value=1470
})

growth_curve:upsert({
    id=1203,
    lv=48,
    value=1500
})

growth_curve:upsert({
    id=1203,
    lv=49,
    value=1530
})

growth_curve:upsert({
    id=1203,
    lv=50,
    value=1560
})

growth_curve:upsert({
    id=1203,
    lv=51,
    value=1590
})

growth_curve:upsert({
    id=1203,
    lv=52,
    value=1620
})

growth_curve:upsert({
    id=1203,
    lv=53,
    value=1650
})

growth_curve:upsert({
    id=1203,
    lv=54,
    value=1680
})

growth_curve:upsert({
    id=1203,
    lv=55,
    value=1710
})

growth_curve:upsert({
    id=1203,
    lv=56,
    value=1740
})

growth_curve:upsert({
    id=1203,
    lv=57,
    value=1770
})

growth_curve:upsert({
    id=1203,
    lv=58,
    value=1800
})

growth_curve:upsert({
    id=1203,
    lv=59,
    value=1830
})

growth_curve:upsert({
    id=1203,
    lv=60,
    value=1860
})

growth_curve:upsert({
    id=1203,
    lv=61,
    value=1890
})

growth_curve:upsert({
    id=1203,
    lv=62,
    value=1920
})

growth_curve:upsert({
    id=1203,
    lv=63,
    value=1950
})

growth_curve:upsert({
    id=1203,
    lv=64,
    value=1980
})

growth_curve:upsert({
    id=1203,
    lv=65,
    value=2010
})

growth_curve:upsert({
    id=1203,
    lv=66,
    value=2040
})

growth_curve:upsert({
    id=1203,
    lv=67,
    value=2070
})

growth_curve:upsert({
    id=1203,
    lv=68,
    value=2100
})

growth_curve:upsert({
    id=1203,
    lv=69,
    value=2130
})

growth_curve:upsert({
    id=1203,
    lv=70,
    value=2160
})

growth_curve:upsert({
    id=1203,
    lv=71,
    value=2190
})

growth_curve:upsert({
    id=1203,
    lv=72,
    value=2220
})

growth_curve:upsert({
    id=1203,
    lv=73,
    value=2250
})

growth_curve:upsert({
    id=1203,
    lv=74,
    value=2280
})

growth_curve:upsert({
    id=1203,
    lv=75,
    value=2310
})

growth_curve:upsert({
    id=1203,
    lv=76,
    value=2340
})

growth_curve:upsert({
    id=1203,
    lv=77,
    value=2370
})

growth_curve:upsert({
    id=1203,
    lv=78,
    value=2400
})

growth_curve:upsert({
    id=1203,
    lv=79,
    value=2430
})

growth_curve:upsert({
    id=1203,
    lv=80,
    value=2460
})

growth_curve:upsert({
    id=1203,
    lv=81,
    value=2490
})

growth_curve:upsert({
    id=1203,
    lv=82,
    value=2520
})

growth_curve:upsert({
    id=1203,
    lv=83,
    value=2550
})

growth_curve:upsert({
    id=1203,
    lv=84,
    value=2580
})

growth_curve:upsert({
    id=1203,
    lv=85,
    value=2610
})

growth_curve:upsert({
    id=1203,
    lv=86,
    value=2640
})

growth_curve:upsert({
    id=1203,
    lv=87,
    value=2670
})

growth_curve:upsert({
    id=1203,
    lv=88,
    value=2700
})

growth_curve:upsert({
    id=1203,
    lv=89,
    value=2730
})

growth_curve:upsert({
    id=1203,
    lv=90,
    value=2760
})

growth_curve:upsert({
    id=1203,
    lv=91,
    value=2790
})

growth_curve:upsert({
    id=1203,
    lv=92,
    value=2820
})

growth_curve:upsert({
    id=1203,
    lv=93,
    value=2850
})

growth_curve:upsert({
    id=1203,
    lv=94,
    value=2880
})

growth_curve:upsert({
    id=1203,
    lv=95,
    value=2910
})

growth_curve:upsert({
    id=1203,
    lv=96,
    value=2940
})

growth_curve:upsert({
    id=1203,
    lv=97,
    value=2970
})

growth_curve:upsert({
    id=1203,
    lv=98,
    value=3000
})

growth_curve:upsert({
    id=1203,
    lv=99,
    value=3030
})

growth_curve:upsert({
    id=1203,
    lv=100,
    value=3060
})

growth_curve:upsert({
    id=1204,
    lv=1,
    value=44
})

growth_curve:upsert({
    id=1204,
    lv=2,
    value=58
})

growth_curve:upsert({
    id=1204,
    lv=3,
    value=72
})

growth_curve:upsert({
    id=1204,
    lv=4,
    value=86
})

growth_curve:upsert({
    id=1204,
    lv=5,
    value=100
})

growth_curve:upsert({
    id=1204,
    lv=6,
    value=114
})

growth_curve:upsert({
    id=1204,
    lv=7,
    value=128
})

growth_curve:upsert({
    id=1204,
    lv=8,
    value=142
})

growth_curve:upsert({
    id=1204,
    lv=9,
    value=156
})

growth_curve:upsert({
    id=1204,
    lv=10,
    value=170
})

growth_curve:upsert({
    id=1204,
    lv=11,
    value=184
})

growth_curve:upsert({
    id=1204,
    lv=12,
    value=198
})

growth_curve:upsert({
    id=1204,
    lv=13,
    value=212
})

growth_curve:upsert({
    id=1204,
    lv=14,
    value=226
})

growth_curve:upsert({
    id=1204,
    lv=15,
    value=240
})

growth_curve:upsert({
    id=1204,
    lv=16,
    value=254
})

growth_curve:upsert({
    id=1204,
    lv=17,
    value=268
})

growth_curve:upsert({
    id=1204,
    lv=18,
    value=282
})

growth_curve:upsert({
    id=1204,
    lv=19,
    value=296
})

growth_curve:upsert({
    id=1204,
    lv=20,
    value=310
})

growth_curve:upsert({
    id=1204,
    lv=21,
    value=324
})

growth_curve:upsert({
    id=1204,
    lv=22,
    value=338
})

growth_curve:upsert({
    id=1204,
    lv=23,
    value=352
})

growth_curve:upsert({
    id=1204,
    lv=24,
    value=366
})

growth_curve:upsert({
    id=1204,
    lv=25,
    value=380
})

growth_curve:upsert({
    id=1204,
    lv=26,
    value=394
})

growth_curve:upsert({
    id=1204,
    lv=27,
    value=408
})

growth_curve:upsert({
    id=1204,
    lv=28,
    value=422
})

growth_curve:upsert({
    id=1204,
    lv=29,
    value=436
})

growth_curve:upsert({
    id=1204,
    lv=30,
    value=450
})

growth_curve:upsert({
    id=1204,
    lv=31,
    value=464
})

growth_curve:upsert({
    id=1204,
    lv=32,
    value=478
})

growth_curve:upsert({
    id=1204,
    lv=33,
    value=492
})

growth_curve:upsert({
    id=1204,
    lv=34,
    value=506
})

growth_curve:upsert({
    id=1204,
    lv=35,
    value=520
})

growth_curve:upsert({
    id=1204,
    lv=36,
    value=534
})

growth_curve:upsert({
    id=1204,
    lv=37,
    value=548
})

growth_curve:upsert({
    id=1204,
    lv=38,
    value=562
})

growth_curve:upsert({
    id=1204,
    lv=39,
    value=576
})

growth_curve:upsert({
    id=1204,
    lv=40,
    value=590
})

growth_curve:upsert({
    id=1204,
    lv=41,
    value=604
})

growth_curve:upsert({
    id=1204,
    lv=42,
    value=618
})

growth_curve:upsert({
    id=1204,
    lv=43,
    value=632
})

growth_curve:upsert({
    id=1204,
    lv=44,
    value=646
})

growth_curve:upsert({
    id=1204,
    lv=45,
    value=660
})

growth_curve:upsert({
    id=1204,
    lv=46,
    value=674
})

growth_curve:upsert({
    id=1204,
    lv=47,
    value=688
})

growth_curve:upsert({
    id=1204,
    lv=48,
    value=702
})

growth_curve:upsert({
    id=1204,
    lv=49,
    value=716
})

growth_curve:upsert({
    id=1204,
    lv=50,
    value=730
})

growth_curve:upsert({
    id=1204,
    lv=51,
    value=744
})

growth_curve:upsert({
    id=1204,
    lv=52,
    value=758
})

growth_curve:upsert({
    id=1204,
    lv=53,
    value=772
})

growth_curve:upsert({
    id=1204,
    lv=54,
    value=786
})

growth_curve:upsert({
    id=1204,
    lv=55,
    value=800
})

growth_curve:upsert({
    id=1204,
    lv=56,
    value=814
})

growth_curve:upsert({
    id=1204,
    lv=57,
    value=828
})

growth_curve:upsert({
    id=1204,
    lv=58,
    value=842
})

growth_curve:upsert({
    id=1204,
    lv=59,
    value=856
})

growth_curve:upsert({
    id=1204,
    lv=60,
    value=870
})

growth_curve:upsert({
    id=1204,
    lv=61,
    value=884
})

growth_curve:upsert({
    id=1204,
    lv=62,
    value=898
})

growth_curve:upsert({
    id=1204,
    lv=63,
    value=912
})

growth_curve:upsert({
    id=1204,
    lv=64,
    value=926
})

growth_curve:upsert({
    id=1204,
    lv=65,
    value=940
})

growth_curve:upsert({
    id=1204,
    lv=66,
    value=954
})

growth_curve:upsert({
    id=1204,
    lv=67,
    value=968
})

growth_curve:upsert({
    id=1204,
    lv=68,
    value=982
})

growth_curve:upsert({
    id=1204,
    lv=69,
    value=996
})

growth_curve:upsert({
    id=1204,
    lv=70,
    value=1010
})

growth_curve:upsert({
    id=1204,
    lv=71,
    value=1024
})

growth_curve:upsert({
    id=1204,
    lv=72,
    value=1038
})

growth_curve:upsert({
    id=1204,
    lv=73,
    value=1052
})

growth_curve:upsert({
    id=1204,
    lv=74,
    value=1066
})

growth_curve:upsert({
    id=1204,
    lv=75,
    value=1080
})

growth_curve:upsert({
    id=1204,
    lv=76,
    value=1094
})

growth_curve:upsert({
    id=1204,
    lv=77,
    value=1108
})

growth_curve:upsert({
    id=1204,
    lv=78,
    value=1122
})

growth_curve:upsert({
    id=1204,
    lv=79,
    value=1136
})

growth_curve:upsert({
    id=1204,
    lv=80,
    value=1150
})

growth_curve:upsert({
    id=1204,
    lv=81,
    value=1164
})

growth_curve:upsert({
    id=1204,
    lv=82,
    value=1178
})

growth_curve:upsert({
    id=1204,
    lv=83,
    value=1192
})

growth_curve:upsert({
    id=1204,
    lv=84,
    value=1206
})

growth_curve:upsert({
    id=1204,
    lv=85,
    value=1220
})

growth_curve:upsert({
    id=1204,
    lv=86,
    value=1234
})

growth_curve:upsert({
    id=1204,
    lv=87,
    value=1248
})

growth_curve:upsert({
    id=1204,
    lv=88,
    value=1262
})

growth_curve:upsert({
    id=1204,
    lv=89,
    value=1276
})

growth_curve:upsert({
    id=1204,
    lv=90,
    value=1290
})

growth_curve:upsert({
    id=1204,
    lv=91,
    value=1304
})

growth_curve:upsert({
    id=1204,
    lv=92,
    value=1318
})

growth_curve:upsert({
    id=1204,
    lv=93,
    value=1332
})

growth_curve:upsert({
    id=1204,
    lv=94,
    value=1346
})

growth_curve:upsert({
    id=1204,
    lv=95,
    value=1360
})

growth_curve:upsert({
    id=1204,
    lv=96,
    value=1374
})

growth_curve:upsert({
    id=1204,
    lv=97,
    value=1388
})

growth_curve:upsert({
    id=1204,
    lv=98,
    value=1402
})

growth_curve:upsert({
    id=1204,
    lv=99,
    value=1416
})

growth_curve:upsert({
    id=1204,
    lv=100,
    value=1430
})

growth_curve:upsert({
    id=1205,
    lv=1,
    value=707
})

growth_curve:upsert({
    id=1205,
    lv=2,
    value=714
})

growth_curve:upsert({
    id=1205,
    lv=3,
    value=721
})

growth_curve:upsert({
    id=1205,
    lv=4,
    value=728
})

growth_curve:upsert({
    id=1205,
    lv=5,
    value=735
})

growth_curve:upsert({
    id=1205,
    lv=6,
    value=742
})

growth_curve:upsert({
    id=1205,
    lv=7,
    value=749
})

growth_curve:upsert({
    id=1205,
    lv=8,
    value=756
})

growth_curve:upsert({
    id=1205,
    lv=9,
    value=763
})

growth_curve:upsert({
    id=1205,
    lv=10,
    value=770
})

growth_curve:upsert({
    id=1205,
    lv=11,
    value=777
})

growth_curve:upsert({
    id=1205,
    lv=12,
    value=784
})

growth_curve:upsert({
    id=1205,
    lv=13,
    value=791
})

growth_curve:upsert({
    id=1205,
    lv=14,
    value=798
})

growth_curve:upsert({
    id=1205,
    lv=15,
    value=805
})

growth_curve:upsert({
    id=1205,
    lv=16,
    value=812
})

growth_curve:upsert({
    id=1205,
    lv=17,
    value=819
})

growth_curve:upsert({
    id=1205,
    lv=18,
    value=826
})

growth_curve:upsert({
    id=1205,
    lv=19,
    value=833
})

growth_curve:upsert({
    id=1205,
    lv=20,
    value=840
})

growth_curve:upsert({
    id=1205,
    lv=21,
    value=847
})

growth_curve:upsert({
    id=1205,
    lv=22,
    value=854
})

growth_curve:upsert({
    id=1205,
    lv=23,
    value=861
})

growth_curve:upsert({
    id=1205,
    lv=24,
    value=868
})

growth_curve:upsert({
    id=1205,
    lv=25,
    value=875
})

growth_curve:upsert({
    id=1205,
    lv=26,
    value=882
})

growth_curve:upsert({
    id=1205,
    lv=27,
    value=889
})

growth_curve:upsert({
    id=1205,
    lv=28,
    value=896
})

growth_curve:upsert({
    id=1205,
    lv=29,
    value=903
})

growth_curve:upsert({
    id=1205,
    lv=30,
    value=910
})

growth_curve:upsert({
    id=1205,
    lv=31,
    value=917
})

growth_curve:upsert({
    id=1205,
    lv=32,
    value=924
})

growth_curve:upsert({
    id=1205,
    lv=33,
    value=931
})

growth_curve:upsert({
    id=1205,
    lv=34,
    value=938
})

growth_curve:upsert({
    id=1205,
    lv=35,
    value=945
})

growth_curve:upsert({
    id=1205,
    lv=36,
    value=952
})

growth_curve:upsert({
    id=1205,
    lv=37,
    value=959
})

growth_curve:upsert({
    id=1205,
    lv=38,
    value=966
})

growth_curve:upsert({
    id=1205,
    lv=39,
    value=973
})

growth_curve:upsert({
    id=1205,
    lv=40,
    value=980
})

growth_curve:upsert({
    id=1205,
    lv=41,
    value=987
})

growth_curve:upsert({
    id=1205,
    lv=42,
    value=994
})

growth_curve:upsert({
    id=1205,
    lv=43,
    value=1001
})

growth_curve:upsert({
    id=1205,
    lv=44,
    value=1008
})

growth_curve:upsert({
    id=1205,
    lv=45,
    value=1015
})

growth_curve:upsert({
    id=1205,
    lv=46,
    value=1022
})

growth_curve:upsert({
    id=1205,
    lv=47,
    value=1029
})

growth_curve:upsert({
    id=1205,
    lv=48,
    value=1036
})

growth_curve:upsert({
    id=1205,
    lv=49,
    value=1043
})

growth_curve:upsert({
    id=1205,
    lv=50,
    value=1050
})

growth_curve:upsert({
    id=1205,
    lv=51,
    value=1057
})

growth_curve:upsert({
    id=1205,
    lv=52,
    value=1064
})

growth_curve:upsert({
    id=1205,
    lv=53,
    value=1071
})

growth_curve:upsert({
    id=1205,
    lv=54,
    value=1078
})

growth_curve:upsert({
    id=1205,
    lv=55,
    value=1085
})

growth_curve:upsert({
    id=1205,
    lv=56,
    value=1092
})

growth_curve:upsert({
    id=1205,
    lv=57,
    value=1099
})

growth_curve:upsert({
    id=1205,
    lv=58,
    value=1106
})

growth_curve:upsert({
    id=1205,
    lv=59,
    value=1113
})

growth_curve:upsert({
    id=1205,
    lv=60,
    value=1120
})

growth_curve:upsert({
    id=1205,
    lv=61,
    value=1127
})

growth_curve:upsert({
    id=1205,
    lv=62,
    value=1134
})

growth_curve:upsert({
    id=1205,
    lv=63,
    value=1141
})

growth_curve:upsert({
    id=1205,
    lv=64,
    value=1148
})

growth_curve:upsert({
    id=1205,
    lv=65,
    value=1155
})

growth_curve:upsert({
    id=1205,
    lv=66,
    value=1162
})

growth_curve:upsert({
    id=1205,
    lv=67,
    value=1169
})

growth_curve:upsert({
    id=1205,
    lv=68,
    value=1176
})

growth_curve:upsert({
    id=1205,
    lv=69,
    value=1183
})

growth_curve:upsert({
    id=1205,
    lv=70,
    value=1190
})

growth_curve:upsert({
    id=1205,
    lv=71,
    value=1197
})

growth_curve:upsert({
    id=1205,
    lv=72,
    value=1204
})

growth_curve:upsert({
    id=1205,
    lv=73,
    value=1211
})

growth_curve:upsert({
    id=1205,
    lv=74,
    value=1218
})

growth_curve:upsert({
    id=1205,
    lv=75,
    value=1225
})

growth_curve:upsert({
    id=1205,
    lv=76,
    value=1232
})

growth_curve:upsert({
    id=1205,
    lv=77,
    value=1239
})

growth_curve:upsert({
    id=1205,
    lv=78,
    value=1246
})

growth_curve:upsert({
    id=1205,
    lv=79,
    value=1253
})

growth_curve:upsert({
    id=1205,
    lv=80,
    value=1260
})

growth_curve:upsert({
    id=1205,
    lv=81,
    value=1267
})

growth_curve:upsert({
    id=1205,
    lv=82,
    value=1274
})

growth_curve:upsert({
    id=1205,
    lv=83,
    value=1281
})

growth_curve:upsert({
    id=1205,
    lv=84,
    value=1288
})

growth_curve:upsert({
    id=1205,
    lv=85,
    value=1295
})

growth_curve:upsert({
    id=1205,
    lv=86,
    value=1302
})

growth_curve:upsert({
    id=1205,
    lv=87,
    value=1309
})

growth_curve:upsert({
    id=1205,
    lv=88,
    value=1316
})

growth_curve:upsert({
    id=1205,
    lv=89,
    value=1323
})

growth_curve:upsert({
    id=1205,
    lv=90,
    value=1330
})

growth_curve:upsert({
    id=1205,
    lv=91,
    value=1337
})

growth_curve:upsert({
    id=1205,
    lv=92,
    value=1344
})

growth_curve:upsert({
    id=1205,
    lv=93,
    value=1351
})

growth_curve:upsert({
    id=1205,
    lv=94,
    value=1358
})

growth_curve:upsert({
    id=1205,
    lv=95,
    value=1365
})

growth_curve:upsert({
    id=1205,
    lv=96,
    value=1372
})

growth_curve:upsert({
    id=1205,
    lv=97,
    value=1379
})

growth_curve:upsert({
    id=1205,
    lv=98,
    value=1386
})

growth_curve:upsert({
    id=1205,
    lv=99,
    value=1393
})

growth_curve:upsert({
    id=1205,
    lv=100,
    value=1400
})

growth_curve:upsert({
    id=1206,
    lv=1,
    value=15000
})

growth_curve:upsert({
    id=1206,
    lv=2,
    value=15050
})

growth_curve:upsert({
    id=1206,
    lv=3,
    value=15100
})

growth_curve:upsert({
    id=1206,
    lv=4,
    value=15150
})

growth_curve:upsert({
    id=1206,
    lv=5,
    value=15200
})

growth_curve:upsert({
    id=1206,
    lv=6,
    value=15250
})

growth_curve:upsert({
    id=1206,
    lv=7,
    value=15300
})

growth_curve:upsert({
    id=1206,
    lv=8,
    value=15350
})

growth_curve:upsert({
    id=1206,
    lv=9,
    value=15400
})

growth_curve:upsert({
    id=1206,
    lv=10,
    value=15450
})

growth_curve:upsert({
    id=1206,
    lv=11,
    value=15500
})

growth_curve:upsert({
    id=1206,
    lv=12,
    value=15550
})

growth_curve:upsert({
    id=1206,
    lv=13,
    value=15600
})

growth_curve:upsert({
    id=1206,
    lv=14,
    value=15650
})

growth_curve:upsert({
    id=1206,
    lv=15,
    value=15700
})

growth_curve:upsert({
    id=1206,
    lv=16,
    value=15750
})

growth_curve:upsert({
    id=1206,
    lv=17,
    value=15800
})

growth_curve:upsert({
    id=1206,
    lv=18,
    value=15850
})

growth_curve:upsert({
    id=1206,
    lv=19,
    value=15900
})

growth_curve:upsert({
    id=1206,
    lv=20,
    value=15950
})

growth_curve:upsert({
    id=1206,
    lv=21,
    value=16000
})

growth_curve:upsert({
    id=1206,
    lv=22,
    value=16050
})

growth_curve:upsert({
    id=1206,
    lv=23,
    value=16100
})

growth_curve:upsert({
    id=1206,
    lv=24,
    value=16150
})

growth_curve:upsert({
    id=1206,
    lv=25,
    value=16200
})

growth_curve:upsert({
    id=1206,
    lv=26,
    value=16250
})

growth_curve:upsert({
    id=1206,
    lv=27,
    value=16300
})

growth_curve:upsert({
    id=1206,
    lv=28,
    value=16350
})

growth_curve:upsert({
    id=1206,
    lv=29,
    value=16400
})

growth_curve:upsert({
    id=1206,
    lv=30,
    value=16450
})

growth_curve:upsert({
    id=1206,
    lv=31,
    value=16500
})

growth_curve:upsert({
    id=1206,
    lv=32,
    value=16550
})

growth_curve:upsert({
    id=1206,
    lv=33,
    value=16600
})

growth_curve:upsert({
    id=1206,
    lv=34,
    value=16650
})

growth_curve:upsert({
    id=1206,
    lv=35,
    value=16700
})

growth_curve:upsert({
    id=1206,
    lv=36,
    value=16750
})

growth_curve:upsert({
    id=1206,
    lv=37,
    value=16800
})

growth_curve:upsert({
    id=1206,
    lv=38,
    value=16850
})

growth_curve:upsert({
    id=1206,
    lv=39,
    value=16900
})

growth_curve:upsert({
    id=1206,
    lv=40,
    value=16950
})

growth_curve:upsert({
    id=1206,
    lv=41,
    value=17000
})

growth_curve:upsert({
    id=1206,
    lv=42,
    value=17050
})

growth_curve:upsert({
    id=1206,
    lv=43,
    value=17100
})

growth_curve:upsert({
    id=1206,
    lv=44,
    value=17150
})

growth_curve:upsert({
    id=1206,
    lv=45,
    value=17200
})

growth_curve:upsert({
    id=1206,
    lv=46,
    value=17250
})

growth_curve:upsert({
    id=1206,
    lv=47,
    value=17300
})

growth_curve:upsert({
    id=1206,
    lv=48,
    value=17350
})

growth_curve:upsert({
    id=1206,
    lv=49,
    value=17400
})

growth_curve:upsert({
    id=1206,
    lv=50,
    value=17450
})

growth_curve:upsert({
    id=1206,
    lv=51,
    value=17500
})

growth_curve:upsert({
    id=1206,
    lv=52,
    value=17550
})

growth_curve:upsert({
    id=1206,
    lv=53,
    value=17600
})

growth_curve:upsert({
    id=1206,
    lv=54,
    value=17650
})

growth_curve:upsert({
    id=1206,
    lv=55,
    value=17700
})

growth_curve:upsert({
    id=1206,
    lv=56,
    value=17750
})

growth_curve:upsert({
    id=1206,
    lv=57,
    value=17800
})

growth_curve:upsert({
    id=1206,
    lv=58,
    value=17850
})

growth_curve:upsert({
    id=1206,
    lv=59,
    value=17900
})

growth_curve:upsert({
    id=1206,
    lv=60,
    value=17950
})

growth_curve:upsert({
    id=1206,
    lv=61,
    value=18000
})

growth_curve:upsert({
    id=1206,
    lv=62,
    value=18050
})

growth_curve:upsert({
    id=1206,
    lv=63,
    value=18100
})

growth_curve:upsert({
    id=1206,
    lv=64,
    value=18150
})

growth_curve:upsert({
    id=1206,
    lv=65,
    value=18200
})

growth_curve:upsert({
    id=1206,
    lv=66,
    value=18250
})

growth_curve:upsert({
    id=1206,
    lv=67,
    value=18300
})

growth_curve:upsert({
    id=1206,
    lv=68,
    value=18350
})

growth_curve:upsert({
    id=1206,
    lv=69,
    value=18400
})

growth_curve:upsert({
    id=1206,
    lv=70,
    value=18450
})

growth_curve:upsert({
    id=1206,
    lv=71,
    value=18500
})

growth_curve:upsert({
    id=1206,
    lv=72,
    value=18550
})

growth_curve:upsert({
    id=1206,
    lv=73,
    value=18600
})

growth_curve:upsert({
    id=1206,
    lv=74,
    value=18650
})

growth_curve:upsert({
    id=1206,
    lv=75,
    value=18700
})

growth_curve:upsert({
    id=1206,
    lv=76,
    value=18750
})

growth_curve:upsert({
    id=1206,
    lv=77,
    value=18800
})

growth_curve:upsert({
    id=1206,
    lv=78,
    value=18850
})

growth_curve:upsert({
    id=1206,
    lv=79,
    value=18900
})

growth_curve:upsert({
    id=1206,
    lv=80,
    value=18950
})

growth_curve:upsert({
    id=1206,
    lv=81,
    value=19000
})

growth_curve:upsert({
    id=1206,
    lv=82,
    value=19050
})

growth_curve:upsert({
    id=1206,
    lv=83,
    value=19100
})

growth_curve:upsert({
    id=1206,
    lv=84,
    value=19150
})

growth_curve:upsert({
    id=1206,
    lv=85,
    value=19200
})

growth_curve:upsert({
    id=1206,
    lv=86,
    value=19250
})

growth_curve:upsert({
    id=1206,
    lv=87,
    value=19300
})

growth_curve:upsert({
    id=1206,
    lv=88,
    value=19350
})

growth_curve:upsert({
    id=1206,
    lv=89,
    value=19400
})

growth_curve:upsert({
    id=1206,
    lv=90,
    value=19450
})

growth_curve:upsert({
    id=1206,
    lv=91,
    value=19500
})

growth_curve:upsert({
    id=1206,
    lv=92,
    value=19550
})

growth_curve:upsert({
    id=1206,
    lv=93,
    value=19600
})

growth_curve:upsert({
    id=1206,
    lv=94,
    value=19650
})

growth_curve:upsert({
    id=1206,
    lv=95,
    value=19700
})

growth_curve:upsert({
    id=1206,
    lv=96,
    value=19750
})

growth_curve:upsert({
    id=1206,
    lv=97,
    value=19800
})

growth_curve:upsert({
    id=1206,
    lv=98,
    value=19850
})

growth_curve:upsert({
    id=1206,
    lv=99,
    value=19900
})

growth_curve:upsert({
    id=1206,
    lv=100,
    value=19950
})

growth_curve:upsert({
    id=2002,
    lv=1,
    value=340
})

growth_curve:upsert({
    id=2002,
    lv=2,
    value=380
})

growth_curve:upsert({
    id=2002,
    lv=3,
    value=420
})

growth_curve:upsert({
    id=2002,
    lv=4,
    value=460
})

growth_curve:upsert({
    id=2002,
    lv=5,
    value=500
})

growth_curve:upsert({
    id=2002,
    lv=6,
    value=540
})

growth_curve:upsert({
    id=2002,
    lv=7,
    value=580
})

growth_curve:upsert({
    id=2002,
    lv=8,
    value=620
})

growth_curve:upsert({
    id=2002,
    lv=9,
    value=660
})

growth_curve:upsert({
    id=2002,
    lv=10,
    value=700
})

growth_curve:upsert({
    id=2002,
    lv=11,
    value=740
})

growth_curve:upsert({
    id=2002,
    lv=12,
    value=780
})

growth_curve:upsert({
    id=2002,
    lv=13,
    value=820
})

growth_curve:upsert({
    id=2002,
    lv=14,
    value=860
})

growth_curve:upsert({
    id=2002,
    lv=15,
    value=900
})

growth_curve:upsert({
    id=2002,
    lv=16,
    value=940
})

growth_curve:upsert({
    id=2002,
    lv=17,
    value=980
})

growth_curve:upsert({
    id=2002,
    lv=18,
    value=1020
})

growth_curve:upsert({
    id=2002,
    lv=19,
    value=1060
})

growth_curve:upsert({
    id=2002,
    lv=20,
    value=1100
})

growth_curve:upsert({
    id=2002,
    lv=21,
    value=1140
})

growth_curve:upsert({
    id=2002,
    lv=22,
    value=1180
})

growth_curve:upsert({
    id=2002,
    lv=23,
    value=1220
})

growth_curve:upsert({
    id=2002,
    lv=24,
    value=1260
})

growth_curve:upsert({
    id=2002,
    lv=25,
    value=1300
})

growth_curve:upsert({
    id=2002,
    lv=26,
    value=1340
})

growth_curve:upsert({
    id=2002,
    lv=27,
    value=1380
})

growth_curve:upsert({
    id=2002,
    lv=28,
    value=1420
})

growth_curve:upsert({
    id=2002,
    lv=29,
    value=1460
})

growth_curve:upsert({
    id=2002,
    lv=30,
    value=1500
})

growth_curve:upsert({
    id=2002,
    lv=31,
    value=1540
})

growth_curve:upsert({
    id=2002,
    lv=32,
    value=1580
})

growth_curve:upsert({
    id=2002,
    lv=33,
    value=1620
})

growth_curve:upsert({
    id=2002,
    lv=34,
    value=1660
})

growth_curve:upsert({
    id=2002,
    lv=35,
    value=1700
})

growth_curve:upsert({
    id=2002,
    lv=36,
    value=1740
})

growth_curve:upsert({
    id=2002,
    lv=37,
    value=1780
})

growth_curve:upsert({
    id=2002,
    lv=38,
    value=1820
})

growth_curve:upsert({
    id=2002,
    lv=39,
    value=1860
})

growth_curve:upsert({
    id=2002,
    lv=40,
    value=1900
})

growth_curve:upsert({
    id=2002,
    lv=41,
    value=1940
})

growth_curve:upsert({
    id=2002,
    lv=42,
    value=1980
})

growth_curve:upsert({
    id=2002,
    lv=43,
    value=2020
})

growth_curve:upsert({
    id=2002,
    lv=44,
    value=2060
})

growth_curve:upsert({
    id=2002,
    lv=45,
    value=2100
})

growth_curve:upsert({
    id=2002,
    lv=46,
    value=2140
})

growth_curve:upsert({
    id=2002,
    lv=47,
    value=2180
})

growth_curve:upsert({
    id=2002,
    lv=48,
    value=2220
})

growth_curve:upsert({
    id=2002,
    lv=49,
    value=2260
})

growth_curve:upsert({
    id=2002,
    lv=50,
    value=2300
})

growth_curve:upsert({
    id=2002,
    lv=51,
    value=2340
})

growth_curve:upsert({
    id=2002,
    lv=52,
    value=2380
})

growth_curve:upsert({
    id=2002,
    lv=53,
    value=2420
})

growth_curve:upsert({
    id=2002,
    lv=54,
    value=2460
})

growth_curve:upsert({
    id=2002,
    lv=55,
    value=2500
})

growth_curve:upsert({
    id=2002,
    lv=56,
    value=2540
})

growth_curve:upsert({
    id=2002,
    lv=57,
    value=2580
})

growth_curve:upsert({
    id=2002,
    lv=58,
    value=2620
})

growth_curve:upsert({
    id=2002,
    lv=59,
    value=2660
})

growth_curve:upsert({
    id=2002,
    lv=60,
    value=2700
})

growth_curve:upsert({
    id=2002,
    lv=61,
    value=2740
})

growth_curve:upsert({
    id=2002,
    lv=62,
    value=2780
})

growth_curve:upsert({
    id=2002,
    lv=63,
    value=2820
})

growth_curve:upsert({
    id=2002,
    lv=64,
    value=2860
})

growth_curve:upsert({
    id=2002,
    lv=65,
    value=2900
})

growth_curve:upsert({
    id=2002,
    lv=66,
    value=2940
})

growth_curve:upsert({
    id=2002,
    lv=67,
    value=2980
})

growth_curve:upsert({
    id=2002,
    lv=68,
    value=3020
})

growth_curve:upsert({
    id=2002,
    lv=69,
    value=3060
})

growth_curve:upsert({
    id=2002,
    lv=70,
    value=3100
})

growth_curve:upsert({
    id=2002,
    lv=71,
    value=3140
})

growth_curve:upsert({
    id=2002,
    lv=72,
    value=3180
})

growth_curve:upsert({
    id=2002,
    lv=73,
    value=3220
})

growth_curve:upsert({
    id=2002,
    lv=74,
    value=3260
})

growth_curve:upsert({
    id=2002,
    lv=75,
    value=3300
})

growth_curve:upsert({
    id=2002,
    lv=76,
    value=3340
})

growth_curve:upsert({
    id=2002,
    lv=77,
    value=3380
})

growth_curve:upsert({
    id=2002,
    lv=78,
    value=3420
})

growth_curve:upsert({
    id=2002,
    lv=79,
    value=3460
})

growth_curve:upsert({
    id=2002,
    lv=80,
    value=3500
})

growth_curve:upsert({
    id=2002,
    lv=81,
    value=3540
})

growth_curve:upsert({
    id=2002,
    lv=82,
    value=3580
})

growth_curve:upsert({
    id=2002,
    lv=83,
    value=3620
})

growth_curve:upsert({
    id=2002,
    lv=84,
    value=3660
})

growth_curve:upsert({
    id=2002,
    lv=85,
    value=3700
})

growth_curve:upsert({
    id=2002,
    lv=86,
    value=3740
})

growth_curve:upsert({
    id=2002,
    lv=87,
    value=3780
})

growth_curve:upsert({
    id=2002,
    lv=88,
    value=3820
})

growth_curve:upsert({
    id=2002,
    lv=89,
    value=3860
})

growth_curve:upsert({
    id=2002,
    lv=90,
    value=3900
})

growth_curve:upsert({
    id=2002,
    lv=91,
    value=3940
})

growth_curve:upsert({
    id=2002,
    lv=92,
    value=3980
})

growth_curve:upsert({
    id=2002,
    lv=93,
    value=4020
})

growth_curve:upsert({
    id=2002,
    lv=94,
    value=4060
})

growth_curve:upsert({
    id=2002,
    lv=95,
    value=4100
})

growth_curve:upsert({
    id=2002,
    lv=96,
    value=4140
})

growth_curve:upsert({
    id=2002,
    lv=97,
    value=4180
})

growth_curve:upsert({
    id=2002,
    lv=98,
    value=4220
})

growth_curve:upsert({
    id=2002,
    lv=99,
    value=4260
})

growth_curve:upsert({
    id=2002,
    lv=100,
    value=4300
})

growth_curve:upsert({
    id=2003,
    lv=1,
    value=48
})

growth_curve:upsert({
    id=2003,
    lv=2,
    value=56
})

growth_curve:upsert({
    id=2003,
    lv=3,
    value=64
})

growth_curve:upsert({
    id=2003,
    lv=4,
    value=72
})

growth_curve:upsert({
    id=2003,
    lv=5,
    value=80
})

growth_curve:upsert({
    id=2003,
    lv=6,
    value=88
})

growth_curve:upsert({
    id=2003,
    lv=7,
    value=96
})

growth_curve:upsert({
    id=2003,
    lv=8,
    value=104
})

growth_curve:upsert({
    id=2003,
    lv=9,
    value=112
})

growth_curve:upsert({
    id=2003,
    lv=10,
    value=120
})

growth_curve:upsert({
    id=2003,
    lv=11,
    value=128
})

growth_curve:upsert({
    id=2003,
    lv=12,
    value=136
})

growth_curve:upsert({
    id=2003,
    lv=13,
    value=144
})

growth_curve:upsert({
    id=2003,
    lv=14,
    value=152
})

growth_curve:upsert({
    id=2003,
    lv=15,
    value=160
})

growth_curve:upsert({
    id=2003,
    lv=16,
    value=168
})

growth_curve:upsert({
    id=2003,
    lv=17,
    value=176
})

growth_curve:upsert({
    id=2003,
    lv=18,
    value=184
})

growth_curve:upsert({
    id=2003,
    lv=19,
    value=192
})

growth_curve:upsert({
    id=2003,
    lv=20,
    value=200
})

growth_curve:upsert({
    id=2003,
    lv=21,
    value=208
})

growth_curve:upsert({
    id=2003,
    lv=22,
    value=216
})

growth_curve:upsert({
    id=2003,
    lv=23,
    value=224
})

growth_curve:upsert({
    id=2003,
    lv=24,
    value=232
})

growth_curve:upsert({
    id=2003,
    lv=25,
    value=240
})

growth_curve:upsert({
    id=2003,
    lv=26,
    value=248
})

growth_curve:upsert({
    id=2003,
    lv=27,
    value=256
})

growth_curve:upsert({
    id=2003,
    lv=28,
    value=264
})

growth_curve:upsert({
    id=2003,
    lv=29,
    value=272
})

growth_curve:upsert({
    id=2003,
    lv=30,
    value=280
})

growth_curve:upsert({
    id=2003,
    lv=31,
    value=288
})

growth_curve:upsert({
    id=2003,
    lv=32,
    value=296
})

growth_curve:upsert({
    id=2003,
    lv=33,
    value=304
})

growth_curve:upsert({
    id=2003,
    lv=34,
    value=312
})

growth_curve:upsert({
    id=2003,
    lv=35,
    value=320
})

growth_curve:upsert({
    id=2003,
    lv=36,
    value=328
})

growth_curve:upsert({
    id=2003,
    lv=37,
    value=336
})

growth_curve:upsert({
    id=2003,
    lv=38,
    value=344
})

growth_curve:upsert({
    id=2003,
    lv=39,
    value=352
})

growth_curve:upsert({
    id=2003,
    lv=40,
    value=360
})

growth_curve:upsert({
    id=2003,
    lv=41,
    value=368
})

growth_curve:upsert({
    id=2003,
    lv=42,
    value=376
})

growth_curve:upsert({
    id=2003,
    lv=43,
    value=384
})

growth_curve:upsert({
    id=2003,
    lv=44,
    value=392
})

growth_curve:upsert({
    id=2003,
    lv=45,
    value=400
})

growth_curve:upsert({
    id=2003,
    lv=46,
    value=408
})

growth_curve:upsert({
    id=2003,
    lv=47,
    value=416
})

growth_curve:upsert({
    id=2003,
    lv=48,
    value=424
})

growth_curve:upsert({
    id=2003,
    lv=49,
    value=432
})

growth_curve:upsert({
    id=2003,
    lv=50,
    value=440
})

growth_curve:upsert({
    id=2003,
    lv=51,
    value=448
})

growth_curve:upsert({
    id=2003,
    lv=52,
    value=456
})

growth_curve:upsert({
    id=2003,
    lv=53,
    value=464
})

growth_curve:upsert({
    id=2003,
    lv=54,
    value=472
})

growth_curve:upsert({
    id=2003,
    lv=55,
    value=480
})

growth_curve:upsert({
    id=2003,
    lv=56,
    value=488
})

growth_curve:upsert({
    id=2003,
    lv=57,
    value=496
})

growth_curve:upsert({
    id=2003,
    lv=58,
    value=504
})

growth_curve:upsert({
    id=2003,
    lv=59,
    value=512
})

growth_curve:upsert({
    id=2003,
    lv=60,
    value=520
})

growth_curve:upsert({
    id=2003,
    lv=61,
    value=528
})

growth_curve:upsert({
    id=2003,
    lv=62,
    value=536
})

growth_curve:upsert({
    id=2003,
    lv=63,
    value=544
})

growth_curve:upsert({
    id=2003,
    lv=64,
    value=552
})

growth_curve:upsert({
    id=2003,
    lv=65,
    value=560
})

growth_curve:upsert({
    id=2003,
    lv=66,
    value=568
})

growth_curve:upsert({
    id=2003,
    lv=67,
    value=576
})

growth_curve:upsert({
    id=2003,
    lv=68,
    value=584
})

growth_curve:upsert({
    id=2003,
    lv=69,
    value=592
})

growth_curve:upsert({
    id=2003,
    lv=70,
    value=600
})

growth_curve:upsert({
    id=2003,
    lv=71,
    value=608
})

growth_curve:upsert({
    id=2003,
    lv=72,
    value=616
})

growth_curve:upsert({
    id=2003,
    lv=73,
    value=624
})

growth_curve:upsert({
    id=2003,
    lv=74,
    value=632
})

growth_curve:upsert({
    id=2003,
    lv=75,
    value=640
})

growth_curve:upsert({
    id=2003,
    lv=76,
    value=648
})

growth_curve:upsert({
    id=2003,
    lv=77,
    value=656
})

growth_curve:upsert({
    id=2003,
    lv=78,
    value=664
})

growth_curve:upsert({
    id=2003,
    lv=79,
    value=672
})

growth_curve:upsert({
    id=2003,
    lv=80,
    value=680
})

growth_curve:upsert({
    id=2003,
    lv=81,
    value=688
})

growth_curve:upsert({
    id=2003,
    lv=82,
    value=696
})

growth_curve:upsert({
    id=2003,
    lv=83,
    value=704
})

growth_curve:upsert({
    id=2003,
    lv=84,
    value=712
})

growth_curve:upsert({
    id=2003,
    lv=85,
    value=720
})

growth_curve:upsert({
    id=2003,
    lv=86,
    value=728
})

growth_curve:upsert({
    id=2003,
    lv=87,
    value=736
})

growth_curve:upsert({
    id=2003,
    lv=88,
    value=744
})

growth_curve:upsert({
    id=2003,
    lv=89,
    value=752
})

growth_curve:upsert({
    id=2003,
    lv=90,
    value=760
})

growth_curve:upsert({
    id=2003,
    lv=91,
    value=768
})

growth_curve:upsert({
    id=2003,
    lv=92,
    value=776
})

growth_curve:upsert({
    id=2003,
    lv=93,
    value=784
})

growth_curve:upsert({
    id=2003,
    lv=94,
    value=792
})

growth_curve:upsert({
    id=2003,
    lv=95,
    value=800
})

growth_curve:upsert({
    id=2003,
    lv=96,
    value=808
})

growth_curve:upsert({
    id=2003,
    lv=97,
    value=816
})

growth_curve:upsert({
    id=2003,
    lv=98,
    value=824
})

growth_curve:upsert({
    id=2003,
    lv=99,
    value=832
})

growth_curve:upsert({
    id=2003,
    lv=100,
    value=840
})

growth_curve:upsert({
    id=2004,
    lv=1,
    value=45
})

growth_curve:upsert({
    id=2004,
    lv=2,
    value=50
})

growth_curve:upsert({
    id=2004,
    lv=3,
    value=55
})

growth_curve:upsert({
    id=2004,
    lv=4,
    value=60
})

growth_curve:upsert({
    id=2004,
    lv=5,
    value=65
})

growth_curve:upsert({
    id=2004,
    lv=6,
    value=70
})

growth_curve:upsert({
    id=2004,
    lv=7,
    value=75
})

growth_curve:upsert({
    id=2004,
    lv=8,
    value=80
})

growth_curve:upsert({
    id=2004,
    lv=9,
    value=85
})

growth_curve:upsert({
    id=2004,
    lv=10,
    value=90
})

growth_curve:upsert({
    id=2004,
    lv=11,
    value=95
})

growth_curve:upsert({
    id=2004,
    lv=12,
    value=100
})

growth_curve:upsert({
    id=2004,
    lv=13,
    value=105
})

growth_curve:upsert({
    id=2004,
    lv=14,
    value=110
})

growth_curve:upsert({
    id=2004,
    lv=15,
    value=115
})

growth_curve:upsert({
    id=2004,
    lv=16,
    value=120
})

growth_curve:upsert({
    id=2004,
    lv=17,
    value=125
})

growth_curve:upsert({
    id=2004,
    lv=18,
    value=130
})

growth_curve:upsert({
    id=2004,
    lv=19,
    value=135
})

growth_curve:upsert({
    id=2004,
    lv=20,
    value=140
})

growth_curve:upsert({
    id=2004,
    lv=21,
    value=145
})

growth_curve:upsert({
    id=2004,
    lv=22,
    value=150
})

growth_curve:upsert({
    id=2004,
    lv=23,
    value=155
})

growth_curve:upsert({
    id=2004,
    lv=24,
    value=160
})

growth_curve:upsert({
    id=2004,
    lv=25,
    value=165
})

growth_curve:upsert({
    id=2004,
    lv=26,
    value=170
})

growth_curve:upsert({
    id=2004,
    lv=27,
    value=175
})

growth_curve:upsert({
    id=2004,
    lv=28,
    value=180
})

growth_curve:upsert({
    id=2004,
    lv=29,
    value=185
})

growth_curve:upsert({
    id=2004,
    lv=30,
    value=190
})

growth_curve:upsert({
    id=2004,
    lv=31,
    value=195
})

growth_curve:upsert({
    id=2004,
    lv=32,
    value=200
})

growth_curve:upsert({
    id=2004,
    lv=33,
    value=205
})

growth_curve:upsert({
    id=2004,
    lv=34,
    value=210
})

growth_curve:upsert({
    id=2004,
    lv=35,
    value=215
})

growth_curve:upsert({
    id=2004,
    lv=36,
    value=220
})

growth_curve:upsert({
    id=2004,
    lv=37,
    value=225
})

growth_curve:upsert({
    id=2004,
    lv=38,
    value=230
})

growth_curve:upsert({
    id=2004,
    lv=39,
    value=235
})

growth_curve:upsert({
    id=2004,
    lv=40,
    value=240
})

growth_curve:upsert({
    id=2004,
    lv=41,
    value=245
})

growth_curve:upsert({
    id=2004,
    lv=42,
    value=250
})

growth_curve:upsert({
    id=2004,
    lv=43,
    value=255
})

growth_curve:upsert({
    id=2004,
    lv=44,
    value=260
})

growth_curve:upsert({
    id=2004,
    lv=45,
    value=265
})

growth_curve:upsert({
    id=2004,
    lv=46,
    value=270
})

growth_curve:upsert({
    id=2004,
    lv=47,
    value=275
})

growth_curve:upsert({
    id=2004,
    lv=48,
    value=280
})

growth_curve:upsert({
    id=2004,
    lv=49,
    value=285
})

growth_curve:upsert({
    id=2004,
    lv=50,
    value=290
})

growth_curve:upsert({
    id=2004,
    lv=51,
    value=295
})

growth_curve:upsert({
    id=2004,
    lv=52,
    value=300
})

growth_curve:upsert({
    id=2004,
    lv=53,
    value=305
})

growth_curve:upsert({
    id=2004,
    lv=54,
    value=310
})

growth_curve:upsert({
    id=2004,
    lv=55,
    value=315
})

growth_curve:upsert({
    id=2004,
    lv=56,
    value=320
})

growth_curve:upsert({
    id=2004,
    lv=57,
    value=325
})

growth_curve:upsert({
    id=2004,
    lv=58,
    value=330
})

growth_curve:upsert({
    id=2004,
    lv=59,
    value=335
})

growth_curve:upsert({
    id=2004,
    lv=60,
    value=340
})

growth_curve:upsert({
    id=2004,
    lv=61,
    value=345
})

growth_curve:upsert({
    id=2004,
    lv=62,
    value=350
})

growth_curve:upsert({
    id=2004,
    lv=63,
    value=355
})

growth_curve:upsert({
    id=2004,
    lv=64,
    value=360
})

growth_curve:upsert({
    id=2004,
    lv=65,
    value=365
})

growth_curve:upsert({
    id=2004,
    lv=66,
    value=370
})

growth_curve:upsert({
    id=2004,
    lv=67,
    value=375
})

growth_curve:upsert({
    id=2004,
    lv=68,
    value=380
})

growth_curve:upsert({
    id=2004,
    lv=69,
    value=385
})

growth_curve:upsert({
    id=2004,
    lv=70,
    value=390
})

growth_curve:upsert({
    id=2004,
    lv=71,
    value=395
})

growth_curve:upsert({
    id=2004,
    lv=72,
    value=400
})

growth_curve:upsert({
    id=2004,
    lv=73,
    value=405
})

growth_curve:upsert({
    id=2004,
    lv=74,
    value=410
})

growth_curve:upsert({
    id=2004,
    lv=75,
    value=415
})

growth_curve:upsert({
    id=2004,
    lv=76,
    value=420
})

growth_curve:upsert({
    id=2004,
    lv=77,
    value=425
})

growth_curve:upsert({
    id=2004,
    lv=78,
    value=430
})

growth_curve:upsert({
    id=2004,
    lv=79,
    value=435
})

growth_curve:upsert({
    id=2004,
    lv=80,
    value=440
})

growth_curve:upsert({
    id=2004,
    lv=81,
    value=445
})

growth_curve:upsert({
    id=2004,
    lv=82,
    value=450
})

growth_curve:upsert({
    id=2004,
    lv=83,
    value=455
})

growth_curve:upsert({
    id=2004,
    lv=84,
    value=460
})

growth_curve:upsert({
    id=2004,
    lv=85,
    value=465
})

growth_curve:upsert({
    id=2004,
    lv=86,
    value=470
})

growth_curve:upsert({
    id=2004,
    lv=87,
    value=475
})

growth_curve:upsert({
    id=2004,
    lv=88,
    value=480
})

growth_curve:upsert({
    id=2004,
    lv=89,
    value=485
})

growth_curve:upsert({
    id=2004,
    lv=90,
    value=490
})

growth_curve:upsert({
    id=2004,
    lv=91,
    value=495
})

growth_curve:upsert({
    id=2004,
    lv=92,
    value=500
})

growth_curve:upsert({
    id=2004,
    lv=93,
    value=505
})

growth_curve:upsert({
    id=2004,
    lv=94,
    value=510
})

growth_curve:upsert({
    id=2004,
    lv=95,
    value=515
})

growth_curve:upsert({
    id=2004,
    lv=96,
    value=520
})

growth_curve:upsert({
    id=2004,
    lv=97,
    value=525
})

growth_curve:upsert({
    id=2004,
    lv=98,
    value=530
})

growth_curve:upsert({
    id=2004,
    lv=99,
    value=535
})

growth_curve:upsert({
    id=2004,
    lv=100,
    value=540
})

growth_curve:upsert({
    id=2005,
    lv=1,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=2,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=3,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=4,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=5,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=6,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=7,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=8,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=9,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=10,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=11,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=12,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=13,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=14,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=15,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=16,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=17,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=18,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=19,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=20,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=21,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=22,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=23,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=24,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=25,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=26,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=27,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=28,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=29,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=30,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=31,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=32,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=33,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=34,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=35,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=36,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=37,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=38,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=39,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=40,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=41,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=42,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=43,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=44,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=45,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=46,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=47,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=48,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=49,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=50,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=51,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=52,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=53,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=54,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=55,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=56,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=57,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=58,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=59,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=60,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=61,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=62,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=63,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=64,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=65,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=66,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=67,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=68,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=69,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=70,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=71,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=72,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=73,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=74,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=75,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=76,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=77,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=78,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=79,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=80,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=81,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=82,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=83,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=84,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=85,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=86,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=87,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=88,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=89,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=90,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=91,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=92,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=93,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=94,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=95,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=96,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=97,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=98,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=99,
    value=0
})

growth_curve:upsert({
    id=2005,
    lv=100,
    value=0
})

growth_curve:upsert({
    id=2006,
    lv=1,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=2,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=3,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=4,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=5,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=6,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=7,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=8,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=9,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=10,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=11,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=12,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=13,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=14,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=15,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=16,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=17,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=18,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=19,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=20,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=21,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=22,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=23,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=24,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=25,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=26,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=27,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=28,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=29,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=30,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=31,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=32,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=33,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=34,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=35,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=36,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=37,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=38,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=39,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=40,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=41,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=42,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=43,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=44,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=45,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=46,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=47,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=48,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=49,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=50,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=51,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=52,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=53,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=54,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=55,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=56,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=57,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=58,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=59,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=60,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=61,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=62,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=63,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=64,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=65,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=66,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=67,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=68,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=69,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=70,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=71,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=72,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=73,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=74,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=75,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=76,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=77,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=78,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=79,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=80,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=81,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=82,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=83,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=84,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=85,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=86,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=87,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=88,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=89,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=90,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=91,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=92,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=93,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=94,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=95,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=96,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=97,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=98,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=99,
    value=10000
})

growth_curve:upsert({
    id=2006,
    lv=100,
    value=10000
})

growth_curve:upsert({
    id=2012,
    lv=1,
    value=1675
})

growth_curve:upsert({
    id=2012,
    lv=2,
    value=1750
})

growth_curve:upsert({
    id=2012,
    lv=3,
    value=1825
})

growth_curve:upsert({
    id=2012,
    lv=4,
    value=1900
})

growth_curve:upsert({
    id=2012,
    lv=5,
    value=1975
})

growth_curve:upsert({
    id=2012,
    lv=6,
    value=2050
})

growth_curve:upsert({
    id=2012,
    lv=7,
    value=2125
})

growth_curve:upsert({
    id=2012,
    lv=8,
    value=2200
})

growth_curve:upsert({
    id=2012,
    lv=9,
    value=2275
})

growth_curve:upsert({
    id=2012,
    lv=10,
    value=2350
})

growth_curve:upsert({
    id=2012,
    lv=11,
    value=2425
})

growth_curve:upsert({
    id=2012,
    lv=12,
    value=2500
})

growth_curve:upsert({
    id=2012,
    lv=13,
    value=2575
})

growth_curve:upsert({
    id=2012,
    lv=14,
    value=2650
})

growth_curve:upsert({
    id=2012,
    lv=15,
    value=2725
})

growth_curve:upsert({
    id=2012,
    lv=16,
    value=2800
})

growth_curve:upsert({
    id=2012,
    lv=17,
    value=2875
})

growth_curve:upsert({
    id=2012,
    lv=18,
    value=2950
})

growth_curve:upsert({
    id=2012,
    lv=19,
    value=3025
})

growth_curve:upsert({
    id=2012,
    lv=20,
    value=3100
})

growth_curve:upsert({
    id=2012,
    lv=21,
    value=3175
})

growth_curve:upsert({
    id=2012,
    lv=22,
    value=3250
})

growth_curve:upsert({
    id=2012,
    lv=23,
    value=3325
})

growth_curve:upsert({
    id=2012,
    lv=24,
    value=3400
})

growth_curve:upsert({
    id=2012,
    lv=25,
    value=3475
})

growth_curve:upsert({
    id=2012,
    lv=26,
    value=3550
})

growth_curve:upsert({
    id=2012,
    lv=27,
    value=3625
})

growth_curve:upsert({
    id=2012,
    lv=28,
    value=3700
})

growth_curve:upsert({
    id=2012,
    lv=29,
    value=3775
})

growth_curve:upsert({
    id=2012,
    lv=30,
    value=3850
})

growth_curve:upsert({
    id=2012,
    lv=31,
    value=3925
})

growth_curve:upsert({
    id=2012,
    lv=32,
    value=4000
})

growth_curve:upsert({
    id=2012,
    lv=33,
    value=4075
})

growth_curve:upsert({
    id=2012,
    lv=34,
    value=4150
})

growth_curve:upsert({
    id=2012,
    lv=35,
    value=4225
})

growth_curve:upsert({
    id=2012,
    lv=36,
    value=4300
})

growth_curve:upsert({
    id=2012,
    lv=37,
    value=4375
})

growth_curve:upsert({
    id=2012,
    lv=38,
    value=4450
})

growth_curve:upsert({
    id=2012,
    lv=39,
    value=4525
})

growth_curve:upsert({
    id=2012,
    lv=40,
    value=4600
})

growth_curve:upsert({
    id=2012,
    lv=41,
    value=4675
})

growth_curve:upsert({
    id=2012,
    lv=42,
    value=4750
})

growth_curve:upsert({
    id=2012,
    lv=43,
    value=4825
})

growth_curve:upsert({
    id=2012,
    lv=44,
    value=4900
})

growth_curve:upsert({
    id=2012,
    lv=45,
    value=4975
})

growth_curve:upsert({
    id=2012,
    lv=46,
    value=5050
})

growth_curve:upsert({
    id=2012,
    lv=47,
    value=5125
})

growth_curve:upsert({
    id=2012,
    lv=48,
    value=5200
})

growth_curve:upsert({
    id=2012,
    lv=49,
    value=5275
})

growth_curve:upsert({
    id=2012,
    lv=50,
    value=5350
})

growth_curve:upsert({
    id=2012,
    lv=51,
    value=5425
})

growth_curve:upsert({
    id=2012,
    lv=52,
    value=5500
})

growth_curve:upsert({
    id=2012,
    lv=53,
    value=5575
})

growth_curve:upsert({
    id=2012,
    lv=54,
    value=5650
})

growth_curve:upsert({
    id=2012,
    lv=55,
    value=5725
})

growth_curve:upsert({
    id=2012,
    lv=56,
    value=5800
})

growth_curve:upsert({
    id=2012,
    lv=57,
    value=5875
})

growth_curve:upsert({
    id=2012,
    lv=58,
    value=5950
})

growth_curve:upsert({
    id=2012,
    lv=59,
    value=6025
})

growth_curve:upsert({
    id=2012,
    lv=60,
    value=6100
})

growth_curve:upsert({
    id=2012,
    lv=61,
    value=6175
})

growth_curve:upsert({
    id=2012,
    lv=62,
    value=6250
})

growth_curve:upsert({
    id=2012,
    lv=63,
    value=6325
})

growth_curve:upsert({
    id=2012,
    lv=64,
    value=6400
})

growth_curve:upsert({
    id=2012,
    lv=65,
    value=6475
})

growth_curve:upsert({
    id=2012,
    lv=66,
    value=6550
})

growth_curve:upsert({
    id=2012,
    lv=67,
    value=6625
})

growth_curve:upsert({
    id=2012,
    lv=68,
    value=6700
})

growth_curve:upsert({
    id=2012,
    lv=69,
    value=6775
})

growth_curve:upsert({
    id=2012,
    lv=70,
    value=6850
})

growth_curve:upsert({
    id=2012,
    lv=71,
    value=6925
})

growth_curve:upsert({
    id=2012,
    lv=72,
    value=7000
})

growth_curve:upsert({
    id=2012,
    lv=73,
    value=7075
})

growth_curve:upsert({
    id=2012,
    lv=74,
    value=7150
})

growth_curve:upsert({
    id=2012,
    lv=75,
    value=7225
})

growth_curve:upsert({
    id=2012,
    lv=76,
    value=7300
})

growth_curve:upsert({
    id=2012,
    lv=77,
    value=7375
})

growth_curve:upsert({
    id=2012,
    lv=78,
    value=7450
})

growth_curve:upsert({
    id=2012,
    lv=79,
    value=7525
})

growth_curve:upsert({
    id=2012,
    lv=80,
    value=7600
})

growth_curve:upsert({
    id=2012,
    lv=81,
    value=7675
})

growth_curve:upsert({
    id=2012,
    lv=82,
    value=7750
})

growth_curve:upsert({
    id=2012,
    lv=83,
    value=7825
})

growth_curve:upsert({
    id=2012,
    lv=84,
    value=7900
})

growth_curve:upsert({
    id=2012,
    lv=85,
    value=7975
})

growth_curve:upsert({
    id=2012,
    lv=86,
    value=8050
})

growth_curve:upsert({
    id=2012,
    lv=87,
    value=8125
})

growth_curve:upsert({
    id=2012,
    lv=88,
    value=8200
})

growth_curve:upsert({
    id=2012,
    lv=89,
    value=8275
})

growth_curve:upsert({
    id=2012,
    lv=90,
    value=8350
})

growth_curve:upsert({
    id=2012,
    lv=91,
    value=8425
})

growth_curve:upsert({
    id=2012,
    lv=92,
    value=8500
})

growth_curve:upsert({
    id=2012,
    lv=93,
    value=8575
})

growth_curve:upsert({
    id=2012,
    lv=94,
    value=8650
})

growth_curve:upsert({
    id=2012,
    lv=95,
    value=8725
})

growth_curve:upsert({
    id=2012,
    lv=96,
    value=8800
})

growth_curve:upsert({
    id=2012,
    lv=97,
    value=8875
})

growth_curve:upsert({
    id=2012,
    lv=98,
    value=8950
})

growth_curve:upsert({
    id=2012,
    lv=99,
    value=9025
})

growth_curve:upsert({
    id=2012,
    lv=100,
    value=9100
})

growth_curve:upsert({
    id=2013,
    lv=1,
    value=100
})

growth_curve:upsert({
    id=2013,
    lv=2,
    value=110
})

growth_curve:upsert({
    id=2013,
    lv=3,
    value=120
})

growth_curve:upsert({
    id=2013,
    lv=4,
    value=130
})

growth_curve:upsert({
    id=2013,
    lv=5,
    value=140
})

growth_curve:upsert({
    id=2013,
    lv=6,
    value=150
})

growth_curve:upsert({
    id=2013,
    lv=7,
    value=160
})

growth_curve:upsert({
    id=2013,
    lv=8,
    value=170
})

growth_curve:upsert({
    id=2013,
    lv=9,
    value=180
})

growth_curve:upsert({
    id=2013,
    lv=10,
    value=190
})

growth_curve:upsert({
    id=2013,
    lv=11,
    value=200
})

growth_curve:upsert({
    id=2013,
    lv=12,
    value=210
})

growth_curve:upsert({
    id=2013,
    lv=13,
    value=220
})

growth_curve:upsert({
    id=2013,
    lv=14,
    value=230
})

growth_curve:upsert({
    id=2013,
    lv=15,
    value=240
})

growth_curve:upsert({
    id=2013,
    lv=16,
    value=250
})

growth_curve:upsert({
    id=2013,
    lv=17,
    value=260
})

growth_curve:upsert({
    id=2013,
    lv=18,
    value=270
})

growth_curve:upsert({
    id=2013,
    lv=19,
    value=280
})

growth_curve:upsert({
    id=2013,
    lv=20,
    value=290
})

growth_curve:upsert({
    id=2013,
    lv=21,
    value=300
})

growth_curve:upsert({
    id=2013,
    lv=22,
    value=310
})

growth_curve:upsert({
    id=2013,
    lv=23,
    value=320
})

growth_curve:upsert({
    id=2013,
    lv=24,
    value=330
})

growth_curve:upsert({
    id=2013,
    lv=25,
    value=340
})

growth_curve:upsert({
    id=2013,
    lv=26,
    value=350
})

growth_curve:upsert({
    id=2013,
    lv=27,
    value=360
})

growth_curve:upsert({
    id=2013,
    lv=28,
    value=370
})

growth_curve:upsert({
    id=2013,
    lv=29,
    value=380
})

growth_curve:upsert({
    id=2013,
    lv=30,
    value=390
})

growth_curve:upsert({
    id=2013,
    lv=31,
    value=400
})

growth_curve:upsert({
    id=2013,
    lv=32,
    value=410
})

growth_curve:upsert({
    id=2013,
    lv=33,
    value=420
})

growth_curve:upsert({
    id=2013,
    lv=34,
    value=430
})

growth_curve:upsert({
    id=2013,
    lv=35,
    value=440
})

growth_curve:upsert({
    id=2013,
    lv=36,
    value=450
})

growth_curve:upsert({
    id=2013,
    lv=37,
    value=460
})

growth_curve:upsert({
    id=2013,
    lv=38,
    value=470
})

growth_curve:upsert({
    id=2013,
    lv=39,
    value=480
})

growth_curve:upsert({
    id=2013,
    lv=40,
    value=490
})

growth_curve:upsert({
    id=2013,
    lv=41,
    value=500
})

growth_curve:upsert({
    id=2013,
    lv=42,
    value=510
})

growth_curve:upsert({
    id=2013,
    lv=43,
    value=520
})

growth_curve:upsert({
    id=2013,
    lv=44,
    value=530
})

growth_curve:upsert({
    id=2013,
    lv=45,
    value=540
})

growth_curve:upsert({
    id=2013,
    lv=46,
    value=550
})

growth_curve:upsert({
    id=2013,
    lv=47,
    value=560
})

growth_curve:upsert({
    id=2013,
    lv=48,
    value=570
})

growth_curve:upsert({
    id=2013,
    lv=49,
    value=580
})

growth_curve:upsert({
    id=2013,
    lv=50,
    value=590
})

growth_curve:upsert({
    id=2013,
    lv=51,
    value=600
})

growth_curve:upsert({
    id=2013,
    lv=52,
    value=610
})

growth_curve:upsert({
    id=2013,
    lv=53,
    value=620
})

growth_curve:upsert({
    id=2013,
    lv=54,
    value=630
})

growth_curve:upsert({
    id=2013,
    lv=55,
    value=640
})

growth_curve:upsert({
    id=2013,
    lv=56,
    value=650
})

growth_curve:upsert({
    id=2013,
    lv=57,
    value=660
})

growth_curve:upsert({
    id=2013,
    lv=58,
    value=670
})

growth_curve:upsert({
    id=2013,
    lv=59,
    value=680
})

growth_curve:upsert({
    id=2013,
    lv=60,
    value=690
})

growth_curve:upsert({
    id=2013,
    lv=61,
    value=700
})

growth_curve:upsert({
    id=2013,
    lv=62,
    value=710
})

growth_curve:upsert({
    id=2013,
    lv=63,
    value=720
})

growth_curve:upsert({
    id=2013,
    lv=64,
    value=730
})

growth_curve:upsert({
    id=2013,
    lv=65,
    value=740
})

growth_curve:upsert({
    id=2013,
    lv=66,
    value=750
})

growth_curve:upsert({
    id=2013,
    lv=67,
    value=760
})

growth_curve:upsert({
    id=2013,
    lv=68,
    value=770
})

growth_curve:upsert({
    id=2013,
    lv=69,
    value=780
})

growth_curve:upsert({
    id=2013,
    lv=70,
    value=790
})

growth_curve:upsert({
    id=2013,
    lv=71,
    value=800
})

growth_curve:upsert({
    id=2013,
    lv=72,
    value=810
})

growth_curve:upsert({
    id=2013,
    lv=73,
    value=820
})

growth_curve:upsert({
    id=2013,
    lv=74,
    value=830
})

growth_curve:upsert({
    id=2013,
    lv=75,
    value=840
})

growth_curve:upsert({
    id=2013,
    lv=76,
    value=850
})

growth_curve:upsert({
    id=2013,
    lv=77,
    value=860
})

growth_curve:upsert({
    id=2013,
    lv=78,
    value=870
})

growth_curve:upsert({
    id=2013,
    lv=79,
    value=880
})

growth_curve:upsert({
    id=2013,
    lv=80,
    value=890
})

growth_curve:upsert({
    id=2013,
    lv=81,
    value=900
})

growth_curve:upsert({
    id=2013,
    lv=82,
    value=910
})

growth_curve:upsert({
    id=2013,
    lv=83,
    value=920
})

growth_curve:upsert({
    id=2013,
    lv=84,
    value=930
})

growth_curve:upsert({
    id=2013,
    lv=85,
    value=940
})

growth_curve:upsert({
    id=2013,
    lv=86,
    value=950
})

growth_curve:upsert({
    id=2013,
    lv=87,
    value=960
})

growth_curve:upsert({
    id=2013,
    lv=88,
    value=970
})

growth_curve:upsert({
    id=2013,
    lv=89,
    value=980
})

growth_curve:upsert({
    id=2013,
    lv=90,
    value=990
})

growth_curve:upsert({
    id=2013,
    lv=91,
    value=1000
})

growth_curve:upsert({
    id=2013,
    lv=92,
    value=1010
})

growth_curve:upsert({
    id=2013,
    lv=93,
    value=1020
})

growth_curve:upsert({
    id=2013,
    lv=94,
    value=1030
})

growth_curve:upsert({
    id=2013,
    lv=95,
    value=1040
})

growth_curve:upsert({
    id=2013,
    lv=96,
    value=1050
})

growth_curve:upsert({
    id=2013,
    lv=97,
    value=1060
})

growth_curve:upsert({
    id=2013,
    lv=98,
    value=1070
})

growth_curve:upsert({
    id=2013,
    lv=99,
    value=1080
})

growth_curve:upsert({
    id=2013,
    lv=100,
    value=1090
})

growth_curve:upsert({
    id=2014,
    lv=1,
    value=68
})

growth_curve:upsert({
    id=2014,
    lv=2,
    value=76
})

growth_curve:upsert({
    id=2014,
    lv=3,
    value=84
})

growth_curve:upsert({
    id=2014,
    lv=4,
    value=92
})

growth_curve:upsert({
    id=2014,
    lv=5,
    value=100
})

growth_curve:upsert({
    id=2014,
    lv=6,
    value=108
})

growth_curve:upsert({
    id=2014,
    lv=7,
    value=116
})

growth_curve:upsert({
    id=2014,
    lv=8,
    value=124
})

growth_curve:upsert({
    id=2014,
    lv=9,
    value=132
})

growth_curve:upsert({
    id=2014,
    lv=10,
    value=140
})

growth_curve:upsert({
    id=2014,
    lv=11,
    value=148
})

growth_curve:upsert({
    id=2014,
    lv=12,
    value=156
})

growth_curve:upsert({
    id=2014,
    lv=13,
    value=164
})

growth_curve:upsert({
    id=2014,
    lv=14,
    value=172
})

growth_curve:upsert({
    id=2014,
    lv=15,
    value=180
})

growth_curve:upsert({
    id=2014,
    lv=16,
    value=188
})

growth_curve:upsert({
    id=2014,
    lv=17,
    value=196
})

growth_curve:upsert({
    id=2014,
    lv=18,
    value=204
})

growth_curve:upsert({
    id=2014,
    lv=19,
    value=212
})

growth_curve:upsert({
    id=2014,
    lv=20,
    value=220
})

growth_curve:upsert({
    id=2014,
    lv=21,
    value=228
})

growth_curve:upsert({
    id=2014,
    lv=22,
    value=236
})

growth_curve:upsert({
    id=2014,
    lv=23,
    value=244
})

growth_curve:upsert({
    id=2014,
    lv=24,
    value=252
})

growth_curve:upsert({
    id=2014,
    lv=25,
    value=260
})

growth_curve:upsert({
    id=2014,
    lv=26,
    value=268
})

growth_curve:upsert({
    id=2014,
    lv=27,
    value=276
})

growth_curve:upsert({
    id=2014,
    lv=28,
    value=284
})

growth_curve:upsert({
    id=2014,
    lv=29,
    value=292
})

growth_curve:upsert({
    id=2014,
    lv=30,
    value=300
})

growth_curve:upsert({
    id=2014,
    lv=31,
    value=308
})

growth_curve:upsert({
    id=2014,
    lv=32,
    value=316
})

growth_curve:upsert({
    id=2014,
    lv=33,
    value=324
})

growth_curve:upsert({
    id=2014,
    lv=34,
    value=332
})

growth_curve:upsert({
    id=2014,
    lv=35,
    value=340
})

growth_curve:upsert({
    id=2014,
    lv=36,
    value=348
})

growth_curve:upsert({
    id=2014,
    lv=37,
    value=356
})

growth_curve:upsert({
    id=2014,
    lv=38,
    value=364
})

growth_curve:upsert({
    id=2014,
    lv=39,
    value=372
})

growth_curve:upsert({
    id=2014,
    lv=40,
    value=380
})

growth_curve:upsert({
    id=2014,
    lv=41,
    value=388
})

growth_curve:upsert({
    id=2014,
    lv=42,
    value=396
})

growth_curve:upsert({
    id=2014,
    lv=43,
    value=404
})

growth_curve:upsert({
    id=2014,
    lv=44,
    value=412
})

growth_curve:upsert({
    id=2014,
    lv=45,
    value=420
})

growth_curve:upsert({
    id=2014,
    lv=46,
    value=428
})

growth_curve:upsert({
    id=2014,
    lv=47,
    value=436
})

growth_curve:upsert({
    id=2014,
    lv=48,
    value=444
})

growth_curve:upsert({
    id=2014,
    lv=49,
    value=452
})

growth_curve:upsert({
    id=2014,
    lv=50,
    value=460
})

growth_curve:upsert({
    id=2014,
    lv=51,
    value=468
})

growth_curve:upsert({
    id=2014,
    lv=52,
    value=476
})

growth_curve:upsert({
    id=2014,
    lv=53,
    value=484
})

growth_curve:upsert({
    id=2014,
    lv=54,
    value=492
})

growth_curve:upsert({
    id=2014,
    lv=55,
    value=500
})

growth_curve:upsert({
    id=2014,
    lv=56,
    value=508
})

growth_curve:upsert({
    id=2014,
    lv=57,
    value=516
})

growth_curve:upsert({
    id=2014,
    lv=58,
    value=524
})

growth_curve:upsert({
    id=2014,
    lv=59,
    value=532
})

growth_curve:upsert({
    id=2014,
    lv=60,
    value=540
})

growth_curve:upsert({
    id=2014,
    lv=61,
    value=548
})

growth_curve:upsert({
    id=2014,
    lv=62,
    value=556
})

growth_curve:upsert({
    id=2014,
    lv=63,
    value=564
})

growth_curve:upsert({
    id=2014,
    lv=64,
    value=572
})

growth_curve:upsert({
    id=2014,
    lv=65,
    value=580
})

growth_curve:upsert({
    id=2014,
    lv=66,
    value=588
})

growth_curve:upsert({
    id=2014,
    lv=67,
    value=596
})

growth_curve:upsert({
    id=2014,
    lv=68,
    value=604
})

growth_curve:upsert({
    id=2014,
    lv=69,
    value=612
})

growth_curve:upsert({
    id=2014,
    lv=70,
    value=620
})

growth_curve:upsert({
    id=2014,
    lv=71,
    value=628
})

growth_curve:upsert({
    id=2014,
    lv=72,
    value=636
})

growth_curve:upsert({
    id=2014,
    lv=73,
    value=644
})

growth_curve:upsert({
    id=2014,
    lv=74,
    value=652
})

growth_curve:upsert({
    id=2014,
    lv=75,
    value=660
})

growth_curve:upsert({
    id=2014,
    lv=76,
    value=668
})

growth_curve:upsert({
    id=2014,
    lv=77,
    value=676
})

growth_curve:upsert({
    id=2014,
    lv=78,
    value=684
})

growth_curve:upsert({
    id=2014,
    lv=79,
    value=692
})

growth_curve:upsert({
    id=2014,
    lv=80,
    value=700
})

growth_curve:upsert({
    id=2014,
    lv=81,
    value=708
})

growth_curve:upsert({
    id=2014,
    lv=82,
    value=716
})

growth_curve:upsert({
    id=2014,
    lv=83,
    value=724
})

growth_curve:upsert({
    id=2014,
    lv=84,
    value=732
})

growth_curve:upsert({
    id=2014,
    lv=85,
    value=740
})

growth_curve:upsert({
    id=2014,
    lv=86,
    value=748
})

growth_curve:upsert({
    id=2014,
    lv=87,
    value=756
})

growth_curve:upsert({
    id=2014,
    lv=88,
    value=764
})

growth_curve:upsert({
    id=2014,
    lv=89,
    value=772
})

growth_curve:upsert({
    id=2014,
    lv=90,
    value=780
})

growth_curve:upsert({
    id=2014,
    lv=91,
    value=788
})

growth_curve:upsert({
    id=2014,
    lv=92,
    value=796
})

growth_curve:upsert({
    id=2014,
    lv=93,
    value=804
})

growth_curve:upsert({
    id=2014,
    lv=94,
    value=812
})

growth_curve:upsert({
    id=2014,
    lv=95,
    value=820
})

growth_curve:upsert({
    id=2014,
    lv=96,
    value=828
})

growth_curve:upsert({
    id=2014,
    lv=97,
    value=836
})

growth_curve:upsert({
    id=2014,
    lv=98,
    value=844
})

growth_curve:upsert({
    id=2014,
    lv=99,
    value=852
})

growth_curve:upsert({
    id=2014,
    lv=100,
    value=860
})

growth_curve:upsert({
    id=2022,
    lv=1,
    value=1885
})

growth_curve:upsert({
    id=2022,
    lv=2,
    value=1970
})

growth_curve:upsert({
    id=2022,
    lv=3,
    value=2055
})

growth_curve:upsert({
    id=2022,
    lv=4,
    value=2140
})

growth_curve:upsert({
    id=2022,
    lv=5,
    value=2225
})

growth_curve:upsert({
    id=2022,
    lv=6,
    value=2310
})

growth_curve:upsert({
    id=2022,
    lv=7,
    value=2395
})

growth_curve:upsert({
    id=2022,
    lv=8,
    value=2480
})

growth_curve:upsert({
    id=2022,
    lv=9,
    value=2565
})

growth_curve:upsert({
    id=2022,
    lv=10,
    value=2650
})

growth_curve:upsert({
    id=2022,
    lv=11,
    value=2735
})

growth_curve:upsert({
    id=2022,
    lv=12,
    value=2820
})

growth_curve:upsert({
    id=2022,
    lv=13,
    value=2905
})

growth_curve:upsert({
    id=2022,
    lv=14,
    value=2990
})

growth_curve:upsert({
    id=2022,
    lv=15,
    value=3075
})

growth_curve:upsert({
    id=2022,
    lv=16,
    value=3160
})

growth_curve:upsert({
    id=2022,
    lv=17,
    value=3245
})

growth_curve:upsert({
    id=2022,
    lv=18,
    value=3330
})

growth_curve:upsert({
    id=2022,
    lv=19,
    value=3415
})

growth_curve:upsert({
    id=2022,
    lv=20,
    value=3500
})

growth_curve:upsert({
    id=2022,
    lv=21,
    value=3585
})

growth_curve:upsert({
    id=2022,
    lv=22,
    value=3670
})

growth_curve:upsert({
    id=2022,
    lv=23,
    value=3755
})

growth_curve:upsert({
    id=2022,
    lv=24,
    value=3840
})

growth_curve:upsert({
    id=2022,
    lv=25,
    value=3925
})

growth_curve:upsert({
    id=2022,
    lv=26,
    value=4010
})

growth_curve:upsert({
    id=2022,
    lv=27,
    value=4095
})

growth_curve:upsert({
    id=2022,
    lv=28,
    value=4180
})

growth_curve:upsert({
    id=2022,
    lv=29,
    value=4265
})

growth_curve:upsert({
    id=2022,
    lv=30,
    value=4350
})

growth_curve:upsert({
    id=2022,
    lv=31,
    value=4435
})

growth_curve:upsert({
    id=2022,
    lv=32,
    value=4520
})

growth_curve:upsert({
    id=2022,
    lv=33,
    value=4605
})

growth_curve:upsert({
    id=2022,
    lv=34,
    value=4690
})

growth_curve:upsert({
    id=2022,
    lv=35,
    value=4775
})

growth_curve:upsert({
    id=2022,
    lv=36,
    value=4860
})

growth_curve:upsert({
    id=2022,
    lv=37,
    value=4945
})

growth_curve:upsert({
    id=2022,
    lv=38,
    value=5030
})

growth_curve:upsert({
    id=2022,
    lv=39,
    value=5115
})

growth_curve:upsert({
    id=2022,
    lv=40,
    value=5200
})

growth_curve:upsert({
    id=2022,
    lv=41,
    value=5285
})

growth_curve:upsert({
    id=2022,
    lv=42,
    value=5370
})

growth_curve:upsert({
    id=2022,
    lv=43,
    value=5455
})

growth_curve:upsert({
    id=2022,
    lv=44,
    value=5540
})

growth_curve:upsert({
    id=2022,
    lv=45,
    value=5625
})

growth_curve:upsert({
    id=2022,
    lv=46,
    value=5710
})

growth_curve:upsert({
    id=2022,
    lv=47,
    value=5795
})

growth_curve:upsert({
    id=2022,
    lv=48,
    value=5880
})

growth_curve:upsert({
    id=2022,
    lv=49,
    value=5965
})

growth_curve:upsert({
    id=2022,
    lv=50,
    value=6050
})

growth_curve:upsert({
    id=2022,
    lv=51,
    value=6135
})

growth_curve:upsert({
    id=2022,
    lv=52,
    value=6220
})

growth_curve:upsert({
    id=2022,
    lv=53,
    value=6305
})

growth_curve:upsert({
    id=2022,
    lv=54,
    value=6390
})

growth_curve:upsert({
    id=2022,
    lv=55,
    value=6475
})

growth_curve:upsert({
    id=2022,
    lv=56,
    value=6560
})

growth_curve:upsert({
    id=2022,
    lv=57,
    value=6645
})

growth_curve:upsert({
    id=2022,
    lv=58,
    value=6730
})

growth_curve:upsert({
    id=2022,
    lv=59,
    value=6815
})

growth_curve:upsert({
    id=2022,
    lv=60,
    value=6900
})

growth_curve:upsert({
    id=2022,
    lv=61,
    value=6985
})

growth_curve:upsert({
    id=2022,
    lv=62,
    value=7070
})

growth_curve:upsert({
    id=2022,
    lv=63,
    value=7155
})

growth_curve:upsert({
    id=2022,
    lv=64,
    value=7240
})

growth_curve:upsert({
    id=2022,
    lv=65,
    value=7325
})

growth_curve:upsert({
    id=2022,
    lv=66,
    value=7410
})

growth_curve:upsert({
    id=2022,
    lv=67,
    value=7495
})

growth_curve:upsert({
    id=2022,
    lv=68,
    value=7580
})

growth_curve:upsert({
    id=2022,
    lv=69,
    value=7665
})

growth_curve:upsert({
    id=2022,
    lv=70,
    value=7750
})

growth_curve:upsert({
    id=2022,
    lv=71,
    value=7835
})

growth_curve:upsert({
    id=2022,
    lv=72,
    value=7920
})

growth_curve:upsert({
    id=2022,
    lv=73,
    value=8005
})

growth_curve:upsert({
    id=2022,
    lv=74,
    value=8090
})

growth_curve:upsert({
    id=2022,
    lv=75,
    value=8175
})

growth_curve:upsert({
    id=2022,
    lv=76,
    value=8260
})

growth_curve:upsert({
    id=2022,
    lv=77,
    value=8345
})

growth_curve:upsert({
    id=2022,
    lv=78,
    value=8430
})

growth_curve:upsert({
    id=2022,
    lv=79,
    value=8515
})

growth_curve:upsert({
    id=2022,
    lv=80,
    value=8600
})

growth_curve:upsert({
    id=2022,
    lv=81,
    value=8685
})

growth_curve:upsert({
    id=2022,
    lv=82,
    value=8770
})

growth_curve:upsert({
    id=2022,
    lv=83,
    value=8855
})

growth_curve:upsert({
    id=2022,
    lv=84,
    value=8940
})

growth_curve:upsert({
    id=2022,
    lv=85,
    value=9025
})

growth_curve:upsert({
    id=2022,
    lv=86,
    value=9110
})

growth_curve:upsert({
    id=2022,
    lv=87,
    value=9195
})

growth_curve:upsert({
    id=2022,
    lv=88,
    value=9280
})

growth_curve:upsert({
    id=2022,
    lv=89,
    value=9365
})

growth_curve:upsert({
    id=2022,
    lv=90,
    value=9450
})

growth_curve:upsert({
    id=2022,
    lv=91,
    value=9535
})

growth_curve:upsert({
    id=2022,
    lv=92,
    value=9620
})

growth_curve:upsert({
    id=2022,
    lv=93,
    value=9705
})

growth_curve:upsert({
    id=2022,
    lv=94,
    value=9790
})

growth_curve:upsert({
    id=2022,
    lv=95,
    value=9875
})

growth_curve:upsert({
    id=2022,
    lv=96,
    value=9960
})

growth_curve:upsert({
    id=2022,
    lv=97,
    value=10045
})

growth_curve:upsert({
    id=2022,
    lv=98,
    value=10130
})

growth_curve:upsert({
    id=2022,
    lv=99,
    value=10215
})

growth_curve:upsert({
    id=2022,
    lv=100,
    value=10300
})

growth_curve:upsert({
    id=2023,
    lv=1,
    value=68
})

growth_curve:upsert({
    id=2023,
    lv=2,
    value=76
})

growth_curve:upsert({
    id=2023,
    lv=3,
    value=84
})

growth_curve:upsert({
    id=2023,
    lv=4,
    value=92
})

growth_curve:upsert({
    id=2023,
    lv=5,
    value=100
})

growth_curve:upsert({
    id=2023,
    lv=6,
    value=108
})

growth_curve:upsert({
    id=2023,
    lv=7,
    value=116
})

growth_curve:upsert({
    id=2023,
    lv=8,
    value=124
})

growth_curve:upsert({
    id=2023,
    lv=9,
    value=132
})

growth_curve:upsert({
    id=2023,
    lv=10,
    value=140
})

growth_curve:upsert({
    id=2023,
    lv=11,
    value=148
})

growth_curve:upsert({
    id=2023,
    lv=12,
    value=156
})

growth_curve:upsert({
    id=2023,
    lv=13,
    value=164
})

growth_curve:upsert({
    id=2023,
    lv=14,
    value=172
})

growth_curve:upsert({
    id=2023,
    lv=15,
    value=180
})

growth_curve:upsert({
    id=2023,
    lv=16,
    value=188
})

growth_curve:upsert({
    id=2023,
    lv=17,
    value=196
})

growth_curve:upsert({
    id=2023,
    lv=18,
    value=204
})

growth_curve:upsert({
    id=2023,
    lv=19,
    value=212
})

growth_curve:upsert({
    id=2023,
    lv=20,
    value=220
})

growth_curve:upsert({
    id=2023,
    lv=21,
    value=228
})

growth_curve:upsert({
    id=2023,
    lv=22,
    value=236
})

growth_curve:upsert({
    id=2023,
    lv=23,
    value=244
})

growth_curve:upsert({
    id=2023,
    lv=24,
    value=252
})

growth_curve:upsert({
    id=2023,
    lv=25,
    value=260
})

growth_curve:upsert({
    id=2023,
    lv=26,
    value=268
})

growth_curve:upsert({
    id=2023,
    lv=27,
    value=276
})

growth_curve:upsert({
    id=2023,
    lv=28,
    value=284
})

growth_curve:upsert({
    id=2023,
    lv=29,
    value=292
})

growth_curve:upsert({
    id=2023,
    lv=30,
    value=300
})

growth_curve:upsert({
    id=2023,
    lv=31,
    value=308
})

growth_curve:upsert({
    id=2023,
    lv=32,
    value=316
})

growth_curve:upsert({
    id=2023,
    lv=33,
    value=324
})

growth_curve:upsert({
    id=2023,
    lv=34,
    value=332
})

growth_curve:upsert({
    id=2023,
    lv=35,
    value=340
})

growth_curve:upsert({
    id=2023,
    lv=36,
    value=348
})

growth_curve:upsert({
    id=2023,
    lv=37,
    value=356
})

growth_curve:upsert({
    id=2023,
    lv=38,
    value=364
})

growth_curve:upsert({
    id=2023,
    lv=39,
    value=372
})

growth_curve:upsert({
    id=2023,
    lv=40,
    value=380
})

growth_curve:upsert({
    id=2023,
    lv=41,
    value=388
})

growth_curve:upsert({
    id=2023,
    lv=42,
    value=396
})

growth_curve:upsert({
    id=2023,
    lv=43,
    value=404
})

growth_curve:upsert({
    id=2023,
    lv=44,
    value=412
})

growth_curve:upsert({
    id=2023,
    lv=45,
    value=420
})

growth_curve:upsert({
    id=2023,
    lv=46,
    value=428
})

growth_curve:upsert({
    id=2023,
    lv=47,
    value=436
})

growth_curve:upsert({
    id=2023,
    lv=48,
    value=444
})

growth_curve:upsert({
    id=2023,
    lv=49,
    value=452
})

growth_curve:upsert({
    id=2023,
    lv=50,
    value=460
})

growth_curve:upsert({
    id=2023,
    lv=51,
    value=468
})

growth_curve:upsert({
    id=2023,
    lv=52,
    value=476
})

growth_curve:upsert({
    id=2023,
    lv=53,
    value=484
})

growth_curve:upsert({
    id=2023,
    lv=54,
    value=492
})

growth_curve:upsert({
    id=2023,
    lv=55,
    value=500
})

growth_curve:upsert({
    id=2023,
    lv=56,
    value=508
})

growth_curve:upsert({
    id=2023,
    lv=57,
    value=516
})

growth_curve:upsert({
    id=2023,
    lv=58,
    value=524
})

growth_curve:upsert({
    id=2023,
    lv=59,
    value=532
})

growth_curve:upsert({
    id=2023,
    lv=60,
    value=540
})

growth_curve:upsert({
    id=2023,
    lv=61,
    value=548
})

growth_curve:upsert({
    id=2023,
    lv=62,
    value=556
})

growth_curve:upsert({
    id=2023,
    lv=63,
    value=564
})

growth_curve:upsert({
    id=2023,
    lv=64,
    value=572
})

growth_curve:upsert({
    id=2023,
    lv=65,
    value=580
})

growth_curve:upsert({
    id=2023,
    lv=66,
    value=588
})

growth_curve:upsert({
    id=2023,
    lv=67,
    value=596
})

growth_curve:upsert({
    id=2023,
    lv=68,
    value=604
})

growth_curve:upsert({
    id=2023,
    lv=69,
    value=612
})

growth_curve:upsert({
    id=2023,
    lv=70,
    value=620
})

growth_curve:upsert({
    id=2023,
    lv=71,
    value=628
})

growth_curve:upsert({
    id=2023,
    lv=72,
    value=636
})

growth_curve:upsert({
    id=2023,
    lv=73,
    value=644
})

growth_curve:upsert({
    id=2023,
    lv=74,
    value=652
})

growth_curve:upsert({
    id=2023,
    lv=75,
    value=660
})

growth_curve:upsert({
    id=2023,
    lv=76,
    value=668
})

growth_curve:upsert({
    id=2023,
    lv=77,
    value=676
})

growth_curve:upsert({
    id=2023,
    lv=78,
    value=684
})

growth_curve:upsert({
    id=2023,
    lv=79,
    value=692
})

growth_curve:upsert({
    id=2023,
    lv=80,
    value=700
})

growth_curve:upsert({
    id=2023,
    lv=81,
    value=708
})

growth_curve:upsert({
    id=2023,
    lv=82,
    value=716
})

growth_curve:upsert({
    id=2023,
    lv=83,
    value=724
})

growth_curve:upsert({
    id=2023,
    lv=84,
    value=732
})

growth_curve:upsert({
    id=2023,
    lv=85,
    value=740
})

growth_curve:upsert({
    id=2023,
    lv=86,
    value=748
})

growth_curve:upsert({
    id=2023,
    lv=87,
    value=756
})

growth_curve:upsert({
    id=2023,
    lv=88,
    value=764
})

growth_curve:upsert({
    id=2023,
    lv=89,
    value=772
})

growth_curve:upsert({
    id=2023,
    lv=90,
    value=780
})

growth_curve:upsert({
    id=2023,
    lv=91,
    value=788
})

growth_curve:upsert({
    id=2023,
    lv=92,
    value=796
})

growth_curve:upsert({
    id=2023,
    lv=93,
    value=804
})

growth_curve:upsert({
    id=2023,
    lv=94,
    value=812
})

growth_curve:upsert({
    id=2023,
    lv=95,
    value=820
})

growth_curve:upsert({
    id=2023,
    lv=96,
    value=828
})

growth_curve:upsert({
    id=2023,
    lv=97,
    value=836
})

growth_curve:upsert({
    id=2023,
    lv=98,
    value=844
})

growth_curve:upsert({
    id=2023,
    lv=99,
    value=852
})

growth_curve:upsert({
    id=2023,
    lv=100,
    value=860
})

growth_curve:upsert({
    id=2024,
    lv=1,
    value=92
})

growth_curve:upsert({
    id=2024,
    lv=2,
    value=104
})

growth_curve:upsert({
    id=2024,
    lv=3,
    value=116
})

growth_curve:upsert({
    id=2024,
    lv=4,
    value=128
})

growth_curve:upsert({
    id=2024,
    lv=5,
    value=140
})

growth_curve:upsert({
    id=2024,
    lv=6,
    value=152
})

growth_curve:upsert({
    id=2024,
    lv=7,
    value=164
})

growth_curve:upsert({
    id=2024,
    lv=8,
    value=176
})

growth_curve:upsert({
    id=2024,
    lv=9,
    value=188
})

growth_curve:upsert({
    id=2024,
    lv=10,
    value=200
})

growth_curve:upsert({
    id=2024,
    lv=11,
    value=212
})

growth_curve:upsert({
    id=2024,
    lv=12,
    value=224
})

growth_curve:upsert({
    id=2024,
    lv=13,
    value=236
})

growth_curve:upsert({
    id=2024,
    lv=14,
    value=248
})

growth_curve:upsert({
    id=2024,
    lv=15,
    value=260
})

growth_curve:upsert({
    id=2024,
    lv=16,
    value=272
})

growth_curve:upsert({
    id=2024,
    lv=17,
    value=284
})

growth_curve:upsert({
    id=2024,
    lv=18,
    value=296
})

growth_curve:upsert({
    id=2024,
    lv=19,
    value=308
})

growth_curve:upsert({
    id=2024,
    lv=20,
    value=320
})

growth_curve:upsert({
    id=2024,
    lv=21,
    value=332
})

growth_curve:upsert({
    id=2024,
    lv=22,
    value=344
})

growth_curve:upsert({
    id=2024,
    lv=23,
    value=356
})

growth_curve:upsert({
    id=2024,
    lv=24,
    value=368
})

growth_curve:upsert({
    id=2024,
    lv=25,
    value=380
})

growth_curve:upsert({
    id=2024,
    lv=26,
    value=392
})

growth_curve:upsert({
    id=2024,
    lv=27,
    value=404
})

growth_curve:upsert({
    id=2024,
    lv=28,
    value=416
})

growth_curve:upsert({
    id=2024,
    lv=29,
    value=428
})

growth_curve:upsert({
    id=2024,
    lv=30,
    value=440
})

growth_curve:upsert({
    id=2024,
    lv=31,
    value=452
})

growth_curve:upsert({
    id=2024,
    lv=32,
    value=464
})

growth_curve:upsert({
    id=2024,
    lv=33,
    value=476
})

growth_curve:upsert({
    id=2024,
    lv=34,
    value=488
})

growth_curve:upsert({
    id=2024,
    lv=35,
    value=500
})

growth_curve:upsert({
    id=2024,
    lv=36,
    value=512
})

growth_curve:upsert({
    id=2024,
    lv=37,
    value=524
})

growth_curve:upsert({
    id=2024,
    lv=38,
    value=536
})

growth_curve:upsert({
    id=2024,
    lv=39,
    value=548
})

growth_curve:upsert({
    id=2024,
    lv=40,
    value=560
})

growth_curve:upsert({
    id=2024,
    lv=41,
    value=572
})

growth_curve:upsert({
    id=2024,
    lv=42,
    value=584
})

growth_curve:upsert({
    id=2024,
    lv=43,
    value=596
})

growth_curve:upsert({
    id=2024,
    lv=44,
    value=608
})

growth_curve:upsert({
    id=2024,
    lv=45,
    value=620
})

growth_curve:upsert({
    id=2024,
    lv=46,
    value=632
})

growth_curve:upsert({
    id=2024,
    lv=47,
    value=644
})

growth_curve:upsert({
    id=2024,
    lv=48,
    value=656
})

growth_curve:upsert({
    id=2024,
    lv=49,
    value=668
})

growth_curve:upsert({
    id=2024,
    lv=50,
    value=680
})

growth_curve:upsert({
    id=2024,
    lv=51,
    value=692
})

growth_curve:upsert({
    id=2024,
    lv=52,
    value=704
})

growth_curve:upsert({
    id=2024,
    lv=53,
    value=716
})

growth_curve:upsert({
    id=2024,
    lv=54,
    value=728
})

growth_curve:upsert({
    id=2024,
    lv=55,
    value=740
})

growth_curve:upsert({
    id=2024,
    lv=56,
    value=752
})

growth_curve:upsert({
    id=2024,
    lv=57,
    value=764
})

growth_curve:upsert({
    id=2024,
    lv=58,
    value=776
})

growth_curve:upsert({
    id=2024,
    lv=59,
    value=788
})

growth_curve:upsert({
    id=2024,
    lv=60,
    value=800
})

growth_curve:upsert({
    id=2024,
    lv=61,
    value=812
})

growth_curve:upsert({
    id=2024,
    lv=62,
    value=824
})

growth_curve:upsert({
    id=2024,
    lv=63,
    value=836
})

growth_curve:upsert({
    id=2024,
    lv=64,
    value=848
})

growth_curve:upsert({
    id=2024,
    lv=65,
    value=860
})

growth_curve:upsert({
    id=2024,
    lv=66,
    value=872
})

growth_curve:upsert({
    id=2024,
    lv=67,
    value=884
})

growth_curve:upsert({
    id=2024,
    lv=68,
    value=896
})

growth_curve:upsert({
    id=2024,
    lv=69,
    value=908
})

growth_curve:upsert({
    id=2024,
    lv=70,
    value=920
})

growth_curve:upsert({
    id=2024,
    lv=71,
    value=932
})

growth_curve:upsert({
    id=2024,
    lv=72,
    value=944
})

growth_curve:upsert({
    id=2024,
    lv=73,
    value=956
})

growth_curve:upsert({
    id=2024,
    lv=74,
    value=968
})

growth_curve:upsert({
    id=2024,
    lv=75,
    value=980
})

growth_curve:upsert({
    id=2024,
    lv=76,
    value=992
})

growth_curve:upsert({
    id=2024,
    lv=77,
    value=1004
})

growth_curve:upsert({
    id=2024,
    lv=78,
    value=1016
})

growth_curve:upsert({
    id=2024,
    lv=79,
    value=1028
})

growth_curve:upsert({
    id=2024,
    lv=80,
    value=1040
})

growth_curve:upsert({
    id=2024,
    lv=81,
    value=1052
})

growth_curve:upsert({
    id=2024,
    lv=82,
    value=1064
})

growth_curve:upsert({
    id=2024,
    lv=83,
    value=1076
})

growth_curve:upsert({
    id=2024,
    lv=84,
    value=1088
})

growth_curve:upsert({
    id=2024,
    lv=85,
    value=1100
})

growth_curve:upsert({
    id=2024,
    lv=86,
    value=1112
})

growth_curve:upsert({
    id=2024,
    lv=87,
    value=1124
})

growth_curve:upsert({
    id=2024,
    lv=88,
    value=1136
})

growth_curve:upsert({
    id=2024,
    lv=89,
    value=1148
})

growth_curve:upsert({
    id=2024,
    lv=90,
    value=1160
})

growth_curve:upsert({
    id=2024,
    lv=91,
    value=1172
})

growth_curve:upsert({
    id=2024,
    lv=92,
    value=1184
})

growth_curve:upsert({
    id=2024,
    lv=93,
    value=1196
})

growth_curve:upsert({
    id=2024,
    lv=94,
    value=1208
})

growth_curve:upsert({
    id=2024,
    lv=95,
    value=1220
})

growth_curve:upsert({
    id=2024,
    lv=96,
    value=1232
})

growth_curve:upsert({
    id=2024,
    lv=97,
    value=1244
})

growth_curve:upsert({
    id=2024,
    lv=98,
    value=1256
})

growth_curve:upsert({
    id=2024,
    lv=99,
    value=1268
})

growth_curve:upsert({
    id=2024,
    lv=100,
    value=1280
})

growth_curve:upsert({
    id=2032,
    lv=1,
    value=285
})

growth_curve:upsert({
    id=2032,
    lv=2,
    value=320
})

growth_curve:upsert({
    id=2032,
    lv=3,
    value=355
})

growth_curve:upsert({
    id=2032,
    lv=4,
    value=390
})

growth_curve:upsert({
    id=2032,
    lv=5,
    value=425
})

growth_curve:upsert({
    id=2032,
    lv=6,
    value=460
})

growth_curve:upsert({
    id=2032,
    lv=7,
    value=495
})

growth_curve:upsert({
    id=2032,
    lv=8,
    value=530
})

growth_curve:upsert({
    id=2032,
    lv=9,
    value=565
})

growth_curve:upsert({
    id=2032,
    lv=10,
    value=600
})

growth_curve:upsert({
    id=2032,
    lv=11,
    value=635
})

growth_curve:upsert({
    id=2032,
    lv=12,
    value=670
})

growth_curve:upsert({
    id=2032,
    lv=13,
    value=705
})

growth_curve:upsert({
    id=2032,
    lv=14,
    value=740
})

growth_curve:upsert({
    id=2032,
    lv=15,
    value=775
})

growth_curve:upsert({
    id=2032,
    lv=16,
    value=810
})

growth_curve:upsert({
    id=2032,
    lv=17,
    value=845
})

growth_curve:upsert({
    id=2032,
    lv=18,
    value=880
})

growth_curve:upsert({
    id=2032,
    lv=19,
    value=915
})

growth_curve:upsert({
    id=2032,
    lv=20,
    value=950
})

growth_curve:upsert({
    id=2032,
    lv=21,
    value=985
})

growth_curve:upsert({
    id=2032,
    lv=22,
    value=1020
})

growth_curve:upsert({
    id=2032,
    lv=23,
    value=1055
})

growth_curve:upsert({
    id=2032,
    lv=24,
    value=1090
})

growth_curve:upsert({
    id=2032,
    lv=25,
    value=1125
})

growth_curve:upsert({
    id=2032,
    lv=26,
    value=1160
})

growth_curve:upsert({
    id=2032,
    lv=27,
    value=1195
})

growth_curve:upsert({
    id=2032,
    lv=28,
    value=1230
})

growth_curve:upsert({
    id=2032,
    lv=29,
    value=1265
})

growth_curve:upsert({
    id=2032,
    lv=30,
    value=1300
})

growth_curve:upsert({
    id=2032,
    lv=31,
    value=1335
})

growth_curve:upsert({
    id=2032,
    lv=32,
    value=1370
})

growth_curve:upsert({
    id=2032,
    lv=33,
    value=1405
})

growth_curve:upsert({
    id=2032,
    lv=34,
    value=1440
})

growth_curve:upsert({
    id=2032,
    lv=35,
    value=1475
})

growth_curve:upsert({
    id=2032,
    lv=36,
    value=1510
})

growth_curve:upsert({
    id=2032,
    lv=37,
    value=1545
})

growth_curve:upsert({
    id=2032,
    lv=38,
    value=1580
})

growth_curve:upsert({
    id=2032,
    lv=39,
    value=1615
})

growth_curve:upsert({
    id=2032,
    lv=40,
    value=1650
})

growth_curve:upsert({
    id=2032,
    lv=41,
    value=1685
})

growth_curve:upsert({
    id=2032,
    lv=42,
    value=1720
})

growth_curve:upsert({
    id=2032,
    lv=43,
    value=1755
})

growth_curve:upsert({
    id=2032,
    lv=44,
    value=1790
})

growth_curve:upsert({
    id=2032,
    lv=45,
    value=1825
})

growth_curve:upsert({
    id=2032,
    lv=46,
    value=1860
})

growth_curve:upsert({
    id=2032,
    lv=47,
    value=1895
})

growth_curve:upsert({
    id=2032,
    lv=48,
    value=1930
})

growth_curve:upsert({
    id=2032,
    lv=49,
    value=1965
})

growth_curve:upsert({
    id=2032,
    lv=50,
    value=2000
})

growth_curve:upsert({
    id=2032,
    lv=51,
    value=2035
})

growth_curve:upsert({
    id=2032,
    lv=52,
    value=2070
})

growth_curve:upsert({
    id=2032,
    lv=53,
    value=2105
})

growth_curve:upsert({
    id=2032,
    lv=54,
    value=2140
})

growth_curve:upsert({
    id=2032,
    lv=55,
    value=2175
})

growth_curve:upsert({
    id=2032,
    lv=56,
    value=2210
})

growth_curve:upsert({
    id=2032,
    lv=57,
    value=2245
})

growth_curve:upsert({
    id=2032,
    lv=58,
    value=2280
})

growth_curve:upsert({
    id=2032,
    lv=59,
    value=2315
})

growth_curve:upsert({
    id=2032,
    lv=60,
    value=2350
})

growth_curve:upsert({
    id=2032,
    lv=61,
    value=2385
})

growth_curve:upsert({
    id=2032,
    lv=62,
    value=2420
})

growth_curve:upsert({
    id=2032,
    lv=63,
    value=2455
})

growth_curve:upsert({
    id=2032,
    lv=64,
    value=2490
})

growth_curve:upsert({
    id=2032,
    lv=65,
    value=2525
})

growth_curve:upsert({
    id=2032,
    lv=66,
    value=2560
})

growth_curve:upsert({
    id=2032,
    lv=67,
    value=2595
})

growth_curve:upsert({
    id=2032,
    lv=68,
    value=2630
})

growth_curve:upsert({
    id=2032,
    lv=69,
    value=2665
})

growth_curve:upsert({
    id=2032,
    lv=70,
    value=2700
})

growth_curve:upsert({
    id=2032,
    lv=71,
    value=2735
})

growth_curve:upsert({
    id=2032,
    lv=72,
    value=2770
})

growth_curve:upsert({
    id=2032,
    lv=73,
    value=2805
})

growth_curve:upsert({
    id=2032,
    lv=74,
    value=2840
})

growth_curve:upsert({
    id=2032,
    lv=75,
    value=2875
})

growth_curve:upsert({
    id=2032,
    lv=76,
    value=2910
})

growth_curve:upsert({
    id=2032,
    lv=77,
    value=2945
})

growth_curve:upsert({
    id=2032,
    lv=78,
    value=2980
})

growth_curve:upsert({
    id=2032,
    lv=79,
    value=3015
})

growth_curve:upsert({
    id=2032,
    lv=80,
    value=3050
})

growth_curve:upsert({
    id=2032,
    lv=81,
    value=3085
})

growth_curve:upsert({
    id=2032,
    lv=82,
    value=3120
})

growth_curve:upsert({
    id=2032,
    lv=83,
    value=3155
})

growth_curve:upsert({
    id=2032,
    lv=84,
    value=3190
})

growth_curve:upsert({
    id=2032,
    lv=85,
    value=3225
})

growth_curve:upsert({
    id=2032,
    lv=86,
    value=3260
})

growth_curve:upsert({
    id=2032,
    lv=87,
    value=3295
})

growth_curve:upsert({
    id=2032,
    lv=88,
    value=3330
})

growth_curve:upsert({
    id=2032,
    lv=89,
    value=3365
})

growth_curve:upsert({
    id=2032,
    lv=90,
    value=3400
})

growth_curve:upsert({
    id=2032,
    lv=91,
    value=3435
})

growth_curve:upsert({
    id=2032,
    lv=92,
    value=3470
})

growth_curve:upsert({
    id=2032,
    lv=93,
    value=3505
})

growth_curve:upsert({
    id=2032,
    lv=94,
    value=3540
})

growth_curve:upsert({
    id=2032,
    lv=95,
    value=3575
})

growth_curve:upsert({
    id=2032,
    lv=96,
    value=3610
})

growth_curve:upsert({
    id=2032,
    lv=97,
    value=3645
})

growth_curve:upsert({
    id=2032,
    lv=98,
    value=3680
})

growth_curve:upsert({
    id=2032,
    lv=99,
    value=3715
})

growth_curve:upsert({
    id=2032,
    lv=100,
    value=3750
})

growth_curve:update()
