--npc_task_show_cfg.lua
--source: 3_npc_NPC任务展示表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local npc_task_show = config_mgr:get_table("npc_task_show")

--导出配置内容
npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_1'
    },
    id=10001,
    task_id=2002
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_4',
        'LC_label_ui_Triggering_loving_1',
        'LC_label_ui_Triggering_building_3',
        'LC_label_ui_Triggering_building_6'
    },
    id=10002,
    task_id=7011
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_2',
        'LC_label_ui_Triggering_building_1',
        'LC_label_ui_Triggering_loving_2'
    },
    id=10101,
    task_id=5001
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_9',
        'LC_label_ui_Triggering_loving_11'
    },
    id=10102,
    task_id=7010
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_people_1'
    },
    id=10201,
    task_id=3001
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_7',
        'LC_label_ui_Triggering_loving_3'
    },
    id=10202,
    task_id=7006
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_1',
        'LC_label_ui_Triggering_building_2'
    },
    id=10301,
    task_id=3005
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_8',
        'LC_label_ui_Triggering_loving_4'
    },
    id=10302,
    task_id=7005
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_people_2',
        'LC_label_ui_Triggering_building_4',
        'LC_label_ui_Triggering_work_1'
    },
    id=10401,
    task_id=7001
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_12',
        'LC_label_ui_Triggering_loving_5'
    },
    id=10402,
    task_id=7009
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_people_3',
        'LC_label_ui_Triggering_building_3'
    },
    id=10501,
    task_id=2003
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_6',
        'LC_label_ui_Triggering_loving_6'
    },
    id=10502,
    task_id=5003
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_people_4',
        'LC_label_ui_Triggering_people_3',
        'LC_label_ui_Triggering_building_3'
    },
    id=10601,
    task_id=2007
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_10',
        'LC_label_ui_Triggering_loving_7'
    },
    id=10602,
    task_id=7012
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_people_5'
    },
    id=10701,
    task_id=2001
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_5',
        'LC_label_ui_Triggering_loving_8'
    },
    id=10702,
    task_id=2010
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_people_6'
    },
    id=10801,
    task_id=6001
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_11',
        'LC_label_ui_Triggering_loving_9',
        'LC_label_ui_Triggering_building_1'
    },
    id=10802,
    task_id=6009
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_sp_1'
    },
    id=10901,
    task_id=7002
})

npc_task_show:upsert({
    conditons={
        'LC_label_ui_Triggering_level_1',
        'LC_label_ui_Triggering_task_3',
        'LC_label_ui_Triggering_building_3',
        'LC_label_ui_Triggering_building_2',
        'LC_label_ui_Triggering_building_5'
    },
    id=10902,
    task_id=5002
})

npc_task_show:update()
