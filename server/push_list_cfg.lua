--push_list_cfg.lua
--source: 17-推送通知配置表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local push_list = config_mgr:get_table("push_list")

--导出配置内容
push_list:upsert({
    id=10000,
    invoke_id=7007,
    push_id=1
})

push_list:upsert({
    id=10001,
    invoke_id=6005,
    push_id=1
})

push_list:upsert({
    id=10002,
    invoke_id=6006,
    push_id=1
})

push_list:upsert({
    id=10003,
    invoke_id=6007,
    push_id=1
})

push_list:upsert({
    id=10004,
    invoke_id=7005,
    push_id=1
})

push_list:upsert({
    id=20000,
    invoke_id=30001,
    push_id=2
})

push_list:upsert({
    id=20001,
    invoke_id=31001,
    push_id=2
})

push_list:upsert({
    id=20002,
    invoke_id=30003,
    push_id=2
})

push_list:upsert({
    id=20003,
    invoke_id=31003,
    push_id=2
})

push_list:upsert({
    id=20004,
    invoke_id=30004,
    push_id=2
})

push_list:upsert({
    id=20005,
    invoke_id=31004,
    push_id=2
})

push_list:upsert({
    id=20006,
    invoke_id=30005,
    push_id=2
})

push_list:upsert({
    id=20007,
    invoke_id=31005,
    push_id=2
})

push_list:upsert({
    id=20008,
    invoke_id=30007,
    push_id=2
})

push_list:upsert({
    id=20009,
    invoke_id=31007,
    push_id=2
})

push_list:upsert({
    id=20010,
    invoke_id=30008,
    push_id=2
})

push_list:upsert({
    id=20011,
    invoke_id=31008,
    push_id=2
})

push_list:upsert({
    id=20012,
    invoke_id=30009,
    push_id=2
})

push_list:upsert({
    id=20013,
    invoke_id=31009,
    push_id=2
})

push_list:upsert({
    id=20014,
    invoke_id=30011,
    push_id=2
})

push_list:upsert({
    id=20015,
    invoke_id=31011,
    push_id=2
})

push_list:upsert({
    id=20016,
    invoke_id=30012,
    push_id=2
})

push_list:upsert({
    id=20017,
    invoke_id=31012,
    push_id=2
})

push_list:update()
