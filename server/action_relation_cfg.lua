--action_relation_cfg.lua
--source: 16_行为关系表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local action_relation = config_mgr:get_table("action_relation")

--导出配置内容
action_relation:upsert({
    id=1,
    object=0,
    relation=0,
    subject=0
})

action_relation:upsert({
    id=2,
    object=1,
    relation=0,
    subject=0
})

action_relation:upsert({
    id=3,
    object=2,
    relation=0,
    subject=0
})

action_relation:upsert({
    id=4,
    object=3,
    relation=0,
    subject=0
})

action_relation:upsert({
    id=5,
    object=4,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=6,
    object=5,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=7,
    object=6,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=8,
    object=7,
    relation=0,
    subject=0
})

action_relation:upsert({
    id=9,
    object=8,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=10,
    object=9,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=11,
    object=10,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=12,
    object=11,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=13,
    object=12,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=14,
    object=13,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=15,
    object=14,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=16,
    object=15,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=17,
    object=16,
    relation=0,
    subject=0
})

action_relation:upsert({
    id=18,
    object=17,
    relation=0,
    subject=0
})

action_relation:upsert({
    id=19,
    object=18,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=20,
    object=19,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=21,
    object=20,
    relation=1,
    subject=0
})

action_relation:upsert({
    id=22,
    object=0,
    relation=0,
    subject=1
})

action_relation:upsert({
    id=23,
    object=1,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=24,
    object=2,
    relation=0,
    subject=1
})

action_relation:upsert({
    id=25,
    object=3,
    relation=0,
    subject=1
})

action_relation:upsert({
    id=26,
    object=4,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=27,
    object=5,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=28,
    object=6,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=29,
    object=7,
    relation=0,
    subject=1
})

action_relation:upsert({
    id=30,
    object=8,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=31,
    object=9,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=32,
    object=10,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=33,
    object=11,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=34,
    object=12,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=35,
    object=13,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=36,
    object=14,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=37,
    object=15,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=38,
    object=16,
    relation=0,
    subject=1
})

action_relation:upsert({
    id=39,
    object=17,
    relation=0,
    subject=1
})

action_relation:upsert({
    id=40,
    object=18,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=41,
    object=19,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=42,
    object=20,
    relation=1,
    subject=1
})

action_relation:upsert({
    id=43,
    object=0,
    relation=0,
    subject=2
})

action_relation:upsert({
    id=44,
    object=1,
    relation=0,
    subject=2
})

action_relation:upsert({
    id=45,
    object=2,
    relation=0,
    subject=2
})

action_relation:upsert({
    id=46,
    object=3,
    relation=0,
    subject=2
})

action_relation:upsert({
    id=47,
    object=4,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=48,
    object=5,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=49,
    object=6,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=50,
    object=7,
    relation=0,
    subject=2
})

action_relation:upsert({
    id=51,
    object=8,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=52,
    object=9,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=53,
    object=10,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=54,
    object=11,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=55,
    object=12,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=56,
    object=13,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=57,
    object=14,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=58,
    object=15,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=59,
    object=16,
    relation=0,
    subject=2
})

action_relation:upsert({
    id=60,
    object=17,
    relation=0,
    subject=2
})

action_relation:upsert({
    id=61,
    object=18,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=62,
    object=19,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=63,
    object=20,
    relation=1,
    subject=2
})

action_relation:upsert({
    id=64,
    object=0,
    relation=0,
    subject=3
})

action_relation:upsert({
    id=65,
    object=1,
    relation=0,
    subject=3
})

action_relation:upsert({
    id=66,
    object=2,
    relation=0,
    subject=3
})

action_relation:upsert({
    id=67,
    object=3,
    relation=0,
    subject=3
})

action_relation:upsert({
    id=68,
    object=4,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=69,
    object=5,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=70,
    object=6,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=71,
    object=7,
    relation=0,
    subject=3
})

action_relation:upsert({
    id=72,
    object=8,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=73,
    object=9,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=74,
    object=10,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=75,
    object=11,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=76,
    object=12,
    relation=3,
    subject=3
})

action_relation:upsert({
    id=77,
    object=13,
    relation=1,
    subject=3
})

action_relation:upsert({
    id=78,
    object=14,
    relation=1,
    subject=3
})

action_relation:upsert({
    id=79,
    object=15,
    relation=1,
    subject=3
})

action_relation:upsert({
    id=80,
    object=16,
    relation=0,
    subject=3
})

action_relation:upsert({
    id=81,
    object=17,
    relation=0,
    subject=3
})

action_relation:upsert({
    id=82,
    object=18,
    relation=1,
    subject=3
})

action_relation:upsert({
    id=83,
    object=19,
    relation=1,
    subject=3
})

action_relation:upsert({
    id=84,
    object=20,
    relation=1,
    subject=3
})

action_relation:upsert({
    id=85,
    object=0,
    relation=3,
    subject=4
})

action_relation:upsert({
    id=86,
    object=1,
    relation=3,
    subject=4
})

action_relation:upsert({
    id=87,
    object=2,
    relation=3,
    subject=4
})

action_relation:upsert({
    id=88,
    object=3,
    relation=0,
    subject=4
})

action_relation:upsert({
    id=89,
    object=4,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=90,
    object=5,
    relation=0,
    subject=4
})

action_relation:upsert({
    id=91,
    object=6,
    relation=0,
    subject=4
})

action_relation:upsert({
    id=92,
    object=7,
    relation=0,
    subject=4
})

action_relation:upsert({
    id=93,
    object=8,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=94,
    object=9,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=95,
    object=10,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=96,
    object=11,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=97,
    object=12,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=98,
    object=13,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=99,
    object=14,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=100,
    object=15,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=101,
    object=16,
    relation=0,
    subject=4
})

action_relation:upsert({
    id=102,
    object=17,
    relation=0,
    subject=4
})

action_relation:upsert({
    id=103,
    object=18,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=104,
    object=19,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=105,
    object=20,
    relation=1,
    subject=4
})

action_relation:upsert({
    id=106,
    object=0,
    relation=3,
    subject=5
})

action_relation:upsert({
    id=107,
    object=1,
    relation=3,
    subject=5
})

action_relation:upsert({
    id=108,
    object=2,
    relation=3,
    subject=5
})

action_relation:upsert({
    id=109,
    object=3,
    relation=0,
    subject=5
})

action_relation:upsert({
    id=110,
    object=4,
    relation=3,
    subject=5
})

action_relation:upsert({
    id=111,
    object=5,
    relation=3,
    subject=5
})

action_relation:upsert({
    id=112,
    object=6,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=113,
    object=7,
    relation=0,
    subject=5
})

action_relation:upsert({
    id=114,
    object=8,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=115,
    object=9,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=116,
    object=10,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=117,
    object=11,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=118,
    object=12,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=119,
    object=13,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=120,
    object=14,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=121,
    object=15,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=122,
    object=16,
    relation=0,
    subject=5
})

action_relation:upsert({
    id=123,
    object=17,
    relation=0,
    subject=5
})

action_relation:upsert({
    id=124,
    object=18,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=125,
    object=19,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=126,
    object=20,
    relation=1,
    subject=5
})

action_relation:upsert({
    id=127,
    object=0,
    relation=3,
    subject=6
})

action_relation:upsert({
    id=128,
    object=1,
    relation=3,
    subject=6
})

action_relation:upsert({
    id=129,
    object=2,
    relation=3,
    subject=6
})

action_relation:upsert({
    id=130,
    object=3,
    relation=0,
    subject=6
})

action_relation:upsert({
    id=131,
    object=4,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=132,
    object=5,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=133,
    object=6,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=134,
    object=7,
    relation=0,
    subject=6
})

action_relation:upsert({
    id=135,
    object=8,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=136,
    object=9,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=137,
    object=10,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=138,
    object=11,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=139,
    object=12,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=140,
    object=13,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=141,
    object=14,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=142,
    object=15,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=143,
    object=16,
    relation=0,
    subject=6
})

action_relation:upsert({
    id=144,
    object=17,
    relation=0,
    subject=6
})

action_relation:upsert({
    id=145,
    object=18,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=146,
    object=19,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=147,
    object=20,
    relation=1,
    subject=6
})

action_relation:upsert({
    id=148,
    object=0,
    relation=3,
    subject=7
})

action_relation:upsert({
    id=149,
    object=1,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=150,
    object=2,
    relation=3,
    subject=7
})

action_relation:upsert({
    id=151,
    object=3,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=152,
    object=4,
    relation=3,
    subject=7
})

action_relation:upsert({
    id=153,
    object=5,
    relation=3,
    subject=7
})

action_relation:upsert({
    id=154,
    object=6,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=155,
    object=7,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=156,
    object=8,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=157,
    object=9,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=158,
    object=10,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=159,
    object=11,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=160,
    object=12,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=161,
    object=13,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=162,
    object=14,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=163,
    object=15,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=164,
    object=16,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=165,
    object=17,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=166,
    object=18,
    relation=0,
    subject=7
})

action_relation:upsert({
    id=167,
    object=19,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=168,
    object=20,
    relation=1,
    subject=7
})

action_relation:upsert({
    id=169,
    object=0,
    relation=3,
    subject=8
})

action_relation:upsert({
    id=170,
    object=1,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=171,
    object=2,
    relation=3,
    subject=8
})

action_relation:upsert({
    id=172,
    object=3,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=173,
    object=4,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=174,
    object=5,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=175,
    object=6,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=176,
    object=7,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=177,
    object=8,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=178,
    object=9,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=179,
    object=10,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=180,
    object=11,
    relation=3,
    subject=8
})

action_relation:upsert({
    id=181,
    object=12,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=182,
    object=13,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=183,
    object=14,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=184,
    object=15,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=185,
    object=16,
    relation=0,
    subject=8
})

action_relation:upsert({
    id=186,
    object=17,
    relation=0,
    subject=8
})

action_relation:upsert({
    id=187,
    object=18,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=188,
    object=19,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=189,
    object=20,
    relation=1,
    subject=8
})

action_relation:upsert({
    id=190,
    object=0,
    relation=3,
    subject=9
})

action_relation:upsert({
    id=191,
    object=1,
    relation=3,
    subject=9
})

action_relation:upsert({
    id=192,
    object=2,
    relation=3,
    subject=9
})

action_relation:upsert({
    id=193,
    object=3,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=194,
    object=4,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=195,
    object=5,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=196,
    object=6,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=197,
    object=7,
    relation=0,
    subject=9
})

action_relation:upsert({
    id=198,
    object=8,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=199,
    object=9,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=200,
    object=10,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=201,
    object=11,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=202,
    object=12,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=203,
    object=13,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=204,
    object=14,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=205,
    object=15,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=206,
    object=16,
    relation=0,
    subject=9
})

action_relation:upsert({
    id=207,
    object=17,
    relation=0,
    subject=9
})

action_relation:upsert({
    id=208,
    object=18,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=209,
    object=19,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=210,
    object=20,
    relation=1,
    subject=9
})

action_relation:upsert({
    id=211,
    object=0,
    relation=3,
    subject=10
})

action_relation:upsert({
    id=212,
    object=1,
    relation=3,
    subject=10
})

action_relation:upsert({
    id=213,
    object=2,
    relation=3,
    subject=10
})

action_relation:upsert({
    id=214,
    object=3,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=215,
    object=4,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=216,
    object=5,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=217,
    object=6,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=218,
    object=7,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=219,
    object=8,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=220,
    object=9,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=221,
    object=10,
    relation=0,
    subject=10
})

action_relation:upsert({
    id=222,
    object=11,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=223,
    object=12,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=224,
    object=13,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=225,
    object=14,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=226,
    object=15,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=227,
    object=16,
    relation=0,
    subject=10
})

action_relation:upsert({
    id=228,
    object=17,
    relation=0,
    subject=10
})

action_relation:upsert({
    id=229,
    object=18,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=230,
    object=19,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=231,
    object=20,
    relation=1,
    subject=10
})

action_relation:upsert({
    id=232,
    object=0,
    relation=3,
    subject=11
})

action_relation:upsert({
    id=233,
    object=1,
    relation=3,
    subject=11
})

action_relation:upsert({
    id=234,
    object=2,
    relation=3,
    subject=11
})

action_relation:upsert({
    id=235,
    object=3,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=236,
    object=4,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=237,
    object=5,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=238,
    object=6,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=239,
    object=7,
    relation=0,
    subject=11
})

action_relation:upsert({
    id=240,
    object=8,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=241,
    object=9,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=242,
    object=10,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=243,
    object=11,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=244,
    object=12,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=245,
    object=13,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=246,
    object=14,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=247,
    object=15,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=248,
    object=16,
    relation=0,
    subject=11
})

action_relation:upsert({
    id=249,
    object=17,
    relation=0,
    subject=11
})

action_relation:upsert({
    id=250,
    object=18,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=251,
    object=19,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=252,
    object=20,
    relation=1,
    subject=11
})

action_relation:upsert({
    id=253,
    object=0,
    relation=3,
    subject=12
})

action_relation:upsert({
    id=254,
    object=1,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=255,
    object=2,
    relation=3,
    subject=12
})

action_relation:upsert({
    id=256,
    object=3,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=257,
    object=4,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=258,
    object=5,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=259,
    object=6,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=260,
    object=7,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=261,
    object=8,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=262,
    object=9,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=263,
    object=10,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=264,
    object=11,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=265,
    object=12,
    relation=0,
    subject=12
})

action_relation:upsert({
    id=266,
    object=13,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=267,
    object=14,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=268,
    object=15,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=269,
    object=16,
    relation=0,
    subject=12
})

action_relation:upsert({
    id=270,
    object=17,
    relation=0,
    subject=12
})

action_relation:upsert({
    id=271,
    object=18,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=272,
    object=19,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=273,
    object=20,
    relation=1,
    subject=12
})

action_relation:upsert({
    id=274,
    object=0,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=275,
    object=1,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=276,
    object=2,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=277,
    object=3,
    relation=1,
    subject=13
})

action_relation:upsert({
    id=278,
    object=4,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=279,
    object=5,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=280,
    object=6,
    relation=0,
    subject=13
})

action_relation:upsert({
    id=281,
    object=7,
    relation=0,
    subject=13
})

action_relation:upsert({
    id=282,
    object=8,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=283,
    object=9,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=284,
    object=10,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=285,
    object=11,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=286,
    object=12,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=287,
    object=13,
    relation=1,
    subject=13
})

action_relation:upsert({
    id=288,
    object=14,
    relation=3,
    subject=13
})

action_relation:upsert({
    id=289,
    object=15,
    relation=1,
    subject=13
})

action_relation:upsert({
    id=290,
    object=16,
    relation=0,
    subject=13
})

action_relation:upsert({
    id=291,
    object=17,
    relation=0,
    subject=13
})

action_relation:upsert({
    id=292,
    object=18,
    relation=1,
    subject=13
})

action_relation:upsert({
    id=293,
    object=19,
    relation=1,
    subject=13
})

action_relation:upsert({
    id=294,
    object=20,
    relation=1,
    subject=13
})

action_relation:upsert({
    id=295,
    object=0,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=296,
    object=1,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=297,
    object=2,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=298,
    object=3,
    relation=1,
    subject=14
})

action_relation:upsert({
    id=299,
    object=4,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=300,
    object=5,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=301,
    object=6,
    relation=0,
    subject=14
})

action_relation:upsert({
    id=302,
    object=7,
    relation=0,
    subject=14
})

action_relation:upsert({
    id=303,
    object=8,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=304,
    object=9,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=305,
    object=10,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=306,
    object=11,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=307,
    object=12,
    relation=3,
    subject=14
})

action_relation:upsert({
    id=308,
    object=13,
    relation=1,
    subject=14
})

action_relation:upsert({
    id=309,
    object=14,
    relation=0,
    subject=14
})

action_relation:upsert({
    id=310,
    object=15,
    relation=1,
    subject=14
})

action_relation:upsert({
    id=311,
    object=16,
    relation=0,
    subject=14
})

action_relation:upsert({
    id=312,
    object=17,
    relation=0,
    subject=14
})

action_relation:upsert({
    id=313,
    object=18,
    relation=1,
    subject=14
})

action_relation:upsert({
    id=314,
    object=19,
    relation=1,
    subject=14
})

action_relation:upsert({
    id=315,
    object=20,
    relation=1,
    subject=14
})

action_relation:upsert({
    id=316,
    object=0,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=317,
    object=1,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=318,
    object=2,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=319,
    object=3,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=320,
    object=4,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=321,
    object=5,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=322,
    object=6,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=323,
    object=7,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=324,
    object=8,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=325,
    object=9,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=326,
    object=10,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=327,
    object=11,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=328,
    object=12,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=329,
    object=13,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=330,
    object=14,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=331,
    object=15,
    relation=1,
    subject=15
})

action_relation:upsert({
    id=332,
    object=16,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=333,
    object=17,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=334,
    object=18,
    relation=3,
    subject=15
})

action_relation:upsert({
    id=335,
    object=19,
    relation=1,
    subject=15
})

action_relation:upsert({
    id=336,
    object=20,
    relation=1,
    subject=15
})

action_relation:upsert({
    id=337,
    object=0,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=338,
    object=1,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=339,
    object=2,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=340,
    object=3,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=341,
    object=4,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=342,
    object=5,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=343,
    object=6,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=344,
    object=7,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=345,
    object=8,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=346,
    object=9,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=347,
    object=10,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=348,
    object=11,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=349,
    object=12,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=350,
    object=13,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=351,
    object=14,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=352,
    object=15,
    relation=1,
    subject=16
})

action_relation:upsert({
    id=353,
    object=16,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=354,
    object=17,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=355,
    object=18,
    relation=0,
    subject=16
})

action_relation:upsert({
    id=356,
    object=19,
    relation=1,
    subject=16
})

action_relation:upsert({
    id=357,
    object=20,
    relation=1,
    subject=16
})

action_relation:upsert({
    id=358,
    object=0,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=359,
    object=1,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=360,
    object=2,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=361,
    object=3,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=362,
    object=4,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=363,
    object=5,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=364,
    object=6,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=365,
    object=7,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=366,
    object=8,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=367,
    object=9,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=368,
    object=10,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=369,
    object=11,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=370,
    object=12,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=371,
    object=13,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=372,
    object=14,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=373,
    object=15,
    relation=1,
    subject=17
})

action_relation:upsert({
    id=374,
    object=16,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=375,
    object=17,
    relation=3,
    subject=17
})

action_relation:upsert({
    id=376,
    object=18,
    relation=0,
    subject=17
})

action_relation:upsert({
    id=377,
    object=19,
    relation=1,
    subject=17
})

action_relation:upsert({
    id=378,
    object=20,
    relation=1,
    subject=17
})

action_relation:upsert({
    id=379,
    object=0,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=380,
    object=1,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=381,
    object=2,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=382,
    object=3,
    relation=1,
    subject=18
})

action_relation:upsert({
    id=383,
    object=4,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=384,
    object=5,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=385,
    object=6,
    relation=0,
    subject=18
})

action_relation:upsert({
    id=386,
    object=7,
    relation=0,
    subject=18
})

action_relation:upsert({
    id=387,
    object=8,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=388,
    object=9,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=389,
    object=10,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=390,
    object=11,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=391,
    object=12,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=392,
    object=13,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=393,
    object=14,
    relation=3,
    subject=18
})

action_relation:upsert({
    id=394,
    object=15,
    relation=1,
    subject=18
})

action_relation:upsert({
    id=395,
    object=16,
    relation=0,
    subject=18
})

action_relation:upsert({
    id=396,
    object=17,
    relation=0,
    subject=18
})

action_relation:upsert({
    id=397,
    object=18,
    relation=1,
    subject=18
})

action_relation:upsert({
    id=398,
    object=19,
    relation=1,
    subject=18
})

action_relation:upsert({
    id=399,
    object=20,
    relation=1,
    subject=18
})

action_relation:upsert({
    id=400,
    object=0,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=401,
    object=1,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=402,
    object=2,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=403,
    object=3,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=404,
    object=4,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=405,
    object=5,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=406,
    object=6,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=407,
    object=7,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=408,
    object=8,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=409,
    object=9,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=410,
    object=10,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=411,
    object=11,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=412,
    object=12,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=413,
    object=13,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=414,
    object=14,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=415,
    object=15,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=416,
    object=16,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=417,
    object=17,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=418,
    object=18,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=419,
    object=19,
    relation=1,
    subject=19
})

action_relation:upsert({
    id=420,
    object=20,
    relation=3,
    subject=19
})

action_relation:upsert({
    id=421,
    object=0,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=422,
    object=1,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=423,
    object=2,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=424,
    object=3,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=425,
    object=4,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=426,
    object=5,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=427,
    object=6,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=428,
    object=7,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=429,
    object=8,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=430,
    object=9,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=431,
    object=10,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=432,
    object=11,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=433,
    object=12,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=434,
    object=13,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=435,
    object=14,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=436,
    object=15,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=437,
    object=16,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=438,
    object=17,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=439,
    object=18,
    relation=3,
    subject=20
})

action_relation:upsert({
    id=440,
    object=19,
    relation=1,
    subject=20
})

action_relation:upsert({
    id=441,
    object=20,
    relation=1,
    subject=20
})

action_relation:update()
