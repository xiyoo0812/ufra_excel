--behavior_cfg.lua
--source: 5_behavior_任务目标行为.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local behavior = config_mgr:get_table("behavior")

--导出配置内容
behavior:upsert({
    enum_key='TALK',
    id=1,
    name='对话',
    type=1
})

behavior:upsert({
    enum_key='PLACE',
    id=2,
    name='摆放',
    type=1
})

behavior:upsert({
    enum_key='GATHER',
    id=3,
    name='采集',
    type=1
})

behavior:upsert({
    enum_key='MAKE',
    id=4,
    name='制作',
    type=1
})

behavior:upsert({
    enum_key='COLLECT',
    id=5,
    name='收集',
    type=1
})

behavior:upsert({
    enum_key='AREA',
    id=6,
    name='目标区域',
    type=1
})

behavior:upsert({
    enum_key='COMMIT',
    id=7,
    name='提交道具',
    type=1
})

behavior:upsert({
    enum_key='COST',
    id=8,
    name='消耗道具',
    type=1
})

behavior:upsert({
    enum_key='GROUND',
    id=9,
    name='解锁地皮',
    type=1
})

behavior:upsert({
    enum_key='TASK',
    id=10,
    name='完成任务',
    type=1
})

behavior:upsert({
    enum_key='VIRTUAL',
    id=11,
    name='虚拟目标',
    type=1
})

behavior:upsert({
    enum_key='LETTER',
    id=12,
    name='信件阅读',
    type=1
})

behavior:upsert({
    enum_key='TIMER',
    id=13,
    name='定时器',
    type=1
})

behavior:upsert({
    enum_key='BUILDING',
    id=14,
    name='完成建筑',
    type=1
})

behavior:upsert({
    enum_key='ASSIGN',
    id=15,
    name='派驻',
    type=1
})

behavior:upsert({
    enum_key='ACCEPT',
    id=16,
    name='接受任务',
    type=1
})

behavior:upsert({
    enum_key='FITUP',
    id=17,
    name='装修',
    type=1
})

behavior:upsert({
    enum_key='LOGIN',
    id=18,
    name='登录游戏',
    type=1
})

behavior:upsert({
    enum_key='ENERGY',
    id=19,
    name='消耗体力',
    type=1
})

behavior:upsert({
    enum_key='RECRUIT',
    id=20,
    name='招募npc',
    type=1
})

behavior:upsert({
    enum_key='BUYITEM',
    id=21,
    name='购买物品',
    type=1
})

behavior:upsert({
    enum_key='PARTNER',
    id=22,
    name='接收城镇npc',
    type=1
})

behavior:upsert({
    enum_key='UP_LEVEL',
    id=23,
    name='玩家升级',
    type=1
})

behavior:upsert({
    enum_key='GAME_DAY',
    id=24,
    name='游戏跨天',
    type=1
})

behavior:upsert({
    enum_key='READ_EMAIL',
    id=25,
    name='阅读邮件',
    type=1
})

behavior:upsert({
    enum_key='ENTER',
    id=26,
    name='进入场景',
    type=1
})

behavior:upsert({
    enum_key='OS_DAY',
    id=27,
    name='自然日跨天',
    type=1
})

behavior:upsert({
    enum_key='BLOCK',
    id=28,
    name='地快清理',
    type=1
})

behavior:upsert({
    enum_key='COMMITS',
    id=29,
    name='提交一组物品',
    type=1
})

behavior:upsert({
    enum_key='TASKS',
    id=30,
    name='完成多个多种类型任务',
    type=1
})

behavior:upsert({
    enum_key='FIND_BUILDING',
    id=31,
    name='完成建筑查历史',
    type=1
})

behavior:upsert({
    enum_key='HUNGRY_RESUME',
    id=32,
    name='恢复饥饿值',
    type=1
})

behavior:upsert({
    enum_key='PRODUCTION',
    id=33,
    name='在建筑上完成生产',
    type=1
})

behavior:upsert({
    enum_key='TOWN_LEVEL',
    id=34,
    name='城镇等级',
    type=1
})

behavior:upsert({
    enum_key='WEAR_EQUIP',
    id=35,
    name='装备某件装备',
    type=1
})

behavior:upsert({
    enum_key='SALE_RESOURCE',
    id=36,
    name='在回收箱售卖资源',
    type=1
})

behavior:upsert({
    enum_key='NPC_CHECK_IN',
    id=37,
    name='NPC入住',
    type=1
})

behavior:upsert({
    enum_key='FINISH_DUNGEON',
    id=38,
    name='完成副本',
    type=1
})

behavior:upsert({
    enum_key='KILL_MONSTER',
    id=39,
    name='杀害怪物',
    type=1
})

behavior:upsert({
    enum_key='DONE_FISH',
    id=40,
    name='完成一次钓鱼',
    type=1
})

behavior:upsert({
    enum_key='RESTORE',
    id=41,
    name='修复物品',
    type=1
})

behavior:upsert({
    enum_key='NPC_GIFT',
    id=42,
    name='给指定npc送礼',
    type=1
})

behavior:upsert({
    enum_key='BUILDING_LEVEL',
    id=43,
    name='拥有建筑等级',
    type=1
})

behavior:upsert({
    enum_key='RESIDENT_NUM',
    id=44,
    name='居民数量',
    type=1
})

behavior:upsert({
    enum_key='ITEM',
    id=201,
    name='发放道具',
    type=2
})

behavior:upsert({
    enum_key='DROP',
    id=202,
    name='掉落道具',
    type=2
})

behavior:upsert({
    enum_key='STORY',
    id=203,
    name='发放任务',
    type=2
})

behavior:upsert({
    enum_key='EMAIL',
    id=204,
    name='发放剧情邮件',
    type=2
})

behavior:update()
