--chat_template_cfg.lua
--source: 15-chat_template_聊天模版表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local chat_template = config_mgr:get_table("chat_template")

--导出配置内容
chat_template:upsert({
    id=100000
})

chat_template:upsert({
    id=100050
})

chat_template:upsert({
    id=100051
})

chat_template:upsert({
    id=100052
})

chat_template:upsert({
    id=100100
})

chat_template:upsert({
    id=100150
})

chat_template:update()
