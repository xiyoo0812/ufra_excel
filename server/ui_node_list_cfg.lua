--ui_node_list_cfg.lua
--source: 0_ui_node_list_UI红点节点配置表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local ui_node_list = config_mgr:get_table("ui_node_list")

--导出配置内容
ui_node_list:upsert({
    amount=0,
    enum_key='UINI_HALL_MEMU',
    id=1000
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_MEMU_FIGURE',
    id=1001
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_FIGURE_INFO_ROLE',
    id=1002
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_MEMU_EMAIL',
    id=1050,
    type=2
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_EMAIL_INBOX',
    id=1051,
    type=2
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_EMAIl_COLLECT',
    id=1052,
    type=2
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_MEMU_PACKET',
    id=1100
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_FIGURE_INFO_PACKET',
    id=1101
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_PACKET_ALL',
    id=1102
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_PACKET_MATERIAL',
    id=1103
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_PACKET_EQUIP',
    id=1104
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_MEMU_CUSTOMER',
    id=1150
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_MEMU_SETTING',
    id=1200
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_ACCOUNT_SETTINGS',
    id=1201
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_GAME_SETTINGS',
    id=1202
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_IMAGE_SETTINGS',
    id=1203
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_SOUND_SETTINGS',
    id=1204
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_OPERATE_SETTINGS',
    id=1205
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_MEMU_RETURN',
    id=1250
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_TASK_TRACK',
    id=1300
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_FAST_MEMU',
    id=1350
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_NOVICE_SIGN_IN',
    id=1351,
    type=1
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_DIS_CORD',
    id=1352
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_DAILY_TASK',
    id=1400,
    type=1
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_DAILY_TASK_LIST',
    id=1401
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_PACKBENCH',
    id=1410
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_PACKBENCH_BUILD',
    id=1411,
    type=6
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_PACKBENCH_TOOLS',
    id=1412,
    type=6
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_ACTY',
    id=1450,
    type=1
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_ACTY_LIST',
    id=1451,
    type=1
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_ACTY_ENERGY',
    id=1452,
    type=1
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_ACTY_ENERGY',
    id=1453,
    type=1
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_HALL_CHMN',
    id=1500
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_CHMN_MANL',
    id=1501
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINI_CHMN_TASK',
    id=1502
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_QUESTION_SURVER',
    id=1503
})

ui_node_list:upsert({
    amount=0,
    enum_key='UINT_FRIEDN',
    id=1550
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1001_1601',
    id=1601,
    type=8
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1001_1602',
    id=1602,
    type=8
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1001_1603',
    id=1603,
    type=8
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1001_1604',
    id=1604,
    type=8
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1001_1605',
    id=1605,
    type=8
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1001_1606',
    id=1606,
    type=8
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1001_1607',
    id=1607,
    type=8
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_1650',
    id=1650
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1650_1651',
    id=1651
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1350_1700',
    id=1700
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_1701',
    id=1701
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1701_1702',
    id=1702
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1701_1703',
    id=1703
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1701_1704',
    id=1704
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1704_1705',
    id=1705
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1704_1706',
    id=1706
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1701_1707',
    id=1707
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_1708',
    id=1708
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1701_1709',
    id=1709
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_1800',
    id=1800,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1800_1801',
    id=1801,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_1900',
    id=1900,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1900_1901',
    id=1901,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_2000',
    id=2000,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_2000_2001',
    id=2001,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_2100',
    id=2100,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_2100_2101',
    id=2101,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_0_2200',
    id=2200,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_2200_2201',
    id=2201,
    type=5
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1350_2300',
    id=2300
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_2300_2301',
    id=2301
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_2300_2302',
    id=2302
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_2300_2303',
    id=2303
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1100_2400',
    id=2400,
    type=7
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1100_2401',
    id=2401,
    type=7
})

ui_node_list:upsert({
    amount=1,
    enum_key='UINT_1100_2402',
    id=2402,
    type=7
})

ui_node_list:update()
