--loop_dailycount_cfg.lua
--source: 10_loop_dailyCount_订单每日刷新时间.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local loop_dailycount = config_mgr:get_table("loop_dailycount")

--导出配置内容
loop_dailycount:upsert({
    finish_count=1,
    id=1,
    refresh_time=600,
    type=7
})

loop_dailycount:upsert({
    finish_count=2,
    id=2,
    refresh_time=600,
    type=7
})

loop_dailycount:upsert({
    finish_count=3,
    id=3,
    refresh_time=600,
    type=7
})

loop_dailycount:upsert({
    finish_count=4,
    id=4,
    refresh_time=600,
    type=7
})

loop_dailycount:upsert({
    finish_count=5,
    id=5,
    refresh_time=600,
    type=7
})

loop_dailycount:upsert({
    finish_count=6,
    id=6,
    refresh_time=660,
    type=7
})

loop_dailycount:upsert({
    finish_count=7,
    id=7,
    refresh_time=660,
    type=7
})

loop_dailycount:upsert({
    finish_count=8,
    id=8,
    refresh_time=660,
    type=7
})

loop_dailycount:upsert({
    finish_count=9,
    id=9,
    refresh_time=660,
    type=7
})

loop_dailycount:upsert({
    finish_count=10,
    id=10,
    refresh_time=660,
    type=7
})

loop_dailycount:upsert({
    finish_count=11,
    id=11,
    refresh_time=720,
    type=7
})

loop_dailycount:upsert({
    finish_count=12,
    id=12,
    refresh_time=720,
    type=7
})

loop_dailycount:upsert({
    finish_count=13,
    id=13,
    refresh_time=720,
    type=7
})

loop_dailycount:upsert({
    finish_count=14,
    id=14,
    refresh_time=720,
    type=7
})

loop_dailycount:upsert({
    finish_count=15,
    id=15,
    refresh_time=720,
    type=7
})

loop_dailycount:upsert({
    finish_count=16,
    id=16,
    refresh_time=780,
    type=7
})

loop_dailycount:upsert({
    finish_count=17,
    id=17,
    refresh_time=780,
    type=7
})

loop_dailycount:upsert({
    finish_count=18,
    id=18,
    refresh_time=780,
    type=7
})

loop_dailycount:upsert({
    finish_count=19,
    id=19,
    refresh_time=780,
    type=7
})

loop_dailycount:upsert({
    finish_count=20,
    id=20,
    refresh_time=780,
    type=7
})

loop_dailycount:upsert({
    finish_count=21,
    id=21,
    refresh_time=840,
    type=7
})

loop_dailycount:upsert({
    finish_count=22,
    id=22,
    refresh_time=840,
    type=7
})

loop_dailycount:upsert({
    finish_count=23,
    id=23,
    refresh_time=840,
    type=7
})

loop_dailycount:upsert({
    finish_count=24,
    id=24,
    refresh_time=840,
    type=7
})

loop_dailycount:upsert({
    finish_count=25,
    id=25,
    refresh_time=840,
    type=7
})

loop_dailycount:upsert({
    finish_count=26,
    id=26,
    refresh_time=900,
    type=7
})

loop_dailycount:upsert({
    finish_count=27,
    id=27,
    refresh_time=900,
    type=7
})

loop_dailycount:upsert({
    finish_count=28,
    id=28,
    refresh_time=900,
    type=7
})

loop_dailycount:upsert({
    finish_count=29,
    id=29,
    refresh_time=900,
    type=7
})

loop_dailycount:upsert({
    finish_count=30,
    id=30,
    refresh_time=900,
    type=7
})

loop_dailycount:upsert({
    finish_count=1,
    id=31,
    refresh_time=1200,
    type=8
})

loop_dailycount:upsert({
    finish_count=2,
    id=32,
    refresh_time=1500,
    type=8
})

loop_dailycount:upsert({
    finish_count=3,
    id=33,
    refresh_time=1800,
    type=8
})

loop_dailycount:upsert({
    finish_count=4,
    id=34,
    refresh_time=3600,
    type=8
})

loop_dailycount:upsert({
    finish_count=5,
    id=35,
    refresh_time=5400,
    type=8
})

loop_dailycount:upsert({
    finish_count=6,
    id=36,
    refresh_time=7200,
    type=8
})

loop_dailycount:upsert({
    finish_count=7,
    id=37,
    refresh_time=7200,
    type=8
})

loop_dailycount:upsert({
    finish_count=8,
    id=38,
    refresh_time=7200,
    type=8
})

loop_dailycount:upsert({
    finish_count=9,
    id=39,
    refresh_time=7200,
    type=8
})

loop_dailycount:upsert({
    finish_count=10,
    id=40,
    refresh_time=7200,
    type=8
})

loop_dailycount:update()
