--scene_effect_cfg.lua
--source: 8_scene_effect_场景效果表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local scene_effect = config_mgr:get_table("scene_effect")

--导出配置内容
scene_effect:upsert({
    effect_ids={
        31000015
    },
    id=60001,
    interaction_icon='UI_Main_Hoe_Img',
    interval=2,
    level=1,
    name='LC_resource_name_covery_hp',
    radius=300,
    resname='Nature_Rock_1_Pre',
    type=1
})

scene_effect:upsert({
    effect_ids={
        31000016
    },
    id=60002,
    interaction_icon='UI_Main_Hoe_Img',
    level=1,
    name='LC_resource_name_covery_pack',
    radius=300,
    resname='Nature_Rock_1_Pre',
    type=2
})

scene_effect:update()
