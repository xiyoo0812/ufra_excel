--town_sat_reward_cfg.lua
--source: 6_town_sat_城镇满意度.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local town_sat_reward = config_mgr:get_table("town_sat_reward")

--导出配置内容
town_sat_reward:upsert({
    arg1=100,
    id=101,
    name='LC_label_ui_Satisfaction_reward2',
    rewards={
        [130002]=10
    },
    type=1
})

town_sat_reward:upsert({
    arg1=60,
    arg2=12,
    id=102,
    name='LC_label_ui_Satisfaction_reward1',
    rewards={
        [103027]=10
    },
    type=2
})

town_sat_reward:update()
