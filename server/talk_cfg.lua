--talk_cfg.lua
--source: 5_talk_非任务对话组表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local talk = config_mgr:get_table("talk")

--导出配置内容
talk:upsert({
    args={
        '101602'
    },
    hold_npc=20001,
    id=1000001,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20002,
    id=1000002,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20003,
    id=1000003,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20004,
    id=1000004,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20010,
    id=1000010,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20001,
    id=1100001,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20002,
    id=1100002,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20003,
    id=1100003,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20004,
    id=1100004,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101602'
    },
    hold_npc=20010,
    id=1100010,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101802'
    },
    hold_npc=20001,
    id=1200001,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101802'
    },
    hold_npc=20002,
    id=1200002,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101802'
    },
    hold_npc=20003,
    id=1200003,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101802'
    },
    hold_npc=20004,
    id=1200004,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '101802'
    },
    hold_npc=20010,
    id=1200010,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '102608'
    },
    hold_npc=20001,
    id=1300001,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '102608'
    },
    hold_npc=20002,
    id=1300002,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '102608'
    },
    hold_npc=20003,
    id=1300003,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '102608'
    },
    hold_npc=20004,
    id=1300004,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '102608'
    },
    hold_npc=20010,
    id=1300010,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340202'
    },
    hold_npc=20001,
    id=1400001,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340202'
    },
    hold_npc=20002,
    id=1400002,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340202'
    },
    hold_npc=20003,
    id=1400003,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340202'
    },
    hold_npc=20004,
    id=1400004,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340202'
    },
    hold_npc=20010,
    id=1400010,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340302'
    },
    hold_npc=20001,
    id=1500001,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340302'
    },
    hold_npc=20002,
    id=1500002,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340302'
    },
    hold_npc=20003,
    id=1500003,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340302'
    },
    hold_npc=20004,
    id=1500004,
    mode=1,
    type=1
})

talk:upsert({
    args={
        '340302'
    },
    hold_npc=20010,
    id=1500010,
    mode=1,
    type=1
})

talk:update()
