--head_cfg.lua
--source: 0_head_sculpture头像及标识.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local head = config_mgr:get_table("head")

--导出配置内容
head:upsert({
    id=1001
})

head:upsert({
    id=1002
})

head:update()
