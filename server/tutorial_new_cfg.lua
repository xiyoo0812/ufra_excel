--tutorial_new_cfg.lua
--source: 0_tutorial_new_新手引导对白表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local tutorial_new = config_mgr:get_table("tutorial_new")

--导出配置内容
tutorial_new:upsert({
    id=10000
})

tutorial_new:upsert({
    id=10001
})

tutorial_new:upsert({
    id=10002
})

tutorial_new:upsert({
    id=10003
})

tutorial_new:upsert({
    id=10004
})

tutorial_new:upsert({
    id=20000
})

tutorial_new:upsert({
    id=20001
})

tutorial_new:upsert({
    id=20002
})

tutorial_new:upsert({
    id=20003
})

tutorial_new:upsert({
    id=20004
})

tutorial_new:upsert({
    id=20005
})

tutorial_new:upsert({
    id=20006
})

tutorial_new:upsert({
    id=20007
})

tutorial_new:upsert({
    id=20008
})

tutorial_new:upsert({
    id=20009
})

tutorial_new:upsert({
    id=20010
})

tutorial_new:upsert({
    id=20011
})

tutorial_new:upsert({
    id=30000
})

tutorial_new:upsert({
    id=30001
})

tutorial_new:upsert({
    id=30002
})

tutorial_new:upsert({
    id=30003
})

tutorial_new:upsert({
    id=30004
})

tutorial_new:upsert({
    id=30005
})

tutorial_new:upsert({
    id=30006
})

tutorial_new:upsert({
    id=30007
})

tutorial_new:upsert({
    id=40000
})

tutorial_new:upsert({
    id=40001
})

tutorial_new:upsert({
    id=40002
})

tutorial_new:upsert({
    id=40003
})

tutorial_new:upsert({
    id=40004
})

tutorial_new:upsert({
    id=40005
})

tutorial_new:upsert({
    id=40006
})

tutorial_new:upsert({
    id=50000
})

tutorial_new:upsert({
    id=50001
})

tutorial_new:upsert({
    id=60000
})

tutorial_new:upsert({
    id=60001
})

tutorial_new:upsert({
    id=60002
})

tutorial_new:upsert({
    id=70000
})

tutorial_new:upsert({
    id=70001
})

tutorial_new:upsert({
    id=70002
})

tutorial_new:update()
