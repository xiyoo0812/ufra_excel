--mail_cfg.lua
--source: 邮件配置表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local mail = config_mgr:get_table("mail")

--导出配置内容
mail:upsert({
    attaches={
        [100001]=50,
        [100004]=50
    },
    content='LC_label_ui_Mail_GM_100101_Text',
    content_params={
        'player_name',
        'other_info'
    },
    expire=1209600,
    from='LC_label_ui_Mail_GM_100101_Sender',
    from_avatar='UI_Machine_Creation_1_Img',
    id=100101,
    source=3,
    title='LC_label_ui_Mail_GM_100101_Title',
    title_params={
        'player_name',
        'other_info'
    }
})

mail:upsert({
    content='LC_mail_content_100102',
    expire=1209600,
    from='LC_mail_from_100102',
    id=100102,
    source=3,
    title='LC_mail_title_100102'
})

mail:upsert({
    content='LC_mail_content_100103',
    expire=1209600,
    from='LC_mail_from_100103',
    id=100103,
    source=3,
    title='LC_mail_title_100103'
})

mail:upsert({
    content='LC_dialogue_topdesc_1004',
    expire=0,
    from='LC_dialogue_npc_20004',
    id=100104,
    source=3,
    title='LC_dialogue_toptitle_1003'
})

mail:upsert({
    attaches={
        [100001]=300
    },
    content='LC_mail_content_100105',
    expire=1209600,
    from='LC_mail_from_100104',
    id=100105,
    source=3,
    title='LC_mail_title_100105'
})

mail:upsert({
    attaches={
        [190004]=1
    },
    content='LC_mail_content_100106',
    expire=1209600,
    from='LC_mail_from_100106',
    id=100106,
    source=3,
    title='LC_mail_title_100106'
})

mail:upsert({
    content='LC_mail_content_100107',
    expire=1209600,
    from='LC_mail_from_100107',
    id=100107,
    source=3,
    title='LC_mail_title_100107'
})

mail:upsert({
    content='LC_mail_content_100108',
    expire=1209600,
    from='LC_mail_from_100108',
    id=100108,
    source=3,
    title='LC_mail_title_100108'
})

mail:upsert({
    content='LC_mail_content_100203',
    expire=1209600,
    from='LC_mail_from_100203',
    id=100203,
    source=3,
    title='LC_mail_title_100203'
})

mail:upsert({
    attaches={
        [104004]=1
    },
    content='LC_mail_content_100204',
    expire=1209600,
    from='LC_mail_from_100204',
    id=100204,
    source=3,
    title='LC_mail_title_100204'
})

mail:upsert({
    attaches={
        [103019]=1
    },
    content='LC_mail_content_100205',
    expire=1209600,
    from='LC_mail_from_100205',
    id=100205,
    source=3,
    title='LC_mail_title_100205'
})

mail:upsert({
    content='LC_mail_content_700101',
    expire=1209600,
    from='LC_mail_from_700101',
    id=100206,
    source=3,
    title='LC_mail_title_700101'
})

mail:upsert({
    attaches={
        [103027]=2
    },
    content='LC_mail_content_300501',
    expire=1209600,
    from='LC_mail_from_300501',
    id=100207,
    source=3,
    title='LC_mail_title_300501'
})

mail:upsert({
    content='LC_mail_content_500201',
    expire=1209600,
    from='LC_mail_from_500201',
    id=100208,
    source=3,
    title='LC_mail_title_500201'
})

mail:upsert({
    content='LC_mail_content_4002',
    expire=1209600,
    from='LC_mail_from_4002',
    id=102011,
    source=3,
    title='LC_mail_title_4002'
})

mail:upsert({
    attaches={
        [100001]=300,
        [101103]=1,
        [104502]=2
    },
    content='LC_mail_content_4001',
    expire=1209600,
    from='LC_mail_from_4001',
    id=102012,
    source=3,
    title='LC_mail_title_4001'
})

mail:upsert({
    attaches={
        [100001]=1000,
        [103027]=2,
        [104501]=5,
        [109003]=10
    },
    content='LC_mail_content_4003',
    expire=1209600,
    from='LC_mail_from_4003',
    id=102013,
    source=3,
    title='LC_mail_title_4003'
})

mail:upsert({
    attaches={
        [201009]=1,
        [204009]=1
    },
    content='LC_mail_content_4004',
    expire=1209600,
    from='LC_mail_from_4004',
    id=102014,
    source=3,
    title='LC_mail_title_4004'
})

mail:upsert({
    content='LC_mail_content_4005',
    expire=1209600,
    from='LC_mail_from_4005',
    id=102015,
    source=3,
    title='LC_mail_title_4005'
})

mail:update()
