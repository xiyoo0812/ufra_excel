--fuel_cfg.lua
--source: 11_fuel_燃料表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local fuel = config_mgr:get_table("fuel")

--导出配置内容
fuel:upsert({
    id=1,
    item_id=100101,
    value=50
})

fuel:upsert({
    id=2,
    item_id=100102,
    value=200
})

fuel:update()
