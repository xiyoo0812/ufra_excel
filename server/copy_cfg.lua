--copy_cfg.lua
--source: 9_copy_副本表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local copy = config_mgr:get_table("copy")

--导出配置内容
copy:upsert({
    duration=259200,
    enter_dir={
        0,
        9316,
        0
    },
    enter_pos={
        -2571,
        1893,
        207
    },
    exit_dir={
        0,
        19904,
        0
    },
    exit_map=1003,
    exit_pos={
        147186,
        11302,
        73796
    },
    exit_timeout=60,
    id=1,
    map_id=1011,
    name='剧情副本1',
    type=1
})

copy:upsert({
    duration=259200,
    enter_dir={
        0,
        30141,
        0
    },
    enter_pos={
        7835,
        3991,
        8888
    },
    exit_dir={
        0,
        11494,
        0
    },
    exit_map=1003,
    exit_pos={
        145317,
        16613,
        103528
    },
    exit_timeout=0,
    id=2,
    map_id=1012,
    name='新手副本',
    type=1
})

copy:update()
