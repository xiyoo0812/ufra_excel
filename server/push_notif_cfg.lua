--push_notif_cfg.lua
--source: 17-推送通知配置表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local push_notif = config_mgr:get_table("push_notif")

--导出配置内容
push_notif:upsert({
    client=true,
    content='LC_label_ui_Push_Notif_100001',
    end_time=4102452000,
    id=100001,
    kind=0,
    push_time='09:00:00',
    start_time=1577844000,
    title='LC_label_ui_Push_notif_reward_title',
    type=0
})

push_notif:upsert({
    client=true,
    content='LC_label_ui_Push_Notif_100001',
    end_time=4102452000,
    id=100002,
    kind=0,
    push_time='18:00:00',
    start_time=1577844000,
    title='LC_label_ui_Push_notif_reward_title',
    type=0
})

push_notif:upsert({
    client=true,
    content='LC_label_ui_Push_Notif_100100',
    end_time=4102452000,
    id=100100,
    kind=1,
    push_time='20:00:00',
    start_time=1577844000,
    title='LC_label_ui_Push_notif_reward_title',
    type=1
})

push_notif:upsert({
    client=true,
    content='LC_label_ui_Push_Notif_100200',
    end_time=1706533200,
    id=100200,
    kind=0,
    push_time='2024/1/26 19:50:00',
    start_time=1706274000,
    title='LC_label_ui_Push_notif_activity_title',
    type=1
})

push_notif:upsert({
    client=true,
    content='LC_label_ui_Push_notif_activity_feedbacktext',
    end_time=1706371200,
    id=100300,
    kind=1,
    push_time='0:00:00',
    start_time=1706198400,
    title='LC_label_ui_Push_notif_activity_feedbacktitle',
    type=1
})

push_notif:upsert({
    client=true,
    content='LC_label_ui_Push_notif_activity_feedbacktext',
    end_time=1706371200,
    id=100301,
    kind=1,
    push_time='12:00:00',
    start_time=1706198400,
    title='LC_label_ui_Push_notif_activity_feedbacktitle',
    type=1
})

push_notif:upsert({
    client=true,
    content='LC_label_ui_Push_notif_activity_feedbacktext',
    end_time=1706371200,
    id=100302,
    kind=1,
    push_time='18:00:00',
    start_time=1706198400,
    title='LC_label_ui_Push_notif_activity_feedbacktitle',
    type=1
})

push_notif:update()
