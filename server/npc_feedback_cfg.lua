--npc_feedback_cfg.lua
--source: 3_npc_feedback_NPC送礼反馈.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local npc_feedback = config_mgr:get_table("npc_feedback")

--导出配置内容
npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620001',
    dialog2='999952020001',
    id=20001
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620002',
    dialog2='999952020002',
    id=20002
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620003',
    dialog2='999952020003',
    id=20003
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620004',
    dialog2='999952020004',
    id=20004
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620007',
    dialog2='999952020007',
    id=20007
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620008',
    dialog2='999952020008',
    id=20008
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620009',
    dialog2='999952020009',
    id=20009
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620010',
    dialog2='999952020010',
    id=20010
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620011',
    dialog2='999952020011',
    id=20011
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621214',
    dialog2='999952020214',
    id=20014
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621014',
    dialog2='999952020014',
    id=20015
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621114',
    dialog2='999952020114',
    id=20016
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621214',
    dialog2='999952020214',
    id=20017
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621115',
    dialog2='999952020116',
    id=20018
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621115',
    dialog2='999952020116',
    id=20019
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621115',
    dialog2='999952020116',
    id=20020
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621214',
    dialog2='999952020214',
    id=20021
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621414',
    dialog2='999952020414',
    id=20022
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621414',
    dialog2='999952020414',
    id=20023
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621414',
    dialog2='999952020414',
    id=20024
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621614',
    dialog2='999952020614',
    id=20025
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621314',
    dialog2='999952020314',
    id=20026
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621314',
    dialog2='999952020314',
    id=20027
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621314',
    dialog2='999952020314',
    id=20028
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621614',
    dialog2='999952020614',
    id=20029
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621614',
    dialog2='999952020614',
    id=20030
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621514',
    dialog2='999952020514',
    id=20031
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621514',
    dialog2='999952020514',
    id=20032
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621514',
    dialog2='999952020514',
    id=20033
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620034',
    dialog2='999952020034',
    id=20034
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620038',
    dialog2='999952020038',
    id=20038
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620039',
    dialog2='999952020039',
    id=20039
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620040',
    dialog2='999952020040',
    id=20040
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620044',
    dialog2='999952020044',
    id=20044
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621014',
    dialog2='999952020014',
    id=20046
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621014',
    dialog2='999952020014',
    id=20047
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621114',
    dialog2='999952020114',
    id=20048
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988621114',
    dialog2='999952020114',
    id=20049
})

npc_feedback:upsert({
    action1='Encourage',
    action2='Encourage',
    dialog1='999988620055',
    dialog2='999952020055',
    id=20055
})

npc_feedback:update()
