--cost_by_times_cfg.lua
--source: 13_cost_by_times次数消耗表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local cost_by_times = config_mgr:get_table("cost_by_times")

--导出配置内容
cost_by_times:upsert({
    id=101,
    lower_limit=1,
    refresh_id=1,
    upper_limit=2
})

cost_by_times:upsert({
    id=102,
    lower_limit=3,
    refresh_id=1,
    refresh_items={
        [100001]=3
    },
    upper_limit=5
})

cost_by_times:upsert({
    id=103,
    lower_limit=6,
    refresh_id=1,
    refresh_items={
        [100001]=5
    },
    upper_limit=7
})

cost_by_times:upsert({
    id=104,
    lower_limit=8,
    refresh_id=1,
    refresh_items={
        [100001]=10
    },
    upper_limit=9
})

cost_by_times:upsert({
    id=105,
    lower_limit=10,
    refresh_id=1,
    refresh_items={
        [100001]=20
    },
    upper_limit=10
})

cost_by_times:upsert({
    id=201,
    lower_limit=1,
    refresh_id=2,
    refresh_items={
        [100001]=1
    },
    upper_limit=2
})

cost_by_times:upsert({
    id=202,
    lower_limit=2,
    refresh_id=2,
    refresh_items={
        [100001]=2
    },
    upper_limit=3
})

cost_by_times:upsert({
    id=203,
    lower_limit=3,
    refresh_id=2,
    refresh_items={
        [100001]=3
    },
    upper_limit=4
})

cost_by_times:upsert({
    id=204,
    lower_limit=4,
    refresh_id=2,
    refresh_items={
        [100001]=4
    },
    upper_limit=5
})

cost_by_times:upsert({
    id=205,
    lower_limit=5,
    refresh_id=2,
    refresh_items={
        [100001]=5
    },
    upper_limit=6
})

cost_by_times:upsert({
    id=206,
    lower_limit=6,
    refresh_id=2,
    refresh_items={
        [100001]=6
    },
    upper_limit=7
})

cost_by_times:upsert({
    id=207,
    lower_limit=7,
    refresh_id=2,
    refresh_items={
        [100001]=7
    },
    upper_limit=10
})

cost_by_times:update()
