--map_cfg.lua
--source: 7_map_场景表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local map = config_mgr:get_table("map")

--导出配置内容
map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=1,
    map_id=1001,
    max_player_count=0,
    name='世界地图',
    nick='home',
    resname='test1',
    safety_point={},
    size={
        102400,
        102400
    },
    type=3
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=2,
    map_id=1002,
    max_player_count=0,
    name='沙石镇',
    nick='sandrock',
    resname='test2',
    safety_point={},
    size={
        102400,
        102400
    },
    type=3
})

map:upsert({
    absolute_safety_point={
        131338,
        12457,
        81797
    },
    aoi=2,
    center=0,
    grid=3200,
    id=3,
    map_id=1003,
    max_player_count=5,
    name='家园地图',
    navmesh='navmesh/world.bin',
    nick='home',
    resname='MainTest',
    safety_point={
        {
            119483,
            11489,
            104637,
            26797
        },
        {
            113890,
            11165,
            97256,
            20715
        },
        {
            111203,
            10998,
            84121,
            11983
        },
        {
            121054,
            12009,
            83388,
            18322
        },
        {
            116750,
            11557,
            77112,
            14598
        },
        {
            131598,
            11809,
            75627,
            15887
        },
        {
            124658,
            11906,
            78246,
            19214
        },
        {
            131338,
            12457,
            81797,
            35262
        },
        {
            139037,
            11212,
            75285,
            9512
        },
        {
            142363,
            12087,
            85172,
            9073
        },
        {
            68701,
            -14517,
            97478,
            2731
        },
        {
            152852,
            13693,
            86959,
            31989
        },
        {
            156040,
            12739,
            77729,
            12565
        },
        {
            27294,
            85821,
            4313,
            14261
        },
        {
            6281,
            85804,
            4376,
            10659
        },
        {
            16559,
            85805,
            4684,
            35947
        }
    },
    size={
        409600,
        409600
    },
    type=1
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=4,
    map_id=1004,
    max_player_count=0,
    name='拜访城市',
    navmesh='navmesh/world.bin',
    nick='visitcity',
    resname='NeighborCity',
    safety_point={
        {
            37619,
            21852,
            -16323,
            15200
        }
    },
    size={
        409600,
        409600
    },
    type=1
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=5,
    map_id=1005,
    max_player_count=5,
    name='战斗同步测试场景',
    navmesh='navmesh/testsync.bin',
    nick='testsync',
    resname='TestSync',
    safety_point={
        {
            450,
            1,
            -447,
            12061
        }
    },
    size={
        102400,
        102400
    },
    type=1
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=6,
    map_id=1006,
    max_player_count=0,
    name='矿洞',
    nick='minecaves',
    resname='MineCavesIn',
    safety_point={
        148131,
        11314,
        73780
    },
    size={
        102400,
        102400
    },
    type=3
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=7,
    map_id=1007,
    max_player_count=0,
    name='矿洞2',
    nick='minecaves_1',
    resname='MineCaves_1',
    safety_point={
        {
            -7000,
            4000,
            -7200,
            0
        }
    },
    size={
        102400,
        102400
    },
    type=1
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=8,
    map_id=1008,
    max_player_count=0,
    name='民居帐篷室内',
    nick='room1',
    resname='Tentlnside',
    safety_point={},
    size={
        102400,
        102400
    },
    type=4
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=9,
    map_id=1009,
    max_player_count=0,
    name='建筑通用室内1',
    nick='room2',
    resname='RoomTest1',
    safety_point={},
    size={
        102400,
        102400
    },
    type=4
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=2,
    center=0,
    grid=3200,
    id=10,
    map_id=1010,
    max_player_count=0,
    name='建筑通用室内2',
    nick='room3',
    resname='RoomTest2',
    safety_point={},
    size={
        102400,
        102400
    },
    type=4
})

map:upsert({
    absolute_safety_point={
        2000,
        2000,
        2000
    },
    aoi=4,
    center=1,
    grid=1600,
    id=11,
    map_id=1011,
    max_player_count=0,
    name='副本01',
    navmesh='navmesh/dungeon_01.bin',
    nick='dungeon_01',
    resname='Dungeon1',
    safety_point={},
    size={
        57600,
        57600
    },
    type=2
})

map:upsert({
    absolute_safety_point={
        3783,
        300,
        9709
    },
    aoi=4,
    center=1,
    grid=1600,
    id=12,
    map_id=1012,
    max_player_count=0,
    name='TBT新手副本',
    navmesh='navmesh/dungeon_02.bin',
    nick='dungeon_02',
    resname='MineCaveDungeon',
    safety_point={},
    size={
        57600,
        57600
    },
    type=2
})

map:update()
