--openfunc_cfg.lua
--source: 0_openfunc_系统开放表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local openfunc = config_mgr:get_table("openfunc")

--导出配置内容
openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='ROLE',
    id=101,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='PACKET',
    id=102,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=3
    },
    enum_key='ACTIVITY',
    id=103,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='NPC',
    id=104,
    server='scene'
})

openfunc:upsert({
    condition={
        [2]=30010
    },
    enum_key='NPC_RECRUIT',
    id=105,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='MANUAL',
    id=106,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=5
    },
    enum_key='SIGN',
    id=107,
    server='lobby'
})

openfunc:upsert({
    condition={
        [4]=1003
    },
    enum_key='HOME_BUILD',
    id=108,
    server='lobby'
})

openfunc:upsert({
    condition={
        [4]=1003
    },
    enum_key='TOWN_BUILD',
    id=109,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='GROSERY_STORE',
    id=110,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='DISCARD',
    id=111,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=3
    },
    enum_key='MAIL',
    id=112,
    server='lobby'
})

openfunc:upsert({
    condition={
        [4]=1001
    },
    enum_key='ORDER',
    id=113,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='DESORATION_ORDER',
    id=114,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='CANTEEN_STORE',
    id=115,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='LOGGING_STORE',
    id=116,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='SMITHY_STORE',
    id=117,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='FARM_STORE',
    id=118,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='PASTURE_STORE',
    id=119,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='WORK_ENERGY',
    id=120,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='DAILY_TASK',
    id=122,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='LOGIN_RECV_ENERGY',
    id=131,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='WORK_MANUAL',
    id=121,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='TAILOR_STORE',
    id=123,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='MINE_STORE',
    id=124,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='FURNITURE_STORE',
    id=125,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='DESIGN_STORE',
    id=126,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='SEND_GIFT',
    id=127,
    server='social'
})

openfunc:upsert({
    condition={
        [3]=2
    },
    enum_key='CHAT',
    id=128,
    server='social'
})

openfunc:upsert({
    condition={
        [3]=99
    },
    enum_key='FRIEND',
    id=129,
    server='social'
})

openfunc:upsert({
    condition={
        [3]=5
    },
    enum_key='PHOTOGRAPH',
    id=130,
    server='social'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='UNION',
    id=132,
    server='chcom'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='FISH',
    id=133,
    server='scene'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='BUILDING_LIST',
    id=134,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='HELP',
    id=136,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=3
    },
    enum_key='SAT',
    id=137,
    server='scene'
})

openfunc:upsert({
    condition={
        [4]=1006
    },
    enum_key='CITY_MANAGEMENT',
    id=138,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=6
    },
    enum_key='ACTIVITY_HOME_PLACE',
    id=140,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=1
    },
    enum_key='SURVEY',
    id=139,
    server='lobby'
})

openfunc:upsert({
    condition={
        [3]=2
    },
    enum_key='ACTION',
    id=141,
    server='social'
})

openfunc:update()
