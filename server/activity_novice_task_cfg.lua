--activity_novice_task_cfg.lua
--source: 12_activity_novice_task_新手任务.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local activity_novice_task = config_mgr:get_table("activity_novice_task")

--导出配置内容
activity_novice_task:upsert({
    activity_id=1040,
    append_rewards={},
    child_id=1,
    id=1,
    rewards={},
    task_id=7007,
    value=3600
})

activity_novice_task:update()
