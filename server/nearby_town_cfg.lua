--nearby_town_cfg.lua
--source: 10_nearby_town_订单发布者小镇.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local nearby_town = config_mgr:get_table("nearby_town")

--导出配置内容
nearby_town:upsert({
    head='UI_Order_Img_Settlement_01',
    id=40001,
    name='LC_order_TownName_1'
})

nearby_town:upsert({
    head='UI_Order_Img_Settlement_02',
    id=40002,
    name='LC_order_TownName_2'
})

nearby_town:upsert({
    head='UI_Order_Img_Settlement_03',
    id=40003,
    name='LC_order_TownName_3'
})

nearby_town:upsert({
    head='UI_Order_Img_Settlement_04',
    id=40004,
    name='LC_order_TownName_4'
})

nearby_town:update()
