--loop_text_cfg.lua
--source: 10_loop_text_订单任务文案表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local loop_text = config_mgr:get_table("loop_text")

--导出配置内容
loop_text:upsert({
    desc_id='LC_order_Describe_19',
    id=2000301,
    label=1,
    name_id='LC_order_Name_1',
    npcid=20003,
    type=4
})

loop_text:upsert({
    desc_id='LC_order_Describe_19',
    id=2001101,
    label=1,
    name_id='LC_order_Name_1',
    npcid=20011,
    type=4
})

loop_text:upsert({
    desc_id='LC_order_Describe_19',
    id=3000001,
    label=1,
    name_id='LC_order_Name_1',
    npcid=30000,
    type=5
})

loop_text:upsert({
    desc_id='LC_order_Describe_17',
    id=4000101,
    label=9,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=8
})

loop_text:upsert({
    desc_id='LC_order_Describe_17',
    id=4000201,
    label=9,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=8
})

loop_text:upsert({
    desc_id='LC_order_Describe_17',
    id=4000301,
    label=9,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=8
})

loop_text:upsert({
    desc_id='LC_order_Describe_17',
    id=4000401,
    label=9,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=8
})

loop_text:upsert({
    desc_id='LC_order_Describe_1',
    id=4000111,
    label=1,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_3',
    id=4000112,
    label=2,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_5',
    id=4000113,
    label=3,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_7',
    id=4000114,
    label=4,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_9',
    id=4000115,
    label=5,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_11',
    id=4000116,
    label=6,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_13',
    id=4000117,
    label=7,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_15',
    id=4000118,
    label=8,
    name_id='LC_order_Name_1',
    npcid=40001,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_1',
    id=4000211,
    label=1,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_3',
    id=4000212,
    label=2,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_5',
    id=4000213,
    label=3,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_7',
    id=4000214,
    label=4,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_9',
    id=4000215,
    label=5,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_11',
    id=4000216,
    label=6,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_13',
    id=4000217,
    label=7,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_15',
    id=4000218,
    label=8,
    name_id='LC_order_Name_1',
    npcid=40002,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_1',
    id=4000311,
    label=1,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_3',
    id=4000312,
    label=2,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_5',
    id=4000313,
    label=3,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_7',
    id=4000314,
    label=4,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_9',
    id=4000315,
    label=5,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_11',
    id=4000316,
    label=6,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_13',
    id=4000317,
    label=7,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_15',
    id=4000318,
    label=8,
    name_id='LC_order_Name_1',
    npcid=40003,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_1',
    id=4000411,
    label=1,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_3',
    id=4000412,
    label=2,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_5',
    id=4000413,
    label=3,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_7',
    id=4000414,
    label=4,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_9',
    id=4000415,
    label=5,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_11',
    id=4000416,
    label=6,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_13',
    id=4000417,
    label=7,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:upsert({
    desc_id='LC_order_Describe_15',
    id=4000418,
    label=8,
    name_id='LC_order_Name_1',
    npcid=40004,
    type=7
})

loop_text:update()
