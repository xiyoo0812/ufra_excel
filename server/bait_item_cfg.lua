--bait_item_cfg.lua
--source: 7_fishing_鱼饵表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local bait_item = config_mgr:get_table("bait_item")

--导出配置内容
bait_item:upsert({
    drop_id=2013,
    id=109001,
    itemId=109001,
    name='1级鱼饵'
})

bait_item:upsert({
    drop_id=2014,
    id=109002,
    itemId=109002,
    name='2级鱼饵'
})

bait_item:upsert({
    drop_id=2015,
    id=109003,
    itemId=109003,
    name='3级鱼饵'
})

bait_item:update()
