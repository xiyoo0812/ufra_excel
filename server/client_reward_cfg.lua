--client_reward_cfg.lua
--source: 14_client_reward_客户端奖励表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local client_reward = config_mgr:get_table("client_reward")

--导出配置内容
client_reward:upsert({
    c_sys_id=111,
    desc='社区分享奖励',
    email_template_id=100105,
    enable=1,
    id=1000,
    limit_count=1,
    name='社区分享奖励',
    reddot_rid=1352,
    reddot_type=3,
    rewards={
        [100001]=1000,
        [101073]=1
    }
})

client_reward:update()
